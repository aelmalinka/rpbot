/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_PAYLOAD_INC
#	define RPBOT_PAYLOAD_INC

#	include "Json.hh"

#	include <utility>
#	include <memory>
#	include <functional>
#	include <string>

	namespace RpBot
	{
		class Payload
		{
			public:
				enum class OpCode : unsigned char;
				typedef std::function<bool(const Payload &)> Predicate;
			public:
				static std::shared_ptr<Payload> Parse(const std::string &);
				Payload();
				virtual ~Payload();
				std::string toJson() const;
				const OpCode &Op() const;
				const Json::Value &Data() const;
				const std::size_t &Seq() const;
				const std::string &Type() const;
				bool hasSeq() const;
				bool hasType() const;
			protected:
				Payload(
					const OpCode &,
					const Json::Value &,
					const std::optional<std::size_t> &,
					const std::optional<std::string> &
				);
			private:
				OpCode _op;
				Json::Value _data;
				std::optional<std::size_t> _seq;
				std::optional<std::string> _type;
			public:
				enum class OpCode : unsigned char {
					Dispatch = 0,
					Heartbeat = 1,
					Identify = 2,
					StatusUpdate = 3,
					VoiceStateUpdate = 4,
					Resume = 6,
					Reconnect = 7,
					RequestGuildMembers = 8,
					InvalidSession = 9,
					Hello = 10,
					HeartbeatAck = 11
				};
				static Predicate Always;
				// 2019-12-19 AMR NOTE: payloads
				static Predicate Dispatch;
				static Predicate Heartbeat;
				static Predicate Reconnect;
				static Predicate InvalidSession;
				static Predicate Hello;
				static Predicate HeartbeatAck;
				// 2019-12-19 AMR NOTE: events (payload type: dispatch)
				static Predicate Ready;
				static Predicate Resumed;
				static Predicate ChannelCreate;
				static Predicate ChannelUpdate;
				static Predicate ChannelDelete;
				static Predicate ChannelPinned;
				static Predicate GuildCreate;
				static Predicate GuildUpdate;
				static Predicate GuildDelete;
				static Predicate GuildBanAdd;
				static Predicate GuildBanRemove;
				static Predicate GuildEmojisUpdate;
				static Predicate GuildIntegrationsUpdate;
				static Predicate GuildMemberAdd;
				static Predicate GuildMemberRemove;
				static Predicate GuildMemberUpdate;
				static Predicate GuildMemberChunk;
				static Predicate GuildRoleCreate;
				static Predicate GuildRoleUpdate;
				static Predicate GuildRoleDelete;
				static Predicate MessageCreate;
				static Predicate MessageUpdate;
				static Predicate MessageDelete;
				static Predicate MessageDeleteBulk;
				static Predicate MessageReactionAdd;
				static Predicate MessageReactionRemove;
				static Predicate MessageReactionRemoveAll;
				static Predicate PressenceUpdate;
				static Predicate TypingStart;
				static Predicate UserUpdate;
				static Predicate VoiceStateUpdate;
				static Predicate VoiceServerUpdate;
				static Predicate WebhooksUpdate;
		};
	}

#endif
