/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "Application.hh"
#include <boost/algorithm/string.hpp>
#include <algorithm>

using namespace RpBot;
using namespace std;

using Coeus::Severity;
using Coeus::value;
using Coeus::Import;

using boost::starts_with;
using boost::to_lower_copy;
using boost::ierase_first;
using boost::join;
using boost::istarts_with;
using boost::iequals;

Application::Application(
	const int argc,
	const char *argv[]
) :
	::Coeus::Application(argc, argv),
	_ctx()
{
	_opts.add_options()
		("token", value<string>(&_token), "The Bot's Token")
		("host", value<string>(&_host)->default_value(RPBOT_DEFAULT_HOST), "The host of Discord's API")
		("service", value<string>(&_serv)->default_value(RPBOT_DEFAULT_SERVICE), "The service (http,https) of Discord's API")
		("url", value<string>(&_base_url)->default_value(RPBOT_DEFAULT_BASE_URL), "The base URL of Discord's API")
		("rest-version", value<string>(&_rest_version)->default_value(RPBOT_DEFAULT_REST_VERSION), "The version of the REST API")
		("gateway-version", value<string>(&_gate_version)->default_value(RPBOT_DEFAULT_GATEWAY_VERSION), "The Gateway version")
		("import-path,i", value<vector<string>>(&_import_paths)->composing(), "The locations to search for modules")
		("module,m", value<vector<string>>(&_load_mods)->composing(), "The modules to load")
		("prefix,p", value<string>(&_prefix)->default_value(RPBOT_DEFAULT_PREFIX), "The prefix for bot commands")
	;

	command_line().add(_opts);
	config_file().add(_opts);
	environment().add(_opts);
}

Application::~Application()
{}

void Application::operator () ()
{
	::Coeus::Application::operator () ();

	if(_token == ""s) {
		clog << "token needs to be set either with --token, configfile, or RPBOT_TOKEN" << endl;
		exit(78);	// 2019-11-20 AMR TODO: sysexits on windows
	}

	for_each(_import_paths.begin(), _import_paths.end(), [](const auto &s) {
		Import<ModuleBase>::addSearchPath(filesystem::path(s));
	});

	// 2019-11-20 AMR TODO: user agent?
	_ctx = make_shared<Context>(_token, _host, _serv, _base_url, _rest_version, _gate_version);
	_rest = make_shared<Rest>(*_ctx);

	COEUS_LOG(log, Severity::Info) << "Base URL: " << _ctx->BaseUrl();
	COEUS_LOG(log, Severity::Debug) << "Auth: " << _ctx->Auth();	// 2020-01-04 AMR TODO: maybe don't share this?
	COEUS_LOG(log, Severity::Info) << "Importing: " << join(_load_mods, ", ");
	COEUS_LOG(log, Severity::Info) << "From: " << join(_import_paths, ", ");

	(*_rest)(
		http::verb::get,
		"/gateway/bot",
		bind(&Application::handle_gate_endpoint, this, placeholders::_1)
	);

	_ctx->io().run();
}

void Application::onConnect() {
	COEUS_LOG(log, Severity::Info) << "Connected";
	for_each(_load_mods.begin(), _load_mods.end(), bind(&Application::load, this, placeholders::_1));
}

void Application::onMessageCreate(const Payload &p) {
	COEUS_LOG(log, Severity::Debug) << "calling modules with message";

	auto line = p.Data()["content"].asString();
	auto i = find_if(
		_aliases.begin(),
		_aliases.end(),
		[this, &line](auto &i) {
			auto start = _prefix + i.first;
			return istarts_with(line, start);
		}
	);

	if(i != _aliases.end()) {
		ierase_first(line, _prefix + i->first + " "s);

		auto name = get<0>(i->second);
		auto command = get<1>(i->second);
		auto args = get<2>(i->second) + line;

		auto res = ""s;
		auto j = find_if(_modules.begin(), _modules.end(), [&name](auto const &i) -> bool {
			return iequals(i.first, name);
		});

		if(j != _modules.end()) {
			res = j->second->command(command, args, p);
		} else {
			res = "module "s + name + " not found"s;
		}

		if(res != ""s)
			_rest->createMessage(res, p.Data()["channel_id"].asString());
	} else {
		modules([&p](auto &m) {
			m->onMessageCreate(p);
		});
	}
}

bool Application::load(const string &n) {
	COEUS_LOG(log, Severity::Debug) << "Attempting to load " << n;
	// 2019-12-31 AMR TODO: Coeus::Import?
	auto i = _modules.emplace(n, n);

	if(!i.first->second)
		_modules.erase(i.first);
	else {
		auto j = i.first->second;
		j->setRest(_rest);
		j->setGate(_gate);
		j->setPrefix(_prefix);
		j->setApplication(this);
		j->onLoaded();

		return true;
	}

	return false;
}

bool Application::unload(const string &n) {
	// 2020-01-09 AMR TODO: improve perf
	if(_modules.find(n) != _modules.end()) {
		_modules.erase(n);
		return true;
	}

	return false;
}

void Application::modules(const function<void(Import<ModuleBase> &)> &cb) {
	if(cb)
		for_each(_modules.begin(), _modules.end(), bind(cb, bind(&map::value_type::second, placeholders::_1)));
}

void Application::setPrefix(const string &prefix) {
	_prefix = prefix;
	modules(bind(&ModuleBase::setPrefix, placeholders::_1, _prefix));
}

void Application::addAlias(const string &alias, const string &mod, const string &cmd, const string &pref) {
	_aliases[alias] = make_tuple(mod, cmd, pref);
}

void Application::removeAlias(const string &alias) {
	_aliases.erase(alias);
}

string Application::environment_map(const string &key)
{
	if(starts_with(key, "RPBOT_")) {
		auto s = to_lower_copy(key);
		ierase_first(s, "RPBOT_");

		COEUS_LOG(log, Severity::Debug) << "'" << s << "' read from environment";
		return s;
	}

	return ::Coeus::Application::environment_map(key);
}

void Application::handle_gate_endpoint(const Json::Value &j) {
	COEUS_LOG(log, Severity::Info) << "Connecting to " << j["url"].asString();
	COEUS_LOG(log, Severity::Debug) << j["session_start_limit"] << endl;
	_gate = make_shared<Gateway>(
		*_ctx,
		static_cast<string>(j["url"].asString())
	);

	_gate->addCallback(
		Payload::Ready,
		bind(&Application::onConnect, this)
	);

	_gate->addCallback(
		Payload::MessageCreate,
		bind(&Application::onMessageCreate, this, placeholders::_1)
	);
}
