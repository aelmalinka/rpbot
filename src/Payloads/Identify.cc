/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Identify.hh"
#include <boost/predef.h>

using namespace RpBot;
using namespace std;

static string osname() {
#	if BOOST_OS_AIX
		return "AIX"s;
#	elif BOOST_OS_AMIGAOS
		return "Amiga"s;
#	elif BOOST_OS_ANDROID
		return "Android"s;
#	elif BOOST_OS_BEOS
		return "BeOS"s;
#	elif BOOST_OS_BSD
		return "BSD"s;
#	elif BOOST_OS_CYGWIN
		return "Cygwin"s;
#	elif BOOST_OS_HAIKU
		return "Haiku"s;
#	elif BOOST_OS_HPUX
		return "HP-UX"s;
#	elif BOOST_OS_IOS
		return "iOS"s;
#	elif BOOST_OS_MACOS
		return "MacOS"s;
#	elif BOOST_OS_OS400
		return "OS/400"s;
#	elif BOOST_OS_QNX
		return "QNX"s;
#	elif BOOST_OS_SOLARIS
		return "Solaris"s;
#	elif BOOST_OS_SRV4
		return "UNIX System-V R4"s;
#	elif BOOST_OS_VMS
		return "VMS"s;
#	elif BOOST_OS_LINUX
		return "Linux"s;
#	elif BOOST_OS_UNIX
		return "Unix"s;
#	elif BOOST_OS_WINDOWS
		return "Windows"s;
#	else
		return "Unknown"s;
#	endif
}

static Json::Value data(const Context &ctx) {
	Json::Value properties;
	properties["$os"] = osname();
	properties["$browser"] = ctx.UserAgent();
	properties["$device"] = ctx.UserAgent();

	Json::Value data;
	data["token"] = ctx.Token();
	data["properties"] = properties;

	return data;
}

Identify::Identify(const Context &ctx) :
	Payload(
		OpCode::Identify,
		data(ctx),
		{},
		{}
	)
{}
