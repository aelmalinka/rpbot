/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Heartbeat.hh"

using namespace RpBot;
using namespace std;

Heartbeat::Heartbeat() :
	Payload(
		OpCode::Heartbeat,
		{},
		{},
		{}
	)
{}

Heartbeat::Heartbeat(const size_t &seq) :
	Payload(
		OpCode::Heartbeat,
		{},
		seq,
		{}
	)
{}
