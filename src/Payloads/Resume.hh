/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_MESSAGES_RESUME_INC
#	define RPBOT_MESSAGES_RESUME_INC

#	include "../Payload.hh"
#	include "../Context.hh"

	namespace RpBot
	{
		class Resume :
			public Payload
		{
			public:
				Resume(const Context &, const std::string &, const std::size_t &);
		};
	}

#endif
