/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_MESSAGE_VOICESTATEUPDATE_INC
#	define RPBOT_MESSAGE_VOICESTATEUPDATE_INC

#	include "../Payload.hh"

	namespace RpBot
	{
		class VoiceStateUpdate :
			public Payload
		{
			public:
				VoiceStateUpdate();
		};
	}

#endif
