/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Resume.hh"

using namespace RpBot;
using namespace std;

static Json::Value data(
	const Context &ctx,
	const string &id,
	const size_t &seq
) {
	Json::Value d;

	d["token"] = ctx.Token();
	d["session_id"] = id;
	d["seq"] = seq;

	return d;
}

Resume::Resume(
	const Context &ctx,
	const string &id,
	const size_t &seq
) :
	Payload(
		OpCode::Resume,
		data(ctx, id, seq),
		{},
		{}
	)
{}
