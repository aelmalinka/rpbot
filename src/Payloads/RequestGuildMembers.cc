/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "RequestGuildMembers.hh"

using namespace RpBot;
using namespace std;

RequestGuildMembers::RequestGuildMembers() :
	Payload(
		OpCode::RequestGuildMembers,
		{},
		{},
		{}
	)
{}
