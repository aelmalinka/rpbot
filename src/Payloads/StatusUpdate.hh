/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_MESSAGES_STATUSUPDATE_INC
#	define RPBOT_MESSAGES_STATUSUPDATE_INC

#	include "../Payload.hh"

	namespace RpBot
	{
		class StatusUpdate :
			public Payload
		{
			public:
				StatusUpdate();
		};
	}

#endif
