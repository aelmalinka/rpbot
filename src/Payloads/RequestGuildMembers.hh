/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_MESSAGE_REQUESTGUILDMEMBERS_INC
#	define RPBOT_MESSAGE_REQUESTGUILDMEMBERS_INC

#	include "../Payload.hh"

	namespace RpBot
	{
		class RequestGuildMembers :
			public Payload
		{
			public:
				RequestGuildMembers();
		};
	}

#endif
