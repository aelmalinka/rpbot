/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_CONTEXT_INC
#	define RPBOT_CONTEXT_INC

#	include "Exception.hh"

//	2019-11-17 AMR TODO: replace with std::net when works
#	include <boost/asio/io_context.hpp>
#	include <boost/asio/ssl.hpp>

	namespace RpBot
	{
		namespace net = boost::asio;
		namespace ssl = net::ssl;

		class Context
		{
			public:
				explicit Context(
					const std::string &,
					const std::string & = _def_host,
					const std::string & = _def_serv,
					const std::string & = _def_base,
					const std::string & = _def_rest,
					const std::string & = _def_gate,
					const std::string & = _def_ua
				);
				net::io_context &io();
				ssl::context &ssl();
				const std::string &Token() const;
				const std::string &Base() const;
				const std::string &Host() const;
				const std::string &Service() const;
				const std::string &GatewayVersion() const;
				const std::string &UserAgent() const;
				std::string BaseUrl() const;
				std::string Auth() const;
			private:
				std::string _token;
				std::string _host;
				std::string _serv;
				std::string _base;
				std::string _gateway;
				std::string _ua;
				net::io_context _io;
				ssl::context _ssl;
			private:
				static const char _def_host[];
				static const char _def_serv[];
				static const char _def_base[];
				static const char _def_rest[];
				static const char _def_gate[];
				static const char _def_ua[];
		};
	}

#endif
