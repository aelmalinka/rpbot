/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Rest.hh"
#include <sstream>

using namespace RpBot;
using namespace std;

Rest::Rest(Context &ctx) :
	_ctx(ctx),
	_last(0),
	_reqs()
{}

Rest::~Rest() = default;

void Rest::operator () (
	const http::verb &verb,
	const string &uri,
	const callback &cb,
	const optional<string> &body
) {
	auto j = _last++;
	auto t = make_tuple(Http(_ctx), cb);
	auto i = _reqs.emplace(j, move(t)).first;
	get<Http>(i->second)(
		verb,
		uri,
		bind(&Rest::handle_res, this, placeholders::_1, j),
		body
	);
}

void Rest::createMessage(const string &msg, const string &ch) {
	auto s = stringstream();
	auto j = Json::Value();

	j["content"] = msg;
	s << j;

	Rest::operator() (
		http::verb::post,
		"/channels/" + ch + "/messages",
		[](const auto &){},
		s.str()
	);
}

void Rest::deleteMessage(const string &msg, const string &ch) {
	Rest::operator() (
		http::verb::delete_,
		"/channels/" + ch + "/messages/" + msg,
		[](const auto &){}
	);
}

void Rest::handle_res(const response &res, const std::size_t &i) {
	// 2019-11-23 AMR TODO: what if content-type != json?
	auto v = jsonParse(res.body());
	COEUS_LOG(Log, Severity::Debug) << v;

	auto j = _reqs.find(i);
	get<callback>(j->second)(v);
	_reqs.erase(i);
}
