/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "ModuleBase.hh"

using namespace RpBot;
using namespace std;

using Coeus::Import;

ModuleBase::ModuleBase(
	const string &name,
	const list<string> &deps
) :
	Log("RpBot-"s + name),
	_name(name),
	_dep_names(deps),
	_prefix(RPBOT_DEFAULT_PREFIX),
	_rest(),
	_gate(),
	_app(nullptr),
	_deps()
{
	COEUS_LOG(Log, Severity::Debug) << "Module " << _name << " Loaded";
}

ModuleBase::~ModuleBase() = default;

void ModuleBase::setRest(const shared_ptr<Rest> &rest) {
	_rest = rest;
}

void ModuleBase::setGate(const shared_ptr<Gateway> &gate) {
	_gate = gate;
}

void ModuleBase::setApplication(Application *a) {
	_app = a;

	for_each(_dep_names.begin(), _dep_names.end(), bind(&ModuleBase::load_dep, this, placeholders::_1));
	app().modules(bind(&ModuleBase::save_dep, this, placeholders::_1));

	if(_dep_names.size() != 0)
		COEUS_THROW(Exception("Dep resolution failed somehow"));
}

void ModuleBase::setPrefix(const string &prefix) {
	_prefix = prefix;
}

const string &ModuleBase::name() const {
	return _name;
}

const string &ModuleBase::prefix() const {
	return _prefix;
}

Rest &ModuleBase::rest() {
	return *_rest;
}

Gateway &ModuleBase::gate() {
	return *_gate;
}

// 2020-01-09 AMR TODO: this could probably be safer
Application &ModuleBase::app() {
	return *_app;
}

ModuleBase::mod &ModuleBase::dep(const string &n) {
	auto i = _deps.find(n);
	if(i == _deps.end())
		// 2020-01-16 AMR TODO: use error info
		COEUS_THROW(Exception(name() + " requesting module not dependend on "s + n));
	return i->second;
}

void ModuleBase::load_dep(const string &n) {
	if(!app().load(n))
		COEUS_THROW(Exception(_name + " needs module "s + n));
}

void ModuleBase::save_dep(const Import<ModuleBase> &m) {
	auto i = find(_dep_names.begin(), _dep_names.end(), m->name());
	if(i != _dep_names.end()) {
		_deps.emplace(m->name(), m);
		_dep_names.erase(i);
	}
}
