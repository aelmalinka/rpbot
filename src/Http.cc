/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Http.hh"

using namespace RpBot;
using namespace std;

Http::Http(Context &c) :
	Connection<>(c),
	_con(false),
	_req()
{}

Http::Http(Http &&o) :
	Connection(move(o)),
	_con(move(o._con)),
	_req(move(o._req)),
	_res(move(o._res)),
	_buff(move(o._buff))
{}

Http::~Http() = default;

void Http::operator () (
	const http::verb &method,
	const string &target,
	const callback &cb,
	const optional<string> &body
) {
	if(!_con)
		connect(ctx().Host(), ctx().Service());

	if(body)
		_req = make_shared<request>(
			method,
			ctx().Base() + target,
			http_version,
			*body
		);
	else
		_req = make_shared<request>(
			method,
			ctx().Base() + target,
			http_version
		);

	if(body) {
		_req->set(http::field::content_type, "application/json");
		_req->set(http::field::content_length, body->size());
	}

	_req->set(http::field::accept, "*/*"s);
	_req->set(http::field::host, ctx().Host());
	_req->set(http::field::user_agent, ctx().UserAgent());
	_req->set(http::field::authorization, ctx().Auth());

	_cb = cb;

	if(_con)
		http::async_write(_stream, *_req, bind(&Http::onWrite, this, placeholders::_1, placeholders::_2));
}

void Http::disconnect() {
	_stream.async_shutdown(bind(&Http::onShutdown, this, placeholders::_1));
}

void Http::onTlsHandshake(const error_code &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});

	COEUS_LOG(Log, Severity::Debug) << " tls handshake";

	if(_req)
		http::async_write(_stream, *_req, bind(&Http::onWrite, this, placeholders::_1, placeholders::_2));
	else
		_con = true;
}

void Http::onWrite(const error_code &ec, const size_t sz) {
	if(ec)
		COEUS_THROW(system_error{ec});

	COEUS_LOG(Log, Severity::Debug) << sz << " written";

	http::async_read(
		_stream,
		_buff,
		_res,
		bind(&Http::onRead, this, placeholders::_1, placeholders::_2)
	);
}

void Http::onRead(const error_code &ec, const size_t sz) {

	COEUS_LOG(Log, Severity::Debug) << sz << " read";

	if(ec) {
		COEUS_LOG(Log, Severity::Debug) << "error with res: " << beast::buffers_to_string(_buff.data());
		COEUS_THROW(system_error{ec});
	}

	disconnect();

	// 2019-11-24 AMR TODO: move to Rest
	// 2019-11-20 AMR TODO: target?
	// 2019-11-20 AMR TODO: actually limit rates based on these
	if(_res.count("X-RateLimit-Remaining"))
		COEUS_LOG(Log, Severity::Info) << _res.at("X-RateLimit-Remaining") << " calls left";
	if(_res.count("X-RateLimit-Reset"))
		COEUS_LOG(Log, Severity::Info) << "_resets at " << _res.at("X-RateLimit-Reset");
	if(_res.count("X-RateLimit-Reset-After"))
		COEUS_LOG(Log, Severity::Info) << _res.at("X-RateLimit-Reset-After") << " time remaining";
	if(_res.count("X-RateLimit-Bucket"))
		COEUS_LOG(Log, Severity::Info) << "in bucket " << _res.at("X-RateLimit-Bucket");
}

void Http::onShutdown(const error_code &ec) {
	if(ec)
		COEUS_LOG(Log, Severity::Info) << "error on ssl shutdown: " << ec.message();

	get_lowest_layer(_stream).close();

	if(_cb)
		_cb(_res);
}
