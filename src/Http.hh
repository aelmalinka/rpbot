/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_HTTP_INC
#	define RPBOT_HTTP_INC

#	include "Connection.hh"

	namespace RpBot
	{
		class Http :
			public Connection<>
		{
			public:
				using body = http::string_body;
				using response = http::response<body>;
				using request = http::request<body>;
				using buffer = beast::flat_buffer;
				using callback = std::function<void(const response &)>;
			public:
				explicit Http(Context &);
				Http(Http &&);
				virtual ~Http();
				virtual void operator () (
					const http::verb &,
					const std::string &,
					const callback &,
					const std::optional<std::string> & = std::optional<std::string>()
				);
				virtual void disconnect();
			protected:
				virtual void onTlsHandshake(const error_code &) override;
				virtual void onWrite(const error_code &, const std::size_t);
				virtual void onRead(const error_code &, const std::size_t);
				virtual void onShutdown(const error_code &);
			private:
				bool _con;
				callback _cb;
				std::shared_ptr<request> _req;
				response _res;
				buffer _buff;
				static constexpr int http_version = 11;
		};
	}

#endif
