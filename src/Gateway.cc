/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Gateway.hh"
#include "Payloads/Heartbeat.hh"
#include "Payloads/Identify.hh"
#include "Payloads/Resume.hh"

using namespace RpBot;
using namespace std;

Gateway::Gateway(
	Context &ctx,
	const optional<string> &uri
) :
	Websocket(ctx),
	_cbs(),
	_rate(0),
	_timer(ctx.io()),
	_seq_num(),
	_timed_out(true)
{
	// 2019-12-19 AMR NOTE: track sequence numbers
	addCallback(
		Payload::Always,
		[this](const auto &m) {
			if(m.hasSeq())
				_seq_num = m.Seq();
		}
	);

	// 2019-12-19 AMR NOTE: respond to heartbeats with heartbeats
	addCallback(
		Payload::Heartbeat,
		[this](const auto &) {
			if(_seq_num)
				send(Heartbeat(*_seq_num));
			else
				send(Heartbeat());
		}
	);

	// 2019-12-19 AMR NOTE: time out if not getting heartbeats in time
	addCallback(
		Payload::HeartbeatAck,
		[this](const auto &) {
			_timed_out = false;
		}
	);

	// 2019-12-19 AMR NOTE: store session id once it's available
	addCallback(
		Payload::Ready,
		[this](const auto &m) {
			_sess_id = m.Data()["session_id"].asString();
		}
	);

	// 2019-12-19 AMR NOTE: Invalid session
	addCallback(
		Payload::InvalidSession,
		[this](const auto &m) {
			COEUS_LOG(Log, Severity::Error) << "Invalid Session" << (m.Data().asBool() ? " (Resumable)" : "");
		}
	);

	// 2019-12-19 AMR NOTE: start heartbeating
	addCallback(
		Payload::Hello,
		[this](const auto &m) {
			_rate = decltype(_rate)(m.Data()["heartbeat_interval"].asInt());
			_timed_out = false;
			_timer.expires_after(_rate);
			_timer.async_wait(bind(&Gateway::heartbeat, this, placeholders::_1));
		}
	);

	// 2019-12-19 AMR NOTE: identify
	addCallback(
		Payload::Hello,
		[this, &ctx](const auto &) {
			if(_sess_id)
				send(Resume(
					ctx,
					*_sess_id,
					(_seq_num ? *_seq_num : 0)
				));
			else
				send(Identify(
					ctx
				));
		}
	);

	if(uri)
		connect(*uri);
	else {	// 2019-11-24 AMR HACK: this bypasses any rate limiting
		Rest r(ctx);
		r(
			http::verb::get,
			_gateway_rest,
			bind(&Gateway::handle_rest, this, placeholders::_1)
		);
	}
}

Gateway::~Gateway() = default;

void Gateway::addCallback(
	const function<bool (const Payload &)> &pred,
	const function<void (const Payload &)> &cb
) {
	_cbs.emplace_back(make_pair(pred, cb));
}

void Gateway::send(
	const Payload &m
) {
	Websocket::send(m.toJson());
}

void Gateway::onMessage(const error_code &ec, const string &d) {
	if(ec)
		COEUS_THROW(system_error{ec});

	COEUS_LOG(Log, Severity::Debug) << d;
	shared_ptr<Payload> msg = Payload::Parse(d);
	onPayload(*msg);
}

void Gateway::onPayload(const Payload &m) {
	for(auto &p : _cbs) {
		if(p.first && p.second && p.first(m))
			p.second(m);
	}
}

void Gateway::handle_rest(const Json::Value &v) {
	connect(v["url"].asString());
}

void Gateway::heartbeat(const error_code &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});
	if(_timed_out)
		COEUS_THROW(system_error{error_code(net::error::basic_errors::timed_out, net::error::get_system_category())});

	_timed_out = true;
	if(_seq_num)
		send(Heartbeat(*_seq_num));
	else
		send(Heartbeat());

	_timer.expires_after(_rate);
	_timer.async_wait(bind(&Gateway::heartbeat, this, placeholders::_1));
}
