/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Module.hh"

#include <boost/algorithm/string.hpp>

using namespace RpBot;
using namespace std;

using boost::istarts_with;

Module::Module(
	const string &name,
	const list<string> &deps
) :
	ModuleBase(name, deps),
	_commands()
{}

Module::~Module() = default;

string Module::command(
	const string &command,
	const string &line,
	const Payload &msg
) {
	COEUS_LOG(Log, Severity::Info) << name() << ": " << command << " (" << line << ")";
	auto i = _commands.find(command);
	if(i != _commands.end()) {
		return i->second(line, msg);
	} else {
		COEUS_LOG(Log, Severity::Error) << "Command " << command << " not found";
		return name() + ": Command "s + command + " not found"s;
	}
}

void Module::setCommand(
	const string &command,
	const function<string(const string &, const Payload &)> &cb
) {
	_commands[command] = cb;
}

void Module::onMessageCreate(const Payload &msg) {
	// 2019-12-28 AMR NOTE: <prefix><name> <command>
	//	eg: !store set
	const auto &line = msg.Data()["content"].asString();
	auto start = prefix() + name();

	if(
		istarts_with(line, start) &&	// 2019-12-28 AMR NOTE: this could be better perf (every module checking)
		line.size() > start.size() + 1
	) {
		auto cmd = line.substr(
			start.size() + 1,				// 2019-12-28 AMR NOTE: just past first space
			line.find(" ", start.size() + 1) - start.size() - 1
		);

		auto args = ""s;
		if(line.size() > start.size() + cmd.size() + 2)
			args = line.substr(start.size() + cmd.size() + 2);

		auto res = this->command(cmd, args, msg);
		COEUS_LOG(Log, Severity::Info) << cmd << ": " << res;

		// 2020-01-06 AMR TODO: optional<string>?
		if(res != "")
			rest().createMessage(res, msg.Data()["channel_id"].asString());
	}
}
