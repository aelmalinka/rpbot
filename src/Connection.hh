/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_CONNECTION_INC
#	define RPBOT_CONNECTION_INC

#	include "Context.hh"

#	include <boost/beast/core.hpp>
#	include <boost/beast/http.hpp>
#	include <boost/beast/ssl/ssl_stream.hpp>

	namespace RpBot
	{
		namespace beast = boost::beast;
		namespace http = beast::http;

		using tcp = net::ip::tcp;
		using error_code = boost::system::error_code;

		template<
			typename Stream = beast::ssl_stream<beast::tcp_stream>
		>
		class Connection
		{
			public:
				explicit Connection(Context &);
				Connection(const Connection<Stream> &) = delete;
				Connection(Connection<Stream> &&);
				virtual ~Connection();
				virtual void connect(const std::string &, const std::string & = "https");
				virtual void disconnect() = 0;
			protected:
				virtual void onResolve(const error_code &, const tcp::resolver::results_type &);
				virtual void onConnect(const error_code &, const tcp::resolver::endpoint_type &);
				virtual void onTlsHandshake(const error_code &) = 0;
			private:
				Context &_ctx;
			protected:
				const Context &ctx() const;
				Stream _stream;
		};
	}

#	include "Connection.impl.hh"

#endif
