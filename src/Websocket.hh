/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_WEBSOCKET_INC
#	define RPBOT_WEBSOCKET_INC

#	include "Connection.hh"
#	include <boost/beast/websocket.hpp>
#	include <queue>

	namespace RpBot
	{
		namespace websocket = beast::websocket;

		// 2019-11-20 AMR TODO: best api?
		class Websocket :
			public Connection<
				websocket::stream<
					beast::ssl_stream<
						beast::tcp_stream
					>
				>
			>
		{
			public:
				typedef websocket::stream<beast::ssl_stream<beast::tcp_stream>> stream;
				typedef beast::flat_buffer buffer;
			public:
				explicit Websocket(Context &);
				virtual ~Websocket();
				// 2020-01-07 AMR TODO: this is bad naming
				virtual void close();
				virtual void disconnect();
				virtual void connect(const std::string &);
				virtual void send(std::string &&);
			protected:
				virtual void onTlsHandshake(const error_code &);
				virtual void onWsHandshake(const error_code &);
				virtual void onRead(const error_code &, const std::size_t &, const std::size_t &);
				virtual void onWrite(const error_code &, const std::size_t &);
				virtual void onMessage(const error_code &, const std::string &) = 0;
				virtual void onClose(const error_code &);
				virtual void onShutdown(const error_code &);
			private:
				void addMessage(const std::string &);
				void _close(const websocket::close_code & = websocket::close_code::normal);
			private:
				bool _connected;
				std::string _host;
				std::string _service;
				std::string _target;
				std::map<std::size_t, buffer> _buffs;
				std::size_t _last;
				std::queue<std::string> _reqs;
		};
	}

#endif
