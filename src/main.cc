/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <cstdlib>
#include <iostream>
#include "Application.hh"

using namespace std;
using namespace RpBot;

int main(int argc, const char *argv[]) {
	try {
		Application app(argc, argv);
		app();

		return EXIT_SUCCESS;
	} catch(exception &e) {
		cerr << e << endl;
		return EXIT_FAILURE;
	}
}
