/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Context.hh"

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <boost/algorithm/string.hpp>
#include <vector>

using namespace RpBot;
using namespace std;

const char Context::_def_host[] = RPBOT_DEFAULT_HOST;
const char Context::_def_serv[] = RPBOT_DEFAULT_SERVICE;
const char Context::_def_base[] = RPBOT_DEFAULT_BASE_URL;
const char Context::_def_rest[] = RPBOT_DEFAULT_REST_VERSION;
const char Context::_def_gate[] = RPBOT_DEFAULT_GATEWAY_VERSION;
const char Context::_def_ua[] = RPBOT_DEFAULT_USER_AGENT;

Context::Context(
	const string &token,
	const string &host,
	const string &serv,
	const string &base,
	const string &rest,
	const string &gate,
	const string &ua
) :
	_token(token),
	_host(host),
	_serv(serv),
	_base(base + "/v"s + rest),
	_gateway(gate),
	_ua(ua),
	_io(),
	_ssl(ssl::context::tlsv12_client)
{}

net::io_context &Context::io() {
	return _io;
}

ssl::context &Context::ssl() {
	return _ssl;
}

const string &Context::Token() const {
	return _token;
}

const string &Context::Base() const {
	return _base;
}

const string &Context::GatewayVersion() const {
	return _gateway;
}

const string &Context::UserAgent() const {
	return _ua;
}

const string &Context::Host() const {
	return _host;
}

const string &Context::Service() const {
	return _serv;
}

string Context::BaseUrl() const {
	return _serv + "://"s + _host + "/"s + _base;
}

string Context::Auth() const {
	return "Bot "s + Token();
}
