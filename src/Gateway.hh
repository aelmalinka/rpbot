/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_GATEWAY_INC
#	define RPBOT_GATEWAY_INC

#	include "Websocket.hh"
#	include "Rest.hh"
#	include "Payload.hh"

#	include <list>
#	include <boost/asio/system_timer.hpp>

	namespace RpBot
	{
		// 2019-12-19 AMR TODO: ratelimiting
		class Gateway :
			public Websocket
		{
			public:
				explicit Gateway(
					Context &,
					const std::optional<std::string> & = std::optional<std::string>()
				);
				virtual ~Gateway();
				virtual void addCallback(const std::function<bool (const Payload &)> &, const std::function<void (const Payload &)> &);
				virtual void send(const Payload &);
			protected:
				virtual void onMessage(const error_code &, const std::string &) override;
				virtual void onPayload(const Payload &);
			private:
				void handle_rest(const Json::Value &);
				void heartbeat(const error_code &);
			private:
				std::list<
					std::pair<
						std::function<bool (const Payload &)>,
						std::function<void (const Payload &)>
					>
				> _cbs;
				std::chrono::milliseconds _rate;
				net::system_timer _timer;
				std::optional<std::size_t> _seq_num;
				std::optional<std::string> _sess_id;
				bool _timed_out;
				static constexpr char _gateway_rest[] = "/gateway/bot";
		};
	}

#endif
