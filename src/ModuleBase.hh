/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_MODULEBASE_INC
#	define RPBOT_MODULEBASE_INC

#	include <Coeus/Import.hh>

#	include "Exception.hh"
#	include "Gateway.hh"
#	include <list>
#	include <map>

	namespace RpBot
	{
		class Application;

		class ModuleBase {
			protected:
				using mod = Coeus::Import<ModuleBase>;
				using dep_t = std::map<
					std::string,
					mod
				>;
			public:
				explicit ModuleBase(const std::string &, const std::list<std::string> &);
				virtual ~ModuleBase();
				virtual std::string command(const std::string &, const std::string &, const Payload &) = 0;
				virtual void setRest(const std::shared_ptr<Rest> &);
				virtual void setGate(const std::shared_ptr<Gateway> &);
				// 2020-01-09 AMR NOTE: application owns modules
				virtual void setApplication(Application *);
				virtual void setPrefix(const std::string &);
				virtual void onLoaded() {}
				virtual void onGuildCreate(const Payload &) {}
				virtual void onGuildUpdate(const Payload &) {}
				virtual void onGuildDelete(const Payload &) {}
				virtual void onChannelCreate(const Payload &) {}
				virtual void onChannelUpdate(const Payload &) {}
				virtual void onChannelDelete(const Payload &) {}
				virtual void onChannelPinned(const Payload &) {}
				virtual void onMessageCreate(const Payload &) {}
				virtual void onMessageUpdate(const Payload &) {}
				virtual void onMessageDelete(const Payload &) {}
				virtual void onMessageDeleteBulk(const Payload &) {}
				const std::string &name() const;
			protected:
				const std::string &prefix() const;
				Rest &rest();
				Gateway &gate();
				Application &app();
				mod &dep(const std::string &);
				Coeus::Log::Source Log;
			private:
				std::string _name;
				std::list<std::string> _dep_names;
				std::string _prefix;
				std::shared_ptr<Rest> _rest;
				std::shared_ptr<Gateway> _gate;
				Application *_app;
				dep_t _deps;
			private:
				void load_dep(const std::string &);
				void save_dep(const mod &);
		};
	}

#	include "Application.hh"

#endif
