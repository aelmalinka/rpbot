/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_CONNECTION_IMPL
#	define RPBOT_CONNECTION_IMPL

#	include <vector>
#	include <utility>
#	include <type_traits>

	namespace RpBot
	{
		namespace detail {
			template<
				typename T,
				bool = !std::is_same<
					typename std::decay<T>::type,
					beast::lowest_layer_type<T>
				>::value
			>
			struct next_or_lowest_layer
			{
				using type = std::decay<T>::type::next_layer_type;
			};

			template<typename T>
			struct next_or_lowest_layer<T, false>
			{
				using type = std::decay<T>::type;
			};

			template<
				typename T,
				bool = std::is_same<
					typename std::decay<T>::type,
					beast::ssl_stream<typename next_or_lowest_layer<T>::type>
				>::value ||
				std::is_same<
					typename std::decay<T>::type,
					ssl::stream<typename next_or_lowest_layer<T>::type>
				>::value
			>
			struct is_ssl :
				std::false_type
			{};

			template<typename T>
			struct is_ssl<T, true> :
				std::true_type
			{};

			template<
				typename T,
				bool = is_ssl<T>::value
			>
			struct get_ssl_type
			{
				using type = typename std::decay<T>::type;
			};

			template<typename T>
			struct get_ssl_type<T, false>
			{
				using type = typename T::next_layer_type;
			};

			template<
				typename T,
				bool = is_ssl<T>::value
			>
			struct get_ssl
			{
				static get_ssl_type<T>::type &value(T &t) noexcept {
					return get_ssl<typename next_or_lowest_layer<T>::type>::value(t.next_layer());
				}
			};

			template<typename T>
			struct get_ssl<T, true>
			{
				static get_ssl_type<T>::type &value(T &t) noexcept {
					return t;
				}
			};
		}

		template<typename Stream>
		Connection<Stream>::Connection(Context &ctx) :
			_ctx(ctx),
			_stream(_ctx.io(), _ctx.ssl())
		{}

		template<typename Stream>
		Connection<Stream>::Connection(Connection<Stream> &&o) :
			_ctx(o._ctx),
			_stream(std::move(o._stream))
		{}

		template<typename Stream>
		Connection<Stream>::~Connection() = default;

		template<typename Stream>
		void Connection<Stream>::connect(const std::string &host, const std::string &service)
		{
			using namespace std;

			COEUS_LOG(Log, Severity::Debug) << "resolving " << host << " " << service;
			auto r = make_shared<tcp::resolver>(_ctx.io());
			r->async_resolve(host, service, [this, r](const auto &a, const auto &b) {
				this->onResolve(a, b);
			});
		}

		template<typename Stream>
		void Connection<Stream>::onResolve(const error_code &ec, const tcp::resolver::results_type &results)
		{
			if(ec)
				COEUS_THROW(std::system_error{ec});

			COEUS_LOG(Log, Severity::Debug) << "Resolved, attempting to connect";
			get_lowest_layer(_stream).async_connect(
				results,
				std::bind(&Connection<Stream>::onConnect, this, std::placeholders::_1, std::placeholders::_2)
			);
		}

		template<typename Stream>
		void Connection<Stream>::onConnect(const error_code &ec, const tcp::resolver::endpoint_type &)
		{
			if(ec)
				COEUS_THROW(std::system_error{ec});

			COEUS_LOG(Log, Severity::Debug) << "Connected, attempting to tls";
			detail::get_ssl<Stream>::value(_stream).async_handshake(
				ssl::stream_base::client,
				std::bind(&Connection<Stream>::onTlsHandshake, this, std::placeholders::_1)
			);
		}

		template<typename Stream>
		const Context &Connection<Stream>::ctx() const {
			return _ctx;
		}
	}

#endif
