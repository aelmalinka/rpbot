/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Json.hh"
#include <memory>
#include <sstream>

using namespace std;
using namespace ::Json;

namespace RpBot {
	Value jsonParse(const string &v) {
		auto m = Value();
		auto b = CharReaderBuilder();
		auto errs = ""s;

		// 2020-01-25 AMR TODO: is there a better way to decltype then remove_reference?
		auto const reader = unique_ptr<CharReader>(b.newCharReader());

		if(!reader->parse(v.data(), v.data() + v.size(), &m, &errs))
			COEUS_THROW(Exception("Failed to parse JSON") <<
				JsonError(errs) <<
				JsonString(v)
			);

		return m;
	}

	string jsonWrite(const Value &v) {
		auto s = stringstream();
		auto b = StreamWriterBuilder();

		b["commentStyle"] = "None";
		b["indentation"] = "\t";

		auto const reader = unique_ptr<StreamWriter>(b.newStreamWriter());

		if(reader->write(v, &s))
			COEUS_THROW(Exception("Failed to write JSON") <<
				JsonValue(v)
			);

		return s.str();
	}
}
