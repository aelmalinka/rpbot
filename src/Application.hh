/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_APPLICATION_INC
#	define RPBOT_APPLICATION_INC

#	include "Exception.hh"
#	include "Rest.hh"
#	include "Gateway.hh"
#	include "ModuleBase.hh"

#	include <map>
#	include <Coeus/Application.hh>

	namespace RpBot
	{
		class Application :
			public Coeus::Application
		{
			private:
				using map = std::map<
					std::string,
					Coeus::Import<ModuleBase>
				>;
				using alias = std::tuple<
					std::string,	// module name
					std::string,	// command
					std::string		// args prefix
				>;
				using aliases = std::map<std::string, alias>;
			public:
				Application(const int, const char *[]);
				virtual ~Application();
				virtual void operator () () override;
				virtual bool load(const std::string &);
				virtual bool unload(const std::string &);
				virtual void modules(const std::function<void(Coeus::Import<ModuleBase> &)> &);
				virtual void setPrefix(const std::string &);
				virtual void addAlias(const std::string &, const std::string &, const std::string &, const std::string &);
				virtual void removeAlias(const std::string &);
			protected:
				virtual void onConnect();
				virtual void onMessageCreate(const Payload &);
			protected:
				virtual std::string environment_map(const std::string &) override;
			private:
				void handle_gate_endpoint(const Json::Value &);
			private:
				std::string _host;
				std::string _serv;
				std::string _base_url;
				std::string _rest_version;
				std::string _gate_version;
				std::string _token;
				std::string _prefix;
				std::vector<std::string> _import_paths;
				std::vector<std::string> _load_mods;
				Coeus::options_description _opts;
			private:
				std::shared_ptr<Context> _ctx;
				std::shared_ptr<Rest> _rest;
				std::shared_ptr<Gateway> _gate;
				map _modules;
				aliases _aliases;
		};
	}

#endif
