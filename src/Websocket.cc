/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Websocket.hh"

#include <boost/algorithm/string.hpp>

using namespace RpBot;
using namespace std;

using boost::split;
using boost::is_any_of;
using boost::join;

Websocket::Websocket(Context &ctx) :
	Connection<stream>(ctx),
	_connected(false),
	_host(),
	_service("https"s),
	_target(),
	_buffs(),
	_last(0)
{}

Websocket::~Websocket() {
	close();
}

void Websocket::close() {
	_close(websocket::close_code::going_away);
}

void Websocket::disconnect() {
	_close();
}

void Websocket::_close(const websocket::close_code &code) {
	beast::get_lowest_layer(_stream).cancel();
	_connected = false;
	_stream.async_close(code, bind(&Websocket::onClose, this, placeholders::_1));
}

void Websocket::connect(const string &uri) {
	// 2019-12-20 AMR TODO: shared URI parsing
	if(uri == "")
		return; // 2019-12-20 AMR NOTE: at least host is required

	vector<string> parts;
	split(parts, uri, is_any_of("/"));

	auto begin = parts.begin() + 3;
	auto end = parts.end();

	vector<string> rest(begin, end);

	_host = parts[2];
	if(_host.find(":") != string::npos) {
		split(parts, _host, is_any_of(":"));
		_host = parts[0];
		_service = parts[1];
	}

	_target = join(rest, "/");
	if(_target == "")
		_target = "/";

	// 2019-12-10 AMR NOTE: weird virtual thing here *technically* Connection<stream>::connect(const string &) exists so this overloads it
	Connection<stream>::connect(_host, _service);
}

// 2019-12-03 AMR TODO: add logging
void Websocket::send(string &&msg) {
	if(_connected)
		addMessage(msg);
	else
		_reqs.push(move(msg));
}

void Websocket::addMessage(const string &msg) {
	COEUS_LOG(Log, Severity::Debug) << msg;

	_stream.async_write(net::const_buffer(msg.data(), msg.size()), bind(&Websocket::onWrite, this, placeholders::_1, placeholders::_2));
}

void Websocket::onTlsHandshake(const error_code &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});

	beast::get_lowest_layer(_stream).expires_never();
	_stream.set_option(
		websocket::stream_base::timeout::suggested(
			beast::role_type::client
		)
	);
	_stream.set_option(websocket::stream_base::decorator([this](websocket::request_type &req) {
		req.set(http::field::user_agent, ctx().UserAgent());
	}));

	COEUS_LOG(Log, Severity::Debug) << "Attempting handshake to " << _host << " at " << _target;
	_stream.async_handshake(_host, _target, bind(&Websocket::onWsHandshake, this, placeholders::_1));
}

void Websocket::onWsHandshake(const error_code &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});

	_connected = true;
	auto p = _buffs.emplace(_last++, buffer{});
	assert(p.second);	// 2019-11-24 AMR NOTE: would be bad if insert didn't happen TODO: throw? or similar?
	auto buff = p.first;
	_stream.async_read(buff->second, [this, buff](const auto &a, const auto &b) {
		this->onRead(a, b, buff->first);
	});

	while(!_reqs.empty()) {
		addMessage(_reqs.front());
		_reqs.pop();
	}
}

void Websocket::onRead(const error_code &ec, const size_t &sz, const size_t &which) {
	{
		auto &buff = _buffs[which];
		// 2019-11-24 AMR TODO: cleanup this
		onMessage(ec, string(static_cast<const char *>(buff.data().data()), sz));
		_buffs.erase(which);
	}

	if(_connected)
	{
		auto p = _buffs.emplace(_last++, buffer{});
		assert(p.second);	// 2019-11-24 AMR NOTE: would be bad if insert didn't happen TODO: throw? or similar?
		auto buff = p.first;
		_stream.async_read(buff->second, [this, buff](const auto &a, const auto &b) {
			this->onRead(a, b, buff->first);
		});
	}
}

void Websocket::onWrite(const error_code &ec, const size_t &) {
	if(ec)
		COEUS_THROW(system_error{ec});
}

void Websocket::onClose(const error_code &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});

	_connected = false;
	_stream.next_layer().async_shutdown(bind(&Websocket::onShutdown, this, placeholders::_1));
}

void Websocket::onShutdown(const error_code &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});

	get_lowest_layer(_stream).close();
}
