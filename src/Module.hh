/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_MODULE_INC
#	define RPBOT_MODULE_INC

#	include "ModuleBase.hh"

	namespace RpBot
	{
		class Module :
			public ModuleBase
		{
			public:
				explicit Module(const std::string &, const std::list<std::string> &);
				virtual ~Module();
				virtual std::string command(const std::string &, const std::string &, const Payload &);
			protected:
				virtual void setCommand(const std::string &, const std::function<std::string(const std::string &, const Payload &)> &);
			protected:
				virtual void onMessageCreate(const Payload &);
			private:
				std::map<std::string, std::function<std::string(const std::string &, const Payload &)>> _commands;
		};
	}

#endif
