/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Payload.hh"

using namespace RpBot;
using namespace std;

shared_ptr<Payload> Payload::Parse(const string &s) {
	auto m = jsonParse(s);

	return shared_ptr<Payload>(new Payload(
		static_cast<OpCode>(m["op"].asInt()),
		m["d"],
		(!m["s"].isNull() ? m["s"].asInt() : optional<size_t>()),
		(!m["t"].isNull() ? m["t"].asString() : optional<string>())
	));
}

Payload::Payload() :
	Payload(
		OpCode::Dispatch,
		{},
		optional<size_t>(),
		optional<string>()
	)
{}

Payload::Payload(
	const OpCode &op,
	const Json::Value &data,
	const optional<size_t> &seq,
	const optional<string> &type
) :
	_op(op),
	_data(data),
	_seq(seq),
	_type(type)
{}

Payload::~Payload() = default;

// 2020-01-26 AMR TODO: should this use jsonWrite?
string Payload::toJson() const {
	stringstream strm;
	Json::Value m;

	m["op"] = static_cast<int>(_op);
	m["d"] = _data;

	if(_seq)
		m["s"] = *_seq;
	else
		m["s"] = Json::Value();

	if(_type)
		m["t"] = *_type;
	else
		m["t"] = Json::Value();

	strm << m;
	return strm.str();
}

const Payload::OpCode &Payload::Op() const {
	return _op;
}

const Json::Value &Payload::Data() const {
	return _data;
}

const size_t &Payload::Seq() const {
	return *_seq;
}

const string &Payload::Type() const {
	return *_type;
}

bool Payload::hasSeq() const {
	return _seq.has_value();
}

bool Payload::hasType() const {
	return _type.has_value();
}

static bool always(const Payload &) {
	return true;
}

static bool dispatch(const Payload &m) {
	return m.Op() == Payload::OpCode::Dispatch;
}

static bool heartbeat(const Payload &m) {
	return m.Op() == Payload::OpCode::Heartbeat;
}

static bool reconnect(const Payload &m) {
	return m.Op() == Payload::OpCode::Reconnect;
}

static bool invalidsession(const Payload &m) {
	return m.Op() == Payload::OpCode::InvalidSession;
}

static bool hello(const Payload &m) {
	return m.Op() == Payload::OpCode::Hello;
}

static bool heartbeatack(const Payload &m) {
	return m.Op() == Payload::OpCode::HeartbeatAck;
}

static bool ready(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "READY"s;
}

static bool resumed(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "RESUMED"s;
}


static bool channel_create(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "CHANNEL_CREATE"s;
}

static bool channel_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "CHANNEL_UPDATE"s;
}

static bool channel_delete(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "CHANNEL_DELETE"s;
}

static bool channel_pinned(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "CHANNEL_PINS_UPDATE"s;
}


static bool guild_create(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_CREATE"s;
}

static bool guild_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_UPDATE"s;
}

static bool guild_delete(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_DELETE"s;
}


static bool guild_ban_add(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_BAN_ADD"s;
}

static bool guild_ban_remove(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_BAN_REMOVE"s;
}

static bool guild_emojis_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_EMOJIS_UPDATE"s;
}

static bool guild_int_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_INTEGRATIONS_UPDATE"s;
}


static bool guild_member_add(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_MEMBER_ADD"s;
}

static bool guild_member_remove(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_MEMBER_REMOVE"s;
}

static bool guild_member_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_MEMBER_UPDATE"s;
}

static bool guild_member_chunk(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_MEMBER_CHUNK"s;
}


static bool guild_role_create(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_ROLE_CREATE"s;
}

static bool guild_role_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_ROLE_UPDATE"s;
}

static bool guild_role_delete(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "GUILD_ROLE_DELETE"s;
}


static bool message_create(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "MESSAGE_CREATE"s;
}

static bool message_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "MESSAGE_UPDATE"s;
}

static bool message_delete(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "MESSAGE_DELETE"s;
}

static bool message_delete_bulk(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "MESSAGE_DELETE_BULK"s;
}


static bool message_reaction_add(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "MESSAGE_REACTION_ADD"s;
}

static bool message_reaction_remove(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "MESSAGE_REACTION_REMOVE"s;
}

static bool message_reaction_remove_all(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "MESSAGE_REACTION_REMOVE_ALL"s;
}


static bool pressence_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "PRESSENCE_UPDATE"s;
}

static bool typing_start(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "TYPING_START"s;
}

static bool user_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "USER_UPDATE"s;
}

static bool voice_state_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "VOICE_STATE_UPDATE"s;
}

static bool voice_server_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "VOICE_SERVER_UPDATE"s;
}

static bool webhooks_update(const Payload &m) {
	return Payload::Dispatch(m) && m.hasType() && m.Type() == "WEBHOOKS_UPDATE"s;
}

Payload::Predicate Payload::Always = always;

Payload::Predicate Payload::Dispatch = dispatch;
Payload::Predicate Payload::Heartbeat = heartbeat;
Payload::Predicate Payload::Reconnect = reconnect;
Payload::Predicate Payload::InvalidSession = invalidsession;
Payload::Predicate Payload::Hello = hello;
Payload::Predicate Payload::HeartbeatAck = heartbeatack;

Payload::Predicate Payload::Ready = ready;
Payload::Predicate Payload::Resumed = resumed;

Payload::Predicate Payload::ChannelCreate = channel_create;
Payload::Predicate Payload::ChannelUpdate = channel_update;
Payload::Predicate Payload::ChannelDelete = channel_delete;
Payload::Predicate Payload::ChannelPinned = channel_pinned;

Payload::Predicate Payload::GuildCreate = guild_create;
Payload::Predicate Payload::GuildUpdate = guild_update;
Payload::Predicate Payload::GuildDelete = guild_delete;

Payload::Predicate Payload::GuildBanAdd = guild_ban_add;
Payload::Predicate Payload::GuildBanRemove = guild_ban_remove;
Payload::Predicate Payload::GuildEmojisUpdate = guild_emojis_update;
Payload::Predicate Payload::GuildIntegrationsUpdate = guild_int_update;

Payload::Predicate Payload::GuildMemberAdd = guild_member_add;
Payload::Predicate Payload::GuildMemberRemove = guild_member_remove;
Payload::Predicate Payload::GuildMemberUpdate = guild_member_update;
Payload::Predicate Payload::GuildMemberChunk = guild_member_chunk;

Payload::Predicate Payload::GuildRoleCreate = guild_role_create;
Payload::Predicate Payload::GuildRoleUpdate = guild_role_update;
Payload::Predicate Payload::GuildRoleDelete = guild_role_delete;

Payload::Predicate Payload::MessageCreate = message_create;
Payload::Predicate Payload::MessageUpdate = message_update;
Payload::Predicate Payload::MessageDelete = message_delete;
Payload::Predicate Payload::MessageDeleteBulk = message_delete_bulk;

Payload::Predicate Payload::MessageReactionAdd = message_reaction_add;
Payload::Predicate Payload::MessageReactionRemove = message_reaction_remove;
Payload::Predicate Payload::MessageReactionRemoveAll = message_reaction_remove_all;

Payload::Predicate Payload::PressenceUpdate = pressence_update;
Payload::Predicate Payload::TypingStart = typing_start;
Payload::Predicate Payload::UserUpdate = user_update;
Payload::Predicate Payload::VoiceStateUpdate = voice_state_update;
Payload::Predicate Payload::VoiceServerUpdate = voice_server_update;
Payload::Predicate Payload::WebhooksUpdate = webhooks_update;
