/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_EXCEPTION_INC
#	define RPBOT_EXCEPTION_INC

#	include <Coeus/Exception.hh>
#	include <Coeus/Log.hh>

	namespace RpBot
	{
		using Coeus::Log::Severity;

		COEUS_EXCEPTION(Exception, "RpBot Exception", Coeus::Exception);
		extern Coeus::Log::Source Log;
	}

#endif
