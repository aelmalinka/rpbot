/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_JSON_INC
#	define RPBOT_JSON_INC

#	include "Exception.hh"
#	include <json/json.h>

	namespace RpBot
	{
		COEUS_ERROR_INFO(JsonError, std::string);
		COEUS_ERROR_INFO(JsonValue, Json::Value);
		COEUS_ERROR_INFO(JsonString, Json::Value);

		Json::Value jsonParse(const std::string &);
		std::string jsonWrite(const Json::Value &);
	}

#endif
