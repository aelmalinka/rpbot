/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined RPBOT_REST_INC
#	define RPBOT_REST_INC

#	include "Http.hh"
#	include "Json.hh"
#	include <map>
#	include <tuple>

	namespace RpBot
	{
		class Rest
		{
			private:
				using callback = std::function<void(const Json::Value &)>;
				using response = Http::response;
				using tuple = std::tuple<Http, callback>;
				using map = std::map<std::size_t, tuple>;
			public:
				Rest(Context &);
				virtual ~Rest();
				virtual void operator () (
					const http::verb &,
					const std::string &,
					const callback &,
					const std::optional<std::string> & = std::optional<std::string>()
				);
				virtual void createMessage(const std::string &, const std::string &);
				virtual void deleteMessage(const std::string &, const std::string &);
			private:
				void handle_res(const response &, const std::size_t &);
			private:
				Context &_ctx;
				std::size_t _last;
				map _reqs;
		};
	}

#endif
