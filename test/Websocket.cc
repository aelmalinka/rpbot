/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../src/Websocket.hh"

using namespace testing;
using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class TestWebsocket :
		public Websocket
	{
		public:
			TestWebsocket(Context &ctx) :
				Websocket(ctx)
			{}
			void onMessage(const RpBot::error_code &ec, const std::string &msg) {
				if(ec)
					FAIL() << system_error{ec};
				else
					SUCCEED() << msg;

				disconnect();
			}
	};

	TEST(WebsocketTests, EchoClient) {
		TEST_BEGIN
		Context ctx("a random token");
		TestWebsocket w(ctx);
		COEUS_LOG(Log, Coeus::Severity::Debug) << "constructed trying to connect";
		w.connect("ws://echo.websocket.org/");
		COEUS_LOG(Log, Coeus::Severity::Debug) << "connected trying to send";
		w.send("This is a test");
		ctx.io().run();
		TEST_END
	}
}
