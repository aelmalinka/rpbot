/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/Module.hh"

using namespace std;
using namespace testing;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class TestModuleBase :
		public ModuleBase
	{
		public:
			TestModuleBase() :
				ModuleBase("Test"s, {})
			{}
			string command(const string &, const string &, const Payload &) {
				return ""s;
			}
			const string &getName() const {
				return name();
			}
			const string &getPrefix() const {
				return prefix();
			}
			Rest &getRest() {
				return rest();
			}
			Gateway &getGateway() {
				return gate();
			}
			Application &getApplication() {
				return app();
			}
	};

	TEST(ModuleBaseTests, Name) {
		TestModuleBase b;

		EXPECT_EQ(b.getName(), "Test"s);
	}

	TEST(ModuleBaseTests, Prefix) {
		TestModuleBase b;

		string pref = "asdf"s;
		ASSERT_NE(RPBOT_DEFAULT_PREFIX, pref);
		EXPECT_NE(b.getPrefix(), pref);
		b.setPrefix(pref);
		EXPECT_EQ(b.getPrefix(), pref);
	}

	TEST(ModuleBaseTests, Rest) {
		TestModuleBase b;
		Context ctx("an invalid token");
		auto r = make_shared<Rest>(ctx);

		b.setRest(r);

		EXPECT_EQ(&b.getRest(), r.get());
	}

	TEST(ModuleBaseTests, Gateway) {
		TestModuleBase b;
		Context ctx("an invalid token");
		auto g = make_shared<Gateway>(ctx, "ws://invalid.invalid/"s);

		b.setGate(g);

		EXPECT_EQ(&b.getGateway(), g.get());
	}

	TEST(ModuleBaseTests, Application) {
		TestModuleBase b;
		vector<const char *> v = {
			"NAME",
		};
		Application app(v.size() - 1, v.data());

		b.setApplication(&app);

		EXPECT_EQ(&b.getApplication(), &app);
	}

	class TestModule :
		public Module
	{
		public:
			TestModule() :
				Module("Test"s, {})
			{
				using namespace std::placeholders;
				setCommand("echo"s, bind(&TestModule::echo, this, _1, _2));
			}
			string echo(const string &args, const Payload &) {
				return args;
			}
			void onMessageCreate(const Payload &p) {
				Module::onMessageCreate(p);
			}
	};

	TEST(ModuleTests, Command) {
		TEST_BEGIN
		TestModule m;

		auto req = "asdf"s;
		auto msg = Payload::Parse(R"({"op":0,"d":null})");
		auto res = m.command("echo"s, req, *msg);

		EXPECT_EQ(req, res);
		TEST_END
	}

	class RestMock
		: public Rest
	{
		public:
			RestMock(Context &ctx) :
				Rest(ctx)
			{}
			MOCK_METHOD(void, createMessage, (const string &, const string &), (override));
	};

	TEST(ModuleTests, Message) {
		TEST_BEGIN
		// 2020-01-04 AMR TODO: mock
		TestModule m;
		Context ctx("oogyboogy");
		auto r = make_shared<StrictMock<RestMock>>(ctx);

		m.setRest(r);

		auto req = "asdf"s;
		auto cmd = RPBOT_DEFAULT_PREFIX + "Test echo "s + req;
		// 2020-01-04 AMR TODO: use Payload::OpCode enum?
		auto msg = Payload::Parse(R"({"op":0,"t":"MESSAGE_CREATE","d":{"channel_id":"achannelid","content":")" + cmd + "\"}}");

		EXPECT_CALL(*r, createMessage(Eq(req), _))
			.Times(1);

		m.onMessageCreate(*msg);
		TEST_END
	}
}
