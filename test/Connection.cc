/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Connection.hh"

using namespace testing;
using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class TestConnection :
		public Connection<>
	{
		public:
			TestConnection(Context &ctx) :
				Connection<>(ctx)
			{}
			void disconnect() {}
		protected:
			void onTlsHandshake(const RpBot::error_code &ec) override {
				if(ec)
					FAIL() << system_error{ec};
				else
					SUCCEED() << "connected";
			}
	};

	TEST(ConnectionTests, Construct) {
		TEST_BEGIN
		Context ctx("an invalid token");
		TestConnection con(ctx);

		ctx.io().run();
		TEST_END
	}
	TEST(ConnectionTests, Connect) {
		TEST_BEGIN
		Context ctx("an invalid token");
		TestConnection con(ctx);
		con.connect("google.com");

		ctx.io().run();
		TEST_END
	}
}
