/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Context.hh"
#include <cctype>

using namespace testing;
using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(ContextTests, Create) {
		TEST_BEGIN
		Context ctx("some random token");

		EXPECT_NE(ctx.Token(), ""s);
		EXPECT_NE(ctx.Base(), ""s);
		EXPECT_NE(ctx.UserAgent(), ""s);	// 2019-11-20 AMR TODO: verify discords reqs and test for them to satisfied?

		for(auto &c : ctx.GatewayVersion()) {
			EXPECT_TRUE(isdigit(c));
		}

		// 2019-11-20 AMR TODO: test for functionality not just existance
		EXPECT_NE(ctx.Host(), ""s);
		EXPECT_NE(ctx.Service(), ""s);
		TEST_END
	}
}
