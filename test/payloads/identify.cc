/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>'
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../../src/Payloads/Identify.hh"

using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(IdentifyTests, CreateDefault) {
		TEST_BEGIN
		Context ctx("an invalid token");
		Identify m(ctx);

		EXPECT_EQ(m.Op(), Payload::OpCode::Identify);
		EXPECT_EQ(m.Data()["token"].asString(), ctx.Token());
		TEST_END
	}
}
