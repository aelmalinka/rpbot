/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>'
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../../src/Payloads/Resume.hh"

using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(ResumeTests, CreateDefault) {
		TEST_BEGIN
		const int seq = 42;
		const string id = "some invlid session";
		Context ctx("an invalid token");
		Resume m(ctx, id, seq);

		EXPECT_EQ(m.Op(), Payload::OpCode::Resume);
		EXPECT_EQ(m.Data()["token"].asString(), ctx.Token());
		EXPECT_EQ(m.Data()["session_id"].asString(), id);
		EXPECT_EQ(m.Data()["seq"].asInt(), seq);
		TEST_END
	}
}
