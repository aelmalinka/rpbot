/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>'
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../../src/Payloads/StatusUpdate.hh"

using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(StatusUpdateTests, CreateDefault) {
		TEST_BEGIN
		StatusUpdate m;

		EXPECT_EQ(m.Op(), Payload::OpCode::StatusUpdate);
		TEST_END
	}
}
