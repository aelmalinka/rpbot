/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>'
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../../src/Payloads/RequestGuildMembers.hh"

using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(RequestGuildMembersTests, CreateDefault) {
		TEST_BEGIN
		RequestGuildMembers m;

		EXPECT_EQ(m.Op(), Payload::OpCode::RequestGuildMembers);
		TEST_END
	}
}
