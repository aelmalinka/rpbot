/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>'
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../../src/Payloads/Heartbeat.hh"

using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(HeartbeatTests, CreateDefault) {
		TEST_BEGIN
		Heartbeat m;

		EXPECT_EQ(m.Op(), Payload::OpCode::Heartbeat);
		EXPECT_FALSE(m.hasSeq());
		TEST_END
	}

	TEST(HeartbeatTests, CreateSeq) {
		TEST_BEGIN
		const size_t seq = 76;
		Heartbeat m(seq);

		ASSERT_TRUE(m.hasSeq());

		EXPECT_EQ(m.Op(), Payload::OpCode::Heartbeat);
		EXPECT_EQ(m.Seq(), seq);
		TEST_END
	}
}
