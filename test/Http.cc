/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Http.hh"

using namespace testing;
using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(HttpTests, Basic) {
		TEST_BEGIN
		Context ctx(
			"an invlaid token",
			"localhost",
			"8080"
		);
		ctx.ssl().load_verify_file("ca.crt");
		Http h(ctx);

		h(
			http::verb::get,
			"/",
			[](const auto &) {
				SUCCEED();
			}
		);

		ctx.io().run();
		TEST_END
	}

	TEST(HttpTests, Twice) {
		TEST_BEGIN
		Context ctx(
			"an invlaid token",
			"localhost",
			"8080"
		);
		ctx.ssl().load_verify_file("ca.crt");
		Http h(ctx);
		Http a(ctx);

		h(
			http::verb::get,
			"/",
			[](const auto &) {
			}
		);
		a(
			http::verb::get,
			"/",
			[](const auto &){
				SUCCEED();
			}
		);

		ctx.io().run();
		TEST_END
	}
}
