/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../src/Payload.hh"
#include <sstream>

using namespace std;

using ::testing::Test;
using ::testing::TestWithParam;
using ::testing::ValuesIn;

using ::RpBot::Payload;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class TestPayload :
		public Payload
	{
		public:
			TestPayload(
				const OpCode &op,
				const Json::Value &d = Json::Value(),
				const optional<size_t> &s = optional<size_t>(),
				const optional<string> &t = optional<string>()
			) :
				Payload(op, d, s, t)
			{}
	};

	TEST(PayloadTests, DataNull) {
		TEST_BEGIN
		TestPayload m(Payload::OpCode::Dispatch);

		EXPECT_TRUE(m.Data().isNull());
		TEST_END
	}

	TEST(PayloadTests, DataInt) {
		TEST_BEGIN
		const int secret = 42;
		TestPayload m(Payload::OpCode::Dispatch, secret);

		EXPECT_EQ(secret, m.Data().asInt());
		TEST_END
	}

	TEST(PayloadTests, DataJson) {
		TEST_BEGIN
		const int secret = 42;
		const string msg = "hello";
		Json::Value j;

		j["secret"] = secret;
		j["msg"] = msg;

		TestPayload m(Payload::OpCode::Dispatch, j);

		EXPECT_EQ(m.Data()["secret"].asInt(), secret);
		EXPECT_EQ(m.Data()["msg"].asString(), msg);
		TEST_END
	}

	TEST(PayloadTests, NoSeq) {
		TEST_BEGIN
		TestPayload m(Payload::OpCode::Dispatch);

		EXPECT_FALSE(m.hasSeq());
		TEST_END
	}

	TEST(PayloadTests, Seq) {
		TEST_BEGIN
		const size_t seq = 67;
		TestPayload m(Payload::OpCode::Dispatch, {}, seq);

		EXPECT_TRUE(m.hasSeq());
		EXPECT_EQ(m.Seq(), seq);
		TEST_END
	}

	TEST(PayloadTests, NoType) {
		TEST_BEGIN
		TestPayload m(Payload::OpCode::Dispatch);

		EXPECT_FALSE(m.hasType());
		TEST_END
	}

	TEST(PayloadTests, Type) {
		TEST_BEGIN
		const string type = "EVENT_NAME";
		TestPayload m(Payload::OpCode::Dispatch, {}, {}, type);

		EXPECT_TRUE(m.hasType());
		EXPECT_EQ(m.Type(), type);
		TEST_END
	}

	TEST(PayloadTests, Parse) {
		TEST_BEGIN
		Json::Value v;
		Json::Value d;

		const int secret = 67;
		const string msg = "hello world";
		const Payload::OpCode op = Payload::OpCode::Dispatch;
		const size_t s = 42;
		const string t = "EVENT_NAME";
		d["secret"] = secret;
		d["message"] = msg;

		v["op"] = static_cast<int>(op);
		v["d"] = d;
		v["s"] = s;
		v["t"] = t;

		stringstream strm;
		strm << v;
		auto m = Payload::Parse(strm.str());

		ASSERT_TRUE(m->hasSeq());
		ASSERT_TRUE(m->hasType());

		EXPECT_EQ(m->Op(), op);
		EXPECT_EQ(m->Data(), d);	// 2019-12-20 AMR NOTE: requires == on Json::Value
		EXPECT_EQ(m->Data()["secret"], secret);
		EXPECT_EQ(m->Data()["message"], msg);
		EXPECT_EQ(m->Seq(), s);
		EXPECT_EQ(m->Type(), t);
		TEST_END
	}

	TEST(PayloadTests, String) {
		TEST_BEGIN
		Json::Value d;

		const Payload::OpCode op = Payload::OpCode::Dispatch;
		const int secret = 42;
		const string msg = "Hello World!";
		const size_t seq = 67;
		const string type = "EVENT_NAME";

		d["secret"] = secret;
		d["message"] = msg;

		auto m1 = make_shared<TestPayload>(op, d, seq, type);
		auto m2 = Payload::Parse(m1->toJson());

		ASSERT_TRUE(m1->hasSeq());
		ASSERT_TRUE(m1->hasType());
		ASSERT_TRUE(m2->hasSeq());
		ASSERT_TRUE(m2->hasType());

		EXPECT_EQ(m1->Op(), m2->Op());
		EXPECT_EQ(m1->Data(), m2->Data());
		EXPECT_EQ(m1->Seq(), m2->Seq());
		EXPECT_EQ(m1->Type(), m2->Type());

		EXPECT_EQ(m2->Op(), op);
		EXPECT_EQ(m2->Data(), d);
		EXPECT_EQ(m2->Seq(), seq);
		EXPECT_EQ(m2->Type(), type);

		EXPECT_EQ(m2->Data()["secret"].asInt(), secret);
		EXPECT_EQ(m2->Data()["message"].asString(), msg);
		TEST_END
	}

	class OpCodeTests :
		public TestWithParam<Payload::OpCode>
	{};

	TEST_P(OpCodeTests, CreateSame) {
		TEST_BEGIN
		TestPayload m(GetParam());

		EXPECT_EQ(m.Op(), GetParam());
		TEST_END
	}

	const vector<Payload::OpCode> Ops = {
		Payload::OpCode::Dispatch,
		Payload::OpCode::Heartbeat,
		Payload::OpCode::Identify,
		Payload::OpCode::StatusUpdate,
		Payload::OpCode::VoiceStateUpdate,
		Payload::OpCode::Resume,
		Payload::OpCode::Reconnect,
		Payload::OpCode::RequestGuildMembers,
		Payload::OpCode::InvalidSession,
		Payload::OpCode::Hello,
		Payload::OpCode::HeartbeatAck
	};
	INSTANTIATE_TEST_SUITE_P(OpCodes, OpCodeTests, ValuesIn(Ops));

	class PayloadPredicateTests :
		public Test
	{
		public:
			PayloadPredicateTests() :
				dispatch(Payload::OpCode::Dispatch),
				heartbeat(Payload::OpCode::Heartbeat),
				identify(Payload::OpCode::Identify),
				status(Payload::OpCode::StatusUpdate),
				voice(Payload::OpCode::VoiceStateUpdate),
				resume(Payload::OpCode::Resume),
				reconnect(Payload::OpCode::Reconnect),
				requestmembers(Payload::OpCode::RequestGuildMembers),
				invalidsession(Payload::OpCode::InvalidSession),
				hello(Payload::OpCode::Hello),
				heartbeatack(Payload::OpCode::HeartbeatAck)
			{}
		protected:
			TestPayload dispatch;
			TestPayload heartbeat;
			TestPayload identify;
			TestPayload status;
			TestPayload voice;
			TestPayload resume;
			TestPayload reconnect;
			TestPayload requestmembers;
			TestPayload invalidsession;
			TestPayload hello;
			TestPayload heartbeatack;
	};

	TEST_F(PayloadPredicateTests, Always) {
		TEST_BEGIN
		EXPECT_TRUE(Payload::Always(dispatch));
		EXPECT_TRUE(Payload::Always(heartbeat));
		EXPECT_TRUE(Payload::Always(identify));
		EXPECT_TRUE(Payload::Always(status));
		EXPECT_TRUE(Payload::Always(voice));
		EXPECT_TRUE(Payload::Always(resume));
		EXPECT_TRUE(Payload::Always(reconnect));
		EXPECT_TRUE(Payload::Always(requestmembers));
		EXPECT_TRUE(Payload::Always(invalidsession));
		EXPECT_TRUE(Payload::Always(hello));
		EXPECT_TRUE(Payload::Always(heartbeatack));
		TEST_END
	}

	TEST_F(PayloadPredicateTests, Dispatch) {
		TEST_BEGIN
		EXPECT_TRUE(Payload::Dispatch(dispatch));
		EXPECT_FALSE(Payload::Dispatch(heartbeat));
		EXPECT_FALSE(Payload::Dispatch(identify));
		EXPECT_FALSE(Payload::Dispatch(status));
		EXPECT_FALSE(Payload::Dispatch(voice));
		EXPECT_FALSE(Payload::Dispatch(resume));
		EXPECT_FALSE(Payload::Dispatch(reconnect));
		EXPECT_FALSE(Payload::Dispatch(requestmembers));
		EXPECT_FALSE(Payload::Dispatch(invalidsession));
		EXPECT_FALSE(Payload::Dispatch(hello));
		EXPECT_FALSE(Payload::Dispatch(heartbeatack));
		TEST_END
	}

	TEST_F(PayloadPredicateTests, Heartbeat) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::Heartbeat(dispatch));
		EXPECT_TRUE(Payload::Heartbeat(heartbeat));
		EXPECT_FALSE(Payload::Heartbeat(identify));
		EXPECT_FALSE(Payload::Heartbeat(status));
		EXPECT_FALSE(Payload::Heartbeat(voice));
		EXPECT_FALSE(Payload::Heartbeat(resume));
		EXPECT_FALSE(Payload::Heartbeat(reconnect));
		EXPECT_FALSE(Payload::Heartbeat(requestmembers));
		EXPECT_FALSE(Payload::Heartbeat(invalidsession));
		EXPECT_FALSE(Payload::Heartbeat(hello));
		EXPECT_FALSE(Payload::Heartbeat(heartbeatack));
		TEST_END
	}

	TEST_F(PayloadPredicateTests, Reconnect) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::Reconnect(dispatch));
		EXPECT_FALSE(Payload::Reconnect(heartbeat));
		EXPECT_FALSE(Payload::Reconnect(identify));
		EXPECT_FALSE(Payload::Reconnect(status));
		EXPECT_FALSE(Payload::Reconnect(voice));
		EXPECT_FALSE(Payload::Reconnect(resume));
		EXPECT_TRUE(Payload::Reconnect(reconnect));
		EXPECT_FALSE(Payload::Reconnect(requestmembers));
		EXPECT_FALSE(Payload::Reconnect(invalidsession));
		EXPECT_FALSE(Payload::Reconnect(hello));
		EXPECT_FALSE(Payload::Reconnect(heartbeatack));
		TEST_END
	}

	TEST_F(PayloadPredicateTests, InvalidSession) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::InvalidSession(dispatch));
		EXPECT_FALSE(Payload::InvalidSession(heartbeat));
		EXPECT_FALSE(Payload::InvalidSession(identify));
		EXPECT_FALSE(Payload::InvalidSession(status));
		EXPECT_FALSE(Payload::InvalidSession(voice));
		EXPECT_FALSE(Payload::InvalidSession(resume));
		EXPECT_FALSE(Payload::InvalidSession(reconnect));
		EXPECT_FALSE(Payload::InvalidSession(requestmembers));
		EXPECT_TRUE(Payload::InvalidSession(invalidsession));
		EXPECT_FALSE(Payload::InvalidSession(hello));
		EXPECT_FALSE(Payload::InvalidSession(heartbeatack));
		TEST_END
	}

	TEST_F(PayloadPredicateTests, Hello) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::Hello(dispatch));
		EXPECT_FALSE(Payload::Hello(heartbeat));
		EXPECT_FALSE(Payload::Hello(identify));
		EXPECT_FALSE(Payload::Hello(status));
		EXPECT_FALSE(Payload::Hello(voice));
		EXPECT_FALSE(Payload::Hello(resume));
		EXPECT_FALSE(Payload::Hello(reconnect));
		EXPECT_FALSE(Payload::Hello(requestmembers));
		EXPECT_FALSE(Payload::Hello(invalidsession));
		EXPECT_TRUE(Payload::Hello(hello));
		EXPECT_FALSE(Payload::Hello(heartbeatack));
		TEST_END
	}

	TEST_F(PayloadPredicateTests, HeartbeatAck) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::HeartbeatAck(dispatch));
		EXPECT_FALSE(Payload::HeartbeatAck(heartbeat));
		EXPECT_FALSE(Payload::HeartbeatAck(identify));
		EXPECT_FALSE(Payload::HeartbeatAck(status));
		EXPECT_FALSE(Payload::HeartbeatAck(voice));
		EXPECT_FALSE(Payload::HeartbeatAck(resume));
		EXPECT_FALSE(Payload::HeartbeatAck(reconnect));
		EXPECT_FALSE(Payload::HeartbeatAck(requestmembers));
		EXPECT_FALSE(Payload::HeartbeatAck(invalidsession));
		EXPECT_FALSE(Payload::HeartbeatAck(hello));
		EXPECT_TRUE(Payload::HeartbeatAck(heartbeatack));
		TEST_END
	}

	class EventPredicateTests :
		public Test
	{
		public:
			EventPredicateTests() :
				ready(Payload::OpCode::Dispatch, {}, {}, "READY"),
				resumed(Payload::OpCode::Dispatch, {}, {}, "RESUMED"),
				channel_create(Payload::OpCode::Dispatch, {}, {}, "CHANNEL_CREATE"),
				channel_update(Payload::OpCode::Dispatch, {}, {}, "CHANNEL_UPDATE"),
				channel_delete(Payload::OpCode::Dispatch, {}, {}, "CHANNEL_DELETE"),
				channel_pinned(Payload::OpCode::Dispatch, {}, {}, "CHANNEL_PINS_UPDATE"),
				guild_create(Payload::OpCode::Dispatch, {}, {}, "GUILD_CREATE"),
				guild_update(Payload::OpCode::Dispatch, {}, {}, "GUILD_UPDATE"),
				guild_delete(Payload::OpCode::Dispatch, {}, {}, "GUILD_DELETE"),
				guild_ban_add(Payload::OpCode::Dispatch, {}, {}, "GUILD_BAN_ADD"),
				guild_ban_remove(Payload::OpCode::Dispatch, {}, {}, "GUILD_BAN_REMOVE"),
				guild_emojis_update(Payload::OpCode::Dispatch, {}, {}, "GUILD_EMOJIS_UPDATE"),
				guild_integrations_update(Payload::OpCode::Dispatch, {}, {}, "GUILD_INTEGRATIONS_UPDATE"),
				guild_member_add(Payload::OpCode::Dispatch, {}, {}, "GUILD_MEMBER_ADD"),
				guild_member_remove(Payload::OpCode::Dispatch, {}, {}, "GUILD_MEMBER_REMOVE"),
				guild_member_update(Payload::OpCode::Dispatch, {}, {}, "GUILD_MEMBER_UPDATE"),
				guild_member_chunk(Payload::OpCode::Dispatch, {}, {}, "GUILD_MEMBER_CHUNK"),
				role_create(Payload::OpCode::Dispatch, {}, {}, "GUILD_ROLE_CREATE"),
				role_update(Payload::OpCode::Dispatch, {}, {}, "GUILD_ROLE_UPDATE"),
				role_delete(Payload::OpCode::Dispatch, {}, {}, "GUILD_ROLE_DELETE"),
				message_create(Payload::OpCode::Dispatch, {}, {}, "MESSAGE_CREATE"),
				message_update(Payload::OpCode::Dispatch, {}, {}, "MESSAGE_UPDATE"),
				message_delete(Payload::OpCode::Dispatch, {}, {}, "MESSAGE_DELETE"),
				message_delete_bulk(Payload::OpCode::Dispatch, {}, {}, "MESSAGE_DELETE_BULK"),
				message_reaction_add(Payload::OpCode::Dispatch, {}, {}, "MESSAGE_REACTION_ADD"),
				message_reaction_remove(Payload::OpCode::Dispatch, {}, {}, "MESSAGE_REACTION_REMOVE"),
				message_reaction_remove_all(Payload::OpCode::Dispatch, {}, {}, "MESSAGE_REACTION_REMOVE_ALL"),
				pressence_update(Payload::OpCode::Dispatch, {}, {}, "PRESSENCE_UPDATE"),
				typing_start(Payload::OpCode::Dispatch, {}, {}, "TYPING_START"),
				user_update(Payload::OpCode::Dispatch, {}, {}, "USER_UPDATE"),
				voice_state_update(Payload::OpCode::Dispatch, {}, {}, "VOICE_STATE_UPDATE"),
				voice_server_update(Payload::OpCode::Dispatch, {}, {}, "VOICE_SERVER_UPDATE"),
				webhooks_update(Payload::OpCode::Dispatch, {}, {}, "WEBHOOKS_UPDATE")
			{}
		protected:
			TestPayload ready;
			TestPayload resumed;
			TestPayload channel_create;
			TestPayload channel_update;
			TestPayload channel_delete;
			TestPayload channel_pinned;
			TestPayload guild_create;
			TestPayload guild_update;
			TestPayload guild_delete;
			TestPayload guild_ban_add;
			TestPayload guild_ban_remove;
			TestPayload guild_emojis_update;
			TestPayload guild_integrations_update;
			TestPayload guild_member_add;
			TestPayload guild_member_remove;
			TestPayload guild_member_update;
			TestPayload guild_member_chunk;
			TestPayload role_create;
			TestPayload role_update;
			TestPayload role_delete;
			TestPayload message_create;
			TestPayload message_update;
			TestPayload message_delete;
			TestPayload message_delete_bulk;
			TestPayload message_reaction_add;
			TestPayload message_reaction_remove;
			TestPayload message_reaction_remove_all;
			TestPayload pressence_update;
			TestPayload typing_start;
			TestPayload user_update;
			TestPayload voice_state_update;
			TestPayload voice_server_update;
			TestPayload webhooks_update;
	};

	TEST_F(EventPredicateTests, Ready) {
		TEST_BEGIN
		EXPECT_TRUE(Payload::Ready(ready));
		EXPECT_FALSE(Payload::Ready(resumed));
		EXPECT_FALSE(Payload::Ready(channel_create));
		EXPECT_FALSE(Payload::Ready(channel_update));
		EXPECT_FALSE(Payload::Ready(channel_delete));
		EXPECT_FALSE(Payload::Ready(channel_pinned));
		EXPECT_FALSE(Payload::Ready(guild_create));
		EXPECT_FALSE(Payload::Ready(guild_update));
		EXPECT_FALSE(Payload::Ready(guild_delete));
		EXPECT_FALSE(Payload::Ready(guild_ban_add));
		EXPECT_FALSE(Payload::Ready(guild_ban_remove));
		EXPECT_FALSE(Payload::Ready(guild_emojis_update));
		EXPECT_FALSE(Payload::Ready(guild_integrations_update));
		EXPECT_FALSE(Payload::Ready(guild_member_add));
		EXPECT_FALSE(Payload::Ready(guild_member_remove));
		EXPECT_FALSE(Payload::Ready(guild_member_update));
		EXPECT_FALSE(Payload::Ready(guild_member_chunk));
		EXPECT_FALSE(Payload::Ready(role_create));
		EXPECT_FALSE(Payload::Ready(role_update));
		EXPECT_FALSE(Payload::Ready(role_delete));
		EXPECT_FALSE(Payload::Ready(message_create));
		EXPECT_FALSE(Payload::Ready(message_update));
		EXPECT_FALSE(Payload::Ready(message_delete));
		EXPECT_FALSE(Payload::Ready(message_delete_bulk));
		EXPECT_FALSE(Payload::Ready(message_reaction_add));
		EXPECT_FALSE(Payload::Ready(message_reaction_remove));
		EXPECT_FALSE(Payload::Ready(message_reaction_remove_all));
		EXPECT_FALSE(Payload::Ready(pressence_update));
		EXPECT_FALSE(Payload::Ready(typing_start));
		EXPECT_FALSE(Payload::Ready(user_update));
		EXPECT_FALSE(Payload::Ready(voice_state_update));
		EXPECT_FALSE(Payload::Ready(voice_server_update));
		EXPECT_FALSE(Payload::Ready(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, Resumed) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::Resumed(ready));
		EXPECT_TRUE(Payload::Resumed(resumed));
		EXPECT_FALSE(Payload::Resumed(channel_create));
		EXPECT_FALSE(Payload::Resumed(channel_update));
		EXPECT_FALSE(Payload::Resumed(channel_delete));
		EXPECT_FALSE(Payload::Resumed(channel_pinned));
		EXPECT_FALSE(Payload::Resumed(guild_create));
		EXPECT_FALSE(Payload::Resumed(guild_update));
		EXPECT_FALSE(Payload::Resumed(guild_delete));
		EXPECT_FALSE(Payload::Resumed(guild_ban_add));
		EXPECT_FALSE(Payload::Resumed(guild_ban_remove));
		EXPECT_FALSE(Payload::Resumed(guild_emojis_update));
		EXPECT_FALSE(Payload::Resumed(guild_integrations_update));
		EXPECT_FALSE(Payload::Resumed(guild_member_add));
		EXPECT_FALSE(Payload::Resumed(guild_member_remove));
		EXPECT_FALSE(Payload::Resumed(guild_member_update));
		EXPECT_FALSE(Payload::Resumed(guild_member_chunk));
		EXPECT_FALSE(Payload::Resumed(role_create));
		EXPECT_FALSE(Payload::Resumed(role_update));
		EXPECT_FALSE(Payload::Resumed(role_delete));
		EXPECT_FALSE(Payload::Resumed(message_create));
		EXPECT_FALSE(Payload::Resumed(message_update));
		EXPECT_FALSE(Payload::Resumed(message_delete));
		EXPECT_FALSE(Payload::Resumed(message_delete_bulk));
		EXPECT_FALSE(Payload::Resumed(message_reaction_add));
		EXPECT_FALSE(Payload::Resumed(message_reaction_remove));
		EXPECT_FALSE(Payload::Resumed(message_reaction_remove_all));
		EXPECT_FALSE(Payload::Resumed(pressence_update));
		EXPECT_FALSE(Payload::Resumed(typing_start));
		EXPECT_FALSE(Payload::Resumed(user_update));
		EXPECT_FALSE(Payload::Resumed(voice_state_update));
		EXPECT_FALSE(Payload::Resumed(voice_server_update));
		EXPECT_FALSE(Payload::Resumed(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, ChannelCreate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::ChannelCreate(ready));
		EXPECT_FALSE(Payload::ChannelCreate(resumed));
		EXPECT_TRUE(Payload::ChannelCreate(channel_create));
		EXPECT_FALSE(Payload::ChannelCreate(channel_update));
		EXPECT_FALSE(Payload::ChannelCreate(channel_delete));
		EXPECT_FALSE(Payload::ChannelCreate(channel_pinned));
		EXPECT_FALSE(Payload::ChannelCreate(guild_create));
		EXPECT_FALSE(Payload::ChannelCreate(guild_update));
		EXPECT_FALSE(Payload::ChannelCreate(guild_delete));
		EXPECT_FALSE(Payload::ChannelCreate(guild_ban_add));
		EXPECT_FALSE(Payload::ChannelCreate(guild_ban_remove));
		EXPECT_FALSE(Payload::ChannelCreate(guild_emojis_update));
		EXPECT_FALSE(Payload::ChannelCreate(guild_integrations_update));
		EXPECT_FALSE(Payload::ChannelCreate(guild_member_add));
		EXPECT_FALSE(Payload::ChannelCreate(guild_member_remove));
		EXPECT_FALSE(Payload::ChannelCreate(guild_member_update));
		EXPECT_FALSE(Payload::ChannelCreate(guild_member_chunk));
		EXPECT_FALSE(Payload::ChannelCreate(role_create));
		EXPECT_FALSE(Payload::ChannelCreate(role_update));
		EXPECT_FALSE(Payload::ChannelCreate(role_delete));
		EXPECT_FALSE(Payload::ChannelCreate(message_create));
		EXPECT_FALSE(Payload::ChannelCreate(message_update));
		EXPECT_FALSE(Payload::ChannelCreate(message_delete));
		EXPECT_FALSE(Payload::ChannelCreate(message_delete_bulk));
		EXPECT_FALSE(Payload::ChannelCreate(message_reaction_add));
		EXPECT_FALSE(Payload::ChannelCreate(message_reaction_remove));
		EXPECT_FALSE(Payload::ChannelCreate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::ChannelCreate(pressence_update));
		EXPECT_FALSE(Payload::ChannelCreate(typing_start));
		EXPECT_FALSE(Payload::ChannelCreate(user_update));
		EXPECT_FALSE(Payload::ChannelCreate(voice_state_update));
		EXPECT_FALSE(Payload::ChannelCreate(voice_server_update));
		EXPECT_FALSE(Payload::ChannelCreate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, ChannelUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::ChannelUpdate(ready));
		EXPECT_FALSE(Payload::ChannelUpdate(resumed));
		EXPECT_FALSE(Payload::ChannelUpdate(channel_create));
		EXPECT_TRUE(Payload::ChannelUpdate(channel_update));
		EXPECT_FALSE(Payload::ChannelUpdate(channel_delete));
		EXPECT_FALSE(Payload::ChannelUpdate(channel_pinned));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_create));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_update));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_delete));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_member_add));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_member_update));
		EXPECT_FALSE(Payload::ChannelUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::ChannelUpdate(role_create));
		EXPECT_FALSE(Payload::ChannelUpdate(role_update));
		EXPECT_FALSE(Payload::ChannelUpdate(role_delete));
		EXPECT_FALSE(Payload::ChannelUpdate(message_create));
		EXPECT_FALSE(Payload::ChannelUpdate(message_update));
		EXPECT_FALSE(Payload::ChannelUpdate(message_delete));
		EXPECT_FALSE(Payload::ChannelUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::ChannelUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::ChannelUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::ChannelUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::ChannelUpdate(pressence_update));
		EXPECT_FALSE(Payload::ChannelUpdate(typing_start));
		EXPECT_FALSE(Payload::ChannelUpdate(user_update));
		EXPECT_FALSE(Payload::ChannelUpdate(voice_state_update));
		EXPECT_FALSE(Payload::ChannelUpdate(voice_server_update));
		EXPECT_FALSE(Payload::ChannelUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, ChannelDelete) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::ChannelDelete(ready));
		EXPECT_FALSE(Payload::ChannelDelete(resumed));
		EXPECT_FALSE(Payload::ChannelDelete(channel_create));
		EXPECT_FALSE(Payload::ChannelDelete(channel_update));
		EXPECT_TRUE(Payload::ChannelDelete(channel_delete));
		EXPECT_FALSE(Payload::ChannelDelete(channel_pinned));
		EXPECT_FALSE(Payload::ChannelDelete(guild_create));
		EXPECT_FALSE(Payload::ChannelDelete(guild_update));
		EXPECT_FALSE(Payload::ChannelDelete(guild_delete));
		EXPECT_FALSE(Payload::ChannelDelete(guild_ban_add));
		EXPECT_FALSE(Payload::ChannelDelete(guild_ban_remove));
		EXPECT_FALSE(Payload::ChannelDelete(guild_emojis_update));
		EXPECT_FALSE(Payload::ChannelDelete(guild_integrations_update));
		EXPECT_FALSE(Payload::ChannelDelete(guild_member_add));
		EXPECT_FALSE(Payload::ChannelDelete(guild_member_remove));
		EXPECT_FALSE(Payload::ChannelDelete(guild_member_update));
		EXPECT_FALSE(Payload::ChannelDelete(guild_member_chunk));
		EXPECT_FALSE(Payload::ChannelDelete(role_create));
		EXPECT_FALSE(Payload::ChannelDelete(role_update));
		EXPECT_FALSE(Payload::ChannelDelete(role_delete));
		EXPECT_FALSE(Payload::ChannelDelete(message_create));
		EXPECT_FALSE(Payload::ChannelDelete(message_update));
		EXPECT_FALSE(Payload::ChannelDelete(message_delete));
		EXPECT_FALSE(Payload::ChannelDelete(message_delete_bulk));
		EXPECT_FALSE(Payload::ChannelDelete(message_reaction_add));
		EXPECT_FALSE(Payload::ChannelDelete(message_reaction_remove));
		EXPECT_FALSE(Payload::ChannelDelete(message_reaction_remove_all));
		EXPECT_FALSE(Payload::ChannelDelete(pressence_update));
		EXPECT_FALSE(Payload::ChannelDelete(typing_start));
		EXPECT_FALSE(Payload::ChannelDelete(user_update));
		EXPECT_FALSE(Payload::ChannelDelete(voice_state_update));
		EXPECT_FALSE(Payload::ChannelDelete(voice_server_update));
		EXPECT_FALSE(Payload::ChannelDelete(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, ChannelPinned) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::ChannelPinned(ready));
		EXPECT_FALSE(Payload::ChannelPinned(resumed));
		EXPECT_FALSE(Payload::ChannelPinned(channel_create));
		EXPECT_FALSE(Payload::ChannelPinned(channel_update));
		EXPECT_FALSE(Payload::ChannelPinned(channel_delete));
		EXPECT_TRUE(Payload::ChannelPinned(channel_pinned));
		EXPECT_FALSE(Payload::ChannelPinned(guild_create));
		EXPECT_FALSE(Payload::ChannelPinned(guild_update));
		EXPECT_FALSE(Payload::ChannelPinned(guild_delete));
		EXPECT_FALSE(Payload::ChannelPinned(guild_ban_add));
		EXPECT_FALSE(Payload::ChannelPinned(guild_ban_remove));
		EXPECT_FALSE(Payload::ChannelPinned(guild_emojis_update));
		EXPECT_FALSE(Payload::ChannelPinned(guild_integrations_update));
		EXPECT_FALSE(Payload::ChannelPinned(guild_member_add));
		EXPECT_FALSE(Payload::ChannelPinned(guild_member_remove));
		EXPECT_FALSE(Payload::ChannelPinned(guild_member_update));
		EXPECT_FALSE(Payload::ChannelPinned(guild_member_chunk));
		EXPECT_FALSE(Payload::ChannelPinned(role_create));
		EXPECT_FALSE(Payload::ChannelPinned(role_update));
		EXPECT_FALSE(Payload::ChannelPinned(role_delete));
		EXPECT_FALSE(Payload::ChannelPinned(message_create));
		EXPECT_FALSE(Payload::ChannelPinned(message_update));
		EXPECT_FALSE(Payload::ChannelPinned(message_delete));
		EXPECT_FALSE(Payload::ChannelPinned(message_delete_bulk));
		EXPECT_FALSE(Payload::ChannelPinned(message_reaction_add));
		EXPECT_FALSE(Payload::ChannelPinned(message_reaction_remove));
		EXPECT_FALSE(Payload::ChannelPinned(message_reaction_remove_all));
		EXPECT_FALSE(Payload::ChannelPinned(pressence_update));
		EXPECT_FALSE(Payload::ChannelPinned(typing_start));
		EXPECT_FALSE(Payload::ChannelPinned(user_update));
		EXPECT_FALSE(Payload::ChannelPinned(voice_state_update));
		EXPECT_FALSE(Payload::ChannelPinned(voice_server_update));
		EXPECT_FALSE(Payload::ChannelPinned(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildCreate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildCreate(ready));
		EXPECT_FALSE(Payload::GuildCreate(resumed));
		EXPECT_FALSE(Payload::GuildCreate(channel_create));
		EXPECT_FALSE(Payload::GuildCreate(channel_update));
		EXPECT_FALSE(Payload::GuildCreate(channel_delete));
		EXPECT_FALSE(Payload::GuildCreate(channel_pinned));
		EXPECT_TRUE(Payload::GuildCreate(guild_create));
		EXPECT_FALSE(Payload::GuildCreate(guild_update));
		EXPECT_FALSE(Payload::GuildCreate(guild_delete));
		EXPECT_FALSE(Payload::GuildCreate(guild_ban_add));
		EXPECT_FALSE(Payload::GuildCreate(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildCreate(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildCreate(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildCreate(guild_member_add));
		EXPECT_FALSE(Payload::GuildCreate(guild_member_remove));
		EXPECT_FALSE(Payload::GuildCreate(guild_member_update));
		EXPECT_FALSE(Payload::GuildCreate(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildCreate(role_create));
		EXPECT_FALSE(Payload::GuildCreate(role_update));
		EXPECT_FALSE(Payload::GuildCreate(role_delete));
		EXPECT_FALSE(Payload::GuildCreate(message_create));
		EXPECT_FALSE(Payload::GuildCreate(message_update));
		EXPECT_FALSE(Payload::GuildCreate(message_delete));
		EXPECT_FALSE(Payload::GuildCreate(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildCreate(message_reaction_add));
		EXPECT_FALSE(Payload::GuildCreate(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildCreate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildCreate(pressence_update));
		EXPECT_FALSE(Payload::GuildCreate(typing_start));
		EXPECT_FALSE(Payload::GuildCreate(user_update));
		EXPECT_FALSE(Payload::GuildCreate(voice_state_update));
		EXPECT_FALSE(Payload::GuildCreate(voice_server_update));
		EXPECT_FALSE(Payload::GuildCreate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildUpdate(ready));
		EXPECT_FALSE(Payload::GuildUpdate(resumed));
		EXPECT_FALSE(Payload::GuildUpdate(channel_create));
		EXPECT_FALSE(Payload::GuildUpdate(channel_update));
		EXPECT_FALSE(Payload::GuildUpdate(channel_delete));
		EXPECT_FALSE(Payload::GuildUpdate(channel_pinned));
		EXPECT_FALSE(Payload::GuildUpdate(guild_create));
		EXPECT_TRUE(Payload::GuildUpdate(guild_update));
		EXPECT_FALSE(Payload::GuildUpdate(guild_delete));
		EXPECT_FALSE(Payload::GuildUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::GuildUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildUpdate(guild_member_add));
		EXPECT_FALSE(Payload::GuildUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::GuildUpdate(guild_member_update));
		EXPECT_FALSE(Payload::GuildUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildUpdate(role_create));
		EXPECT_FALSE(Payload::GuildUpdate(role_update));
		EXPECT_FALSE(Payload::GuildUpdate(role_delete));
		EXPECT_FALSE(Payload::GuildUpdate(message_create));
		EXPECT_FALSE(Payload::GuildUpdate(message_update));
		EXPECT_FALSE(Payload::GuildUpdate(message_delete));
		EXPECT_FALSE(Payload::GuildUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::GuildUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildUpdate(pressence_update));
		EXPECT_FALSE(Payload::GuildUpdate(typing_start));
		EXPECT_FALSE(Payload::GuildUpdate(user_update));
		EXPECT_FALSE(Payload::GuildUpdate(voice_state_update));
		EXPECT_FALSE(Payload::GuildUpdate(voice_server_update));
		EXPECT_FALSE(Payload::GuildUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildDelete) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildDelete(ready));
		EXPECT_FALSE(Payload::GuildDelete(resumed));
		EXPECT_FALSE(Payload::GuildDelete(channel_create));
		EXPECT_FALSE(Payload::GuildDelete(channel_update));
		EXPECT_FALSE(Payload::GuildDelete(channel_delete));
		EXPECT_FALSE(Payload::GuildDelete(channel_pinned));
		EXPECT_FALSE(Payload::GuildDelete(guild_create));
		EXPECT_FALSE(Payload::GuildDelete(guild_update));
		EXPECT_TRUE(Payload::GuildDelete(guild_delete));
		EXPECT_FALSE(Payload::GuildDelete(guild_ban_add));
		EXPECT_FALSE(Payload::GuildDelete(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildDelete(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildDelete(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildDelete(guild_member_add));
		EXPECT_FALSE(Payload::GuildDelete(guild_member_remove));
		EXPECT_FALSE(Payload::GuildDelete(guild_member_update));
		EXPECT_FALSE(Payload::GuildDelete(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildDelete(role_create));
		EXPECT_FALSE(Payload::GuildDelete(role_update));
		EXPECT_FALSE(Payload::GuildDelete(role_delete));
		EXPECT_FALSE(Payload::GuildDelete(message_create));
		EXPECT_FALSE(Payload::GuildDelete(message_update));
		EXPECT_FALSE(Payload::GuildDelete(message_delete));
		EXPECT_FALSE(Payload::GuildDelete(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildDelete(message_reaction_add));
		EXPECT_FALSE(Payload::GuildDelete(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildDelete(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildDelete(pressence_update));
		EXPECT_FALSE(Payload::GuildDelete(typing_start));
		EXPECT_FALSE(Payload::GuildDelete(user_update));
		EXPECT_FALSE(Payload::GuildDelete(voice_state_update));
		EXPECT_FALSE(Payload::GuildDelete(voice_server_update));
		EXPECT_FALSE(Payload::GuildDelete(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildBanAdd) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildBanAdd(ready));
		EXPECT_FALSE(Payload::GuildBanAdd(resumed));
		EXPECT_FALSE(Payload::GuildBanAdd(channel_create));
		EXPECT_FALSE(Payload::GuildBanAdd(channel_update));
		EXPECT_FALSE(Payload::GuildBanAdd(channel_delete));
		EXPECT_FALSE(Payload::GuildBanAdd(channel_pinned));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_create));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_update));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_delete));
		EXPECT_TRUE(Payload::GuildBanAdd(guild_ban_add));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_member_add));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_member_remove));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_member_update));
		EXPECT_FALSE(Payload::GuildBanAdd(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildBanAdd(role_create));
		EXPECT_FALSE(Payload::GuildBanAdd(role_update));
		EXPECT_FALSE(Payload::GuildBanAdd(role_delete));
		EXPECT_FALSE(Payload::GuildBanAdd(message_create));
		EXPECT_FALSE(Payload::GuildBanAdd(message_update));
		EXPECT_FALSE(Payload::GuildBanAdd(message_delete));
		EXPECT_FALSE(Payload::GuildBanAdd(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildBanAdd(message_reaction_add));
		EXPECT_FALSE(Payload::GuildBanAdd(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildBanAdd(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildBanAdd(pressence_update));
		EXPECT_FALSE(Payload::GuildBanAdd(typing_start));
		EXPECT_FALSE(Payload::GuildBanAdd(user_update));
		EXPECT_FALSE(Payload::GuildBanAdd(voice_state_update));
		EXPECT_FALSE(Payload::GuildBanAdd(voice_server_update));
		EXPECT_FALSE(Payload::GuildBanAdd(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildBanRemove) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildBanRemove(ready));
		EXPECT_FALSE(Payload::GuildBanRemove(resumed));
		EXPECT_FALSE(Payload::GuildBanRemove(channel_create));
		EXPECT_FALSE(Payload::GuildBanRemove(channel_update));
		EXPECT_FALSE(Payload::GuildBanRemove(channel_delete));
		EXPECT_FALSE(Payload::GuildBanRemove(channel_pinned));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_create));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_update));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_delete));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_ban_add));
		EXPECT_TRUE(Payload::GuildBanRemove(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_member_add));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_member_remove));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_member_update));
		EXPECT_FALSE(Payload::GuildBanRemove(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildBanRemove(role_create));
		EXPECT_FALSE(Payload::GuildBanRemove(role_update));
		EXPECT_FALSE(Payload::GuildBanRemove(role_delete));
		EXPECT_FALSE(Payload::GuildBanRemove(message_create));
		EXPECT_FALSE(Payload::GuildBanRemove(message_update));
		EXPECT_FALSE(Payload::GuildBanRemove(message_delete));
		EXPECT_FALSE(Payload::GuildBanRemove(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildBanRemove(message_reaction_add));
		EXPECT_FALSE(Payload::GuildBanRemove(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildBanRemove(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildBanRemove(pressence_update));
		EXPECT_FALSE(Payload::GuildBanRemove(typing_start));
		EXPECT_FALSE(Payload::GuildBanRemove(user_update));
		EXPECT_FALSE(Payload::GuildBanRemove(voice_state_update));
		EXPECT_FALSE(Payload::GuildBanRemove(voice_server_update));
		EXPECT_FALSE(Payload::GuildBanRemove(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildEmojisUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildEmojisUpdate(ready));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(resumed));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(channel_create));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(channel_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(channel_delete));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(channel_pinned));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_create));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_delete));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_ban_remove));
		EXPECT_TRUE(Payload::GuildEmojisUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_member_add));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_member_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(role_create));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(role_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(role_delete));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(message_create));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(message_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(message_delete));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(pressence_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(typing_start));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(user_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(voice_state_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(voice_server_update));
		EXPECT_FALSE(Payload::GuildEmojisUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildIntegrationsUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(ready));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(resumed));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(channel_create));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(channel_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(channel_delete));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(channel_pinned));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_create));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_delete));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_emojis_update));
		EXPECT_TRUE(Payload::GuildIntegrationsUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_member_add));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_member_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(role_create));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(role_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(role_delete));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(message_create));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(message_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(message_delete));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(pressence_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(typing_start));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(user_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(voice_state_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(voice_server_update));
		EXPECT_FALSE(Payload::GuildIntegrationsUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildMemberAdd) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildMemberAdd(ready));
		EXPECT_FALSE(Payload::GuildMemberAdd(resumed));
		EXPECT_FALSE(Payload::GuildMemberAdd(channel_create));
		EXPECT_FALSE(Payload::GuildMemberAdd(channel_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(channel_delete));
		EXPECT_FALSE(Payload::GuildMemberAdd(channel_pinned));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_create));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_delete));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_ban_add));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_integrations_update));
		EXPECT_TRUE(Payload::GuildMemberAdd(guild_member_add));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_member_remove));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_member_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildMemberAdd(role_create));
		EXPECT_FALSE(Payload::GuildMemberAdd(role_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(role_delete));
		EXPECT_FALSE(Payload::GuildMemberAdd(message_create));
		EXPECT_FALSE(Payload::GuildMemberAdd(message_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(message_delete));
		EXPECT_FALSE(Payload::GuildMemberAdd(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildMemberAdd(message_reaction_add));
		EXPECT_FALSE(Payload::GuildMemberAdd(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildMemberAdd(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildMemberAdd(pressence_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(typing_start));
		EXPECT_FALSE(Payload::GuildMemberAdd(user_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(voice_state_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(voice_server_update));
		EXPECT_FALSE(Payload::GuildMemberAdd(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildMemberRemove) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildMemberRemove(ready));
		EXPECT_FALSE(Payload::GuildMemberRemove(resumed));
		EXPECT_FALSE(Payload::GuildMemberRemove(channel_create));
		EXPECT_FALSE(Payload::GuildMemberRemove(channel_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(channel_delete));
		EXPECT_FALSE(Payload::GuildMemberRemove(channel_pinned));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_create));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_delete));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_ban_add));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_member_add));
		EXPECT_TRUE(Payload::GuildMemberRemove(guild_member_remove));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_member_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildMemberRemove(role_create));
		EXPECT_FALSE(Payload::GuildMemberRemove(role_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(role_delete));
		EXPECT_FALSE(Payload::GuildMemberRemove(message_create));
		EXPECT_FALSE(Payload::GuildMemberRemove(message_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(message_delete));
		EXPECT_FALSE(Payload::GuildMemberRemove(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildMemberRemove(message_reaction_add));
		EXPECT_FALSE(Payload::GuildMemberRemove(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildMemberRemove(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildMemberRemove(pressence_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(typing_start));
		EXPECT_FALSE(Payload::GuildMemberRemove(user_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(voice_state_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(voice_server_update));
		EXPECT_FALSE(Payload::GuildMemberRemove(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildMemberUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildMemberUpdate(ready));
		EXPECT_FALSE(Payload::GuildMemberUpdate(resumed));
		EXPECT_FALSE(Payload::GuildMemberUpdate(channel_create));
		EXPECT_FALSE(Payload::GuildMemberUpdate(channel_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(channel_delete));
		EXPECT_FALSE(Payload::GuildMemberUpdate(channel_pinned));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_create));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_delete));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_member_add));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_member_remove));
		EXPECT_TRUE(Payload::GuildMemberUpdate(guild_member_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildMemberUpdate(role_create));
		EXPECT_FALSE(Payload::GuildMemberUpdate(role_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(role_delete));
		EXPECT_FALSE(Payload::GuildMemberUpdate(message_create));
		EXPECT_FALSE(Payload::GuildMemberUpdate(message_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(message_delete));
		EXPECT_FALSE(Payload::GuildMemberUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildMemberUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::GuildMemberUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildMemberUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildMemberUpdate(pressence_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(typing_start));
		EXPECT_FALSE(Payload::GuildMemberUpdate(user_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(voice_state_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(voice_server_update));
		EXPECT_FALSE(Payload::GuildMemberUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildMemberChunk) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildMemberChunk(ready));
		EXPECT_FALSE(Payload::GuildMemberChunk(resumed));
		EXPECT_FALSE(Payload::GuildMemberChunk(channel_create));
		EXPECT_FALSE(Payload::GuildMemberChunk(channel_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(channel_delete));
		EXPECT_FALSE(Payload::GuildMemberChunk(channel_pinned));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_create));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_delete));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_ban_add));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_member_add));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_member_remove));
		EXPECT_FALSE(Payload::GuildMemberChunk(guild_member_update));
		EXPECT_TRUE(Payload::GuildMemberChunk(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildMemberChunk(role_create));
		EXPECT_FALSE(Payload::GuildMemberChunk(role_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(role_delete));
		EXPECT_FALSE(Payload::GuildMemberChunk(message_create));
		EXPECT_FALSE(Payload::GuildMemberChunk(message_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(message_delete));
		EXPECT_FALSE(Payload::GuildMemberChunk(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildMemberChunk(message_reaction_add));
		EXPECT_FALSE(Payload::GuildMemberChunk(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildMemberChunk(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildMemberChunk(pressence_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(typing_start));
		EXPECT_FALSE(Payload::GuildMemberChunk(user_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(voice_state_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(voice_server_update));
		EXPECT_FALSE(Payload::GuildMemberChunk(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildRoleCreate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildRoleCreate(ready));
		EXPECT_FALSE(Payload::GuildRoleCreate(resumed));
		EXPECT_FALSE(Payload::GuildRoleCreate(channel_create));
		EXPECT_FALSE(Payload::GuildRoleCreate(channel_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(channel_delete));
		EXPECT_FALSE(Payload::GuildRoleCreate(channel_pinned));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_create));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_delete));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_ban_add));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_member_add));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_member_remove));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_member_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(guild_member_chunk));
		EXPECT_TRUE(Payload::GuildRoleCreate(role_create));
		EXPECT_FALSE(Payload::GuildRoleCreate(role_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(role_delete));
		EXPECT_FALSE(Payload::GuildRoleCreate(message_create));
		EXPECT_FALSE(Payload::GuildRoleCreate(message_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(message_delete));
		EXPECT_FALSE(Payload::GuildRoleCreate(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildRoleCreate(message_reaction_add));
		EXPECT_FALSE(Payload::GuildRoleCreate(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildRoleCreate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildRoleCreate(pressence_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(typing_start));
		EXPECT_FALSE(Payload::GuildRoleCreate(user_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(voice_state_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(voice_server_update));
		EXPECT_FALSE(Payload::GuildRoleCreate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildRoleUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildRoleUpdate(ready));
		EXPECT_FALSE(Payload::GuildRoleUpdate(resumed));
		EXPECT_FALSE(Payload::GuildRoleUpdate(channel_create));
		EXPECT_FALSE(Payload::GuildRoleUpdate(channel_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(channel_delete));
		EXPECT_FALSE(Payload::GuildRoleUpdate(channel_pinned));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_create));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_delete));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_member_add));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_member_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildRoleUpdate(role_create));
		EXPECT_TRUE(Payload::GuildRoleUpdate(role_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(role_delete));
		EXPECT_FALSE(Payload::GuildRoleUpdate(message_create));
		EXPECT_FALSE(Payload::GuildRoleUpdate(message_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(message_delete));
		EXPECT_FALSE(Payload::GuildRoleUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildRoleUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::GuildRoleUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildRoleUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildRoleUpdate(pressence_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(typing_start));
		EXPECT_FALSE(Payload::GuildRoleUpdate(user_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(voice_state_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(voice_server_update));
		EXPECT_FALSE(Payload::GuildRoleUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, GuildRoleDelete) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::GuildRoleDelete(ready));
		EXPECT_FALSE(Payload::GuildRoleDelete(resumed));
		EXPECT_FALSE(Payload::GuildRoleDelete(channel_create));
		EXPECT_FALSE(Payload::GuildRoleDelete(channel_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(channel_delete));
		EXPECT_FALSE(Payload::GuildRoleDelete(channel_pinned));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_create));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_delete));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_ban_add));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_ban_remove));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_emojis_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_integrations_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_member_add));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_member_remove));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_member_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(guild_member_chunk));
		EXPECT_FALSE(Payload::GuildRoleDelete(role_create));
		EXPECT_FALSE(Payload::GuildRoleDelete(role_update));
		EXPECT_TRUE(Payload::GuildRoleDelete(role_delete));
		EXPECT_FALSE(Payload::GuildRoleDelete(message_create));
		EXPECT_FALSE(Payload::GuildRoleDelete(message_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(message_delete));
		EXPECT_FALSE(Payload::GuildRoleDelete(message_delete_bulk));
		EXPECT_FALSE(Payload::GuildRoleDelete(message_reaction_add));
		EXPECT_FALSE(Payload::GuildRoleDelete(message_reaction_remove));
		EXPECT_FALSE(Payload::GuildRoleDelete(message_reaction_remove_all));
		EXPECT_FALSE(Payload::GuildRoleDelete(pressence_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(typing_start));
		EXPECT_FALSE(Payload::GuildRoleDelete(user_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(voice_state_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(voice_server_update));
		EXPECT_FALSE(Payload::GuildRoleDelete(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, MessageCreate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::MessageCreate(ready));
		EXPECT_FALSE(Payload::MessageCreate(resumed));
		EXPECT_FALSE(Payload::MessageCreate(channel_create));
		EXPECT_FALSE(Payload::MessageCreate(channel_update));
		EXPECT_FALSE(Payload::MessageCreate(channel_delete));
		EXPECT_FALSE(Payload::MessageCreate(channel_pinned));
		EXPECT_FALSE(Payload::MessageCreate(guild_create));
		EXPECT_FALSE(Payload::MessageCreate(guild_update));
		EXPECT_FALSE(Payload::MessageCreate(guild_delete));
		EXPECT_FALSE(Payload::MessageCreate(guild_ban_add));
		EXPECT_FALSE(Payload::MessageCreate(guild_ban_remove));
		EXPECT_FALSE(Payload::MessageCreate(guild_emojis_update));
		EXPECT_FALSE(Payload::MessageCreate(guild_integrations_update));
		EXPECT_FALSE(Payload::MessageCreate(guild_member_add));
		EXPECT_FALSE(Payload::MessageCreate(guild_member_remove));
		EXPECT_FALSE(Payload::MessageCreate(guild_member_update));
		EXPECT_FALSE(Payload::MessageCreate(guild_member_chunk));
		EXPECT_FALSE(Payload::MessageCreate(role_create));
		EXPECT_FALSE(Payload::MessageCreate(role_update));
		EXPECT_FALSE(Payload::MessageCreate(role_delete));
		EXPECT_TRUE(Payload::MessageCreate(message_create));
		EXPECT_FALSE(Payload::MessageCreate(message_update));
		EXPECT_FALSE(Payload::MessageCreate(message_delete));
		EXPECT_FALSE(Payload::MessageCreate(message_delete_bulk));
		EXPECT_FALSE(Payload::MessageCreate(message_reaction_add));
		EXPECT_FALSE(Payload::MessageCreate(message_reaction_remove));
		EXPECT_FALSE(Payload::MessageCreate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::MessageCreate(pressence_update));
		EXPECT_FALSE(Payload::MessageCreate(typing_start));
		EXPECT_FALSE(Payload::MessageCreate(user_update));
		EXPECT_FALSE(Payload::MessageCreate(voice_state_update));
		EXPECT_FALSE(Payload::MessageCreate(voice_server_update));
		EXPECT_FALSE(Payload::MessageCreate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, MessageUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::MessageUpdate(ready));
		EXPECT_FALSE(Payload::MessageUpdate(resumed));
		EXPECT_FALSE(Payload::MessageUpdate(channel_create));
		EXPECT_FALSE(Payload::MessageUpdate(channel_update));
		EXPECT_FALSE(Payload::MessageUpdate(channel_delete));
		EXPECT_FALSE(Payload::MessageUpdate(channel_pinned));
		EXPECT_FALSE(Payload::MessageUpdate(guild_create));
		EXPECT_FALSE(Payload::MessageUpdate(guild_update));
		EXPECT_FALSE(Payload::MessageUpdate(guild_delete));
		EXPECT_FALSE(Payload::MessageUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::MessageUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::MessageUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::MessageUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::MessageUpdate(guild_member_add));
		EXPECT_FALSE(Payload::MessageUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::MessageUpdate(guild_member_update));
		EXPECT_FALSE(Payload::MessageUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::MessageUpdate(role_create));
		EXPECT_FALSE(Payload::MessageUpdate(role_update));
		EXPECT_FALSE(Payload::MessageUpdate(role_delete));
		EXPECT_FALSE(Payload::MessageUpdate(message_create));
		EXPECT_TRUE(Payload::MessageUpdate(message_update));
		EXPECT_FALSE(Payload::MessageUpdate(message_delete));
		EXPECT_FALSE(Payload::MessageUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::MessageUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::MessageUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::MessageUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::MessageUpdate(pressence_update));
		EXPECT_FALSE(Payload::MessageUpdate(typing_start));
		EXPECT_FALSE(Payload::MessageUpdate(user_update));
		EXPECT_FALSE(Payload::MessageUpdate(voice_state_update));
		EXPECT_FALSE(Payload::MessageUpdate(voice_server_update));
		EXPECT_FALSE(Payload::MessageUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, MessageDelete) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::MessageDelete(ready));
		EXPECT_FALSE(Payload::MessageDelete(resumed));
		EXPECT_FALSE(Payload::MessageDelete(channel_create));
		EXPECT_FALSE(Payload::MessageDelete(channel_update));
		EXPECT_FALSE(Payload::MessageDelete(channel_delete));
		EXPECT_FALSE(Payload::MessageDelete(channel_pinned));
		EXPECT_FALSE(Payload::MessageDelete(guild_create));
		EXPECT_FALSE(Payload::MessageDelete(guild_update));
		EXPECT_FALSE(Payload::MessageDelete(guild_delete));
		EXPECT_FALSE(Payload::MessageDelete(guild_ban_add));
		EXPECT_FALSE(Payload::MessageDelete(guild_ban_remove));
		EXPECT_FALSE(Payload::MessageDelete(guild_emojis_update));
		EXPECT_FALSE(Payload::MessageDelete(guild_integrations_update));
		EXPECT_FALSE(Payload::MessageDelete(guild_member_add));
		EXPECT_FALSE(Payload::MessageDelete(guild_member_remove));
		EXPECT_FALSE(Payload::MessageDelete(guild_member_update));
		EXPECT_FALSE(Payload::MessageDelete(guild_member_chunk));
		EXPECT_FALSE(Payload::MessageDelete(role_create));
		EXPECT_FALSE(Payload::MessageDelete(role_update));
		EXPECT_FALSE(Payload::MessageDelete(role_delete));
		EXPECT_FALSE(Payload::MessageDelete(message_create));
		EXPECT_FALSE(Payload::MessageDelete(message_update));
		EXPECT_TRUE(Payload::MessageDelete(message_delete));
		EXPECT_FALSE(Payload::MessageDelete(message_delete_bulk));
		EXPECT_FALSE(Payload::MessageDelete(message_reaction_add));
		EXPECT_FALSE(Payload::MessageDelete(message_reaction_remove));
		EXPECT_FALSE(Payload::MessageDelete(message_reaction_remove_all));
		EXPECT_FALSE(Payload::MessageDelete(pressence_update));
		EXPECT_FALSE(Payload::MessageDelete(typing_start));
		EXPECT_FALSE(Payload::MessageDelete(user_update));
		EXPECT_FALSE(Payload::MessageDelete(voice_state_update));
		EXPECT_FALSE(Payload::MessageDelete(voice_server_update));
		EXPECT_FALSE(Payload::MessageDelete(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, MessageDeleteBulk) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::MessageDeleteBulk(ready));
		EXPECT_FALSE(Payload::MessageDeleteBulk(resumed));
		EXPECT_FALSE(Payload::MessageDeleteBulk(channel_create));
		EXPECT_FALSE(Payload::MessageDeleteBulk(channel_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(channel_delete));
		EXPECT_FALSE(Payload::MessageDeleteBulk(channel_pinned));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_create));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_delete));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_ban_add));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_ban_remove));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_emojis_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_integrations_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_member_add));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_member_remove));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_member_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(guild_member_chunk));
		EXPECT_FALSE(Payload::MessageDeleteBulk(role_create));
		EXPECT_FALSE(Payload::MessageDeleteBulk(role_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(role_delete));
		EXPECT_FALSE(Payload::MessageDeleteBulk(message_create));
		EXPECT_FALSE(Payload::MessageDeleteBulk(message_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(message_delete));
		EXPECT_TRUE(Payload::MessageDeleteBulk(message_delete_bulk));
		EXPECT_FALSE(Payload::MessageDeleteBulk(message_reaction_add));
		EXPECT_FALSE(Payload::MessageDeleteBulk(message_reaction_remove));
		EXPECT_FALSE(Payload::MessageDeleteBulk(message_reaction_remove_all));
		EXPECT_FALSE(Payload::MessageDeleteBulk(pressence_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(typing_start));
		EXPECT_FALSE(Payload::MessageDeleteBulk(user_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(voice_state_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(voice_server_update));
		EXPECT_FALSE(Payload::MessageDeleteBulk(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, MessageReactionAdd) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::MessageReactionAdd(ready));
		EXPECT_FALSE(Payload::MessageReactionAdd(resumed));
		EXPECT_FALSE(Payload::MessageReactionAdd(channel_create));
		EXPECT_FALSE(Payload::MessageReactionAdd(channel_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(channel_delete));
		EXPECT_FALSE(Payload::MessageReactionAdd(channel_pinned));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_create));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_delete));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_ban_add));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_ban_remove));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_emojis_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_integrations_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_member_add));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_member_remove));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_member_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(guild_member_chunk));
		EXPECT_FALSE(Payload::MessageReactionAdd(role_create));
		EXPECT_FALSE(Payload::MessageReactionAdd(role_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(role_delete));
		EXPECT_FALSE(Payload::MessageReactionAdd(message_create));
		EXPECT_FALSE(Payload::MessageReactionAdd(message_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(message_delete));
		EXPECT_FALSE(Payload::MessageReactionAdd(message_delete_bulk));
		EXPECT_TRUE(Payload::MessageReactionAdd(message_reaction_add));
		EXPECT_FALSE(Payload::MessageReactionAdd(message_reaction_remove));
		EXPECT_FALSE(Payload::MessageReactionAdd(message_reaction_remove_all));
		EXPECT_FALSE(Payload::MessageReactionAdd(pressence_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(typing_start));
		EXPECT_FALSE(Payload::MessageReactionAdd(user_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(voice_state_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(voice_server_update));
		EXPECT_FALSE(Payload::MessageReactionAdd(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, MessageReactionRemove) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::MessageReactionRemove(ready));
		EXPECT_FALSE(Payload::MessageReactionRemove(resumed));
		EXPECT_FALSE(Payload::MessageReactionRemove(channel_create));
		EXPECT_FALSE(Payload::MessageReactionRemove(channel_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(channel_delete));
		EXPECT_FALSE(Payload::MessageReactionRemove(channel_pinned));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_create));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_delete));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_ban_add));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_ban_remove));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_emojis_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_integrations_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_member_add));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_member_remove));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_member_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(guild_member_chunk));
		EXPECT_FALSE(Payload::MessageReactionRemove(role_create));
		EXPECT_FALSE(Payload::MessageReactionRemove(role_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(role_delete));
		EXPECT_FALSE(Payload::MessageReactionRemove(message_create));
		EXPECT_FALSE(Payload::MessageReactionRemove(message_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(message_delete));
		EXPECT_FALSE(Payload::MessageReactionRemove(message_delete_bulk));
		EXPECT_FALSE(Payload::MessageReactionRemove(message_reaction_add));
		EXPECT_TRUE(Payload::MessageReactionRemove(message_reaction_remove));
		EXPECT_FALSE(Payload::MessageReactionRemove(message_reaction_remove_all));
		EXPECT_FALSE(Payload::MessageReactionRemove(pressence_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(typing_start));
		EXPECT_FALSE(Payload::MessageReactionRemove(user_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(voice_state_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(voice_server_update));
		EXPECT_FALSE(Payload::MessageReactionRemove(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, MessageReactionRemoveAll) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(ready));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(resumed));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(channel_create));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(channel_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(channel_delete));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(channel_pinned));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_create));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_delete));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_ban_add));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_ban_remove));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_emojis_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_integrations_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_member_add));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_member_remove));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_member_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(guild_member_chunk));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(role_create));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(role_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(role_delete));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(message_create));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(message_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(message_delete));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(message_delete_bulk));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(message_reaction_add));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(message_reaction_remove));
		EXPECT_TRUE(Payload::MessageReactionRemoveAll(message_reaction_remove_all));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(pressence_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(typing_start));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(user_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(voice_state_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(voice_server_update));
		EXPECT_FALSE(Payload::MessageReactionRemoveAll(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, PressenceUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::PressenceUpdate(ready));
		EXPECT_FALSE(Payload::PressenceUpdate(resumed));
		EXPECT_FALSE(Payload::PressenceUpdate(channel_create));
		EXPECT_FALSE(Payload::PressenceUpdate(channel_update));
		EXPECT_FALSE(Payload::PressenceUpdate(channel_delete));
		EXPECT_FALSE(Payload::PressenceUpdate(channel_pinned));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_create));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_update));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_delete));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_member_add));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_member_update));
		EXPECT_FALSE(Payload::PressenceUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::PressenceUpdate(role_create));
		EXPECT_FALSE(Payload::PressenceUpdate(role_update));
		EXPECT_FALSE(Payload::PressenceUpdate(role_delete));
		EXPECT_FALSE(Payload::PressenceUpdate(message_create));
		EXPECT_FALSE(Payload::PressenceUpdate(message_update));
		EXPECT_FALSE(Payload::PressenceUpdate(message_delete));
		EXPECT_FALSE(Payload::PressenceUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::PressenceUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::PressenceUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::PressenceUpdate(message_reaction_remove_all));
		EXPECT_TRUE(Payload::PressenceUpdate(pressence_update));
		EXPECT_FALSE(Payload::PressenceUpdate(typing_start));
		EXPECT_FALSE(Payload::PressenceUpdate(user_update));
		EXPECT_FALSE(Payload::PressenceUpdate(voice_state_update));
		EXPECT_FALSE(Payload::PressenceUpdate(voice_server_update));
		EXPECT_FALSE(Payload::PressenceUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, TypingStart) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::TypingStart(ready));
		EXPECT_FALSE(Payload::TypingStart(resumed));
		EXPECT_FALSE(Payload::TypingStart(channel_create));
		EXPECT_FALSE(Payload::TypingStart(channel_update));
		EXPECT_FALSE(Payload::TypingStart(channel_delete));
		EXPECT_FALSE(Payload::TypingStart(channel_pinned));
		EXPECT_FALSE(Payload::TypingStart(guild_create));
		EXPECT_FALSE(Payload::TypingStart(guild_update));
		EXPECT_FALSE(Payload::TypingStart(guild_delete));
		EXPECT_FALSE(Payload::TypingStart(guild_ban_add));
		EXPECT_FALSE(Payload::TypingStart(guild_ban_remove));
		EXPECT_FALSE(Payload::TypingStart(guild_emojis_update));
		EXPECT_FALSE(Payload::TypingStart(guild_integrations_update));
		EXPECT_FALSE(Payload::TypingStart(guild_member_add));
		EXPECT_FALSE(Payload::TypingStart(guild_member_remove));
		EXPECT_FALSE(Payload::TypingStart(guild_member_update));
		EXPECT_FALSE(Payload::TypingStart(guild_member_chunk));
		EXPECT_FALSE(Payload::TypingStart(role_create));
		EXPECT_FALSE(Payload::TypingStart(role_update));
		EXPECT_FALSE(Payload::TypingStart(role_delete));
		EXPECT_FALSE(Payload::TypingStart(message_create));
		EXPECT_FALSE(Payload::TypingStart(message_update));
		EXPECT_FALSE(Payload::TypingStart(message_delete));
		EXPECT_FALSE(Payload::TypingStart(message_delete_bulk));
		EXPECT_FALSE(Payload::TypingStart(message_reaction_add));
		EXPECT_FALSE(Payload::TypingStart(message_reaction_remove));
		EXPECT_FALSE(Payload::TypingStart(message_reaction_remove_all));
		EXPECT_FALSE(Payload::TypingStart(pressence_update));
		EXPECT_TRUE(Payload::TypingStart(typing_start));
		EXPECT_FALSE(Payload::TypingStart(user_update));
		EXPECT_FALSE(Payload::TypingStart(voice_state_update));
		EXPECT_FALSE(Payload::TypingStart(voice_server_update));
		EXPECT_FALSE(Payload::TypingStart(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, UserUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::UserUpdate(ready));
		EXPECT_FALSE(Payload::UserUpdate(resumed));
		EXPECT_FALSE(Payload::UserUpdate(channel_create));
		EXPECT_FALSE(Payload::UserUpdate(channel_update));
		EXPECT_FALSE(Payload::UserUpdate(channel_delete));
		EXPECT_FALSE(Payload::UserUpdate(channel_pinned));
		EXPECT_FALSE(Payload::UserUpdate(guild_create));
		EXPECT_FALSE(Payload::UserUpdate(guild_update));
		EXPECT_FALSE(Payload::UserUpdate(guild_delete));
		EXPECT_FALSE(Payload::UserUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::UserUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::UserUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::UserUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::UserUpdate(guild_member_add));
		EXPECT_FALSE(Payload::UserUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::UserUpdate(guild_member_update));
		EXPECT_FALSE(Payload::UserUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::UserUpdate(role_create));
		EXPECT_FALSE(Payload::UserUpdate(role_update));
		EXPECT_FALSE(Payload::UserUpdate(role_delete));
		EXPECT_FALSE(Payload::UserUpdate(message_create));
		EXPECT_FALSE(Payload::UserUpdate(message_update));
		EXPECT_FALSE(Payload::UserUpdate(message_delete));
		EXPECT_FALSE(Payload::UserUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::UserUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::UserUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::UserUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::UserUpdate(pressence_update));
		EXPECT_FALSE(Payload::UserUpdate(typing_start));
		EXPECT_TRUE(Payload::UserUpdate(user_update));
		EXPECT_FALSE(Payload::UserUpdate(voice_state_update));
		EXPECT_FALSE(Payload::UserUpdate(voice_server_update));
		EXPECT_FALSE(Payload::UserUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, VoiceStateUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::VoiceStateUpdate(ready));
		EXPECT_FALSE(Payload::VoiceStateUpdate(resumed));
		EXPECT_FALSE(Payload::VoiceStateUpdate(channel_create));
		EXPECT_FALSE(Payload::VoiceStateUpdate(channel_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(channel_delete));
		EXPECT_FALSE(Payload::VoiceStateUpdate(channel_pinned));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_create));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_delete));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_member_add));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_member_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::VoiceStateUpdate(role_create));
		EXPECT_FALSE(Payload::VoiceStateUpdate(role_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(role_delete));
		EXPECT_FALSE(Payload::VoiceStateUpdate(message_create));
		EXPECT_FALSE(Payload::VoiceStateUpdate(message_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(message_delete));
		EXPECT_FALSE(Payload::VoiceStateUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::VoiceStateUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::VoiceStateUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::VoiceStateUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::VoiceStateUpdate(pressence_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(typing_start));
		EXPECT_FALSE(Payload::VoiceStateUpdate(user_update));
		EXPECT_TRUE(Payload::VoiceStateUpdate(voice_state_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(voice_server_update));
		EXPECT_FALSE(Payload::VoiceStateUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, VoiceServerUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::VoiceServerUpdate(ready));
		EXPECT_FALSE(Payload::VoiceServerUpdate(resumed));
		EXPECT_FALSE(Payload::VoiceServerUpdate(channel_create));
		EXPECT_FALSE(Payload::VoiceServerUpdate(channel_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(channel_delete));
		EXPECT_FALSE(Payload::VoiceServerUpdate(channel_pinned));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_create));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_delete));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_member_add));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_member_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::VoiceServerUpdate(role_create));
		EXPECT_FALSE(Payload::VoiceServerUpdate(role_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(role_delete));
		EXPECT_FALSE(Payload::VoiceServerUpdate(message_create));
		EXPECT_FALSE(Payload::VoiceServerUpdate(message_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(message_delete));
		EXPECT_FALSE(Payload::VoiceServerUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::VoiceServerUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::VoiceServerUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::VoiceServerUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::VoiceServerUpdate(pressence_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(typing_start));
		EXPECT_FALSE(Payload::VoiceServerUpdate(user_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(voice_state_update));
		EXPECT_TRUE(Payload::VoiceServerUpdate(voice_server_update));
		EXPECT_FALSE(Payload::VoiceServerUpdate(webhooks_update));
		TEST_END
	}

	TEST_F(EventPredicateTests, WebhooksUpdate) {
		TEST_BEGIN
		EXPECT_FALSE(Payload::WebhooksUpdate(ready));
		EXPECT_FALSE(Payload::WebhooksUpdate(resumed));
		EXPECT_FALSE(Payload::WebhooksUpdate(channel_create));
		EXPECT_FALSE(Payload::WebhooksUpdate(channel_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(channel_delete));
		EXPECT_FALSE(Payload::WebhooksUpdate(channel_pinned));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_create));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_delete));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_ban_add));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_ban_remove));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_emojis_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_integrations_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_member_add));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_member_remove));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_member_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(guild_member_chunk));
		EXPECT_FALSE(Payload::WebhooksUpdate(role_create));
		EXPECT_FALSE(Payload::WebhooksUpdate(role_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(role_delete));
		EXPECT_FALSE(Payload::WebhooksUpdate(message_create));
		EXPECT_FALSE(Payload::WebhooksUpdate(message_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(message_delete));
		EXPECT_FALSE(Payload::WebhooksUpdate(message_delete_bulk));
		EXPECT_FALSE(Payload::WebhooksUpdate(message_reaction_add));
		EXPECT_FALSE(Payload::WebhooksUpdate(message_reaction_remove));
		EXPECT_FALSE(Payload::WebhooksUpdate(message_reaction_remove_all));
		EXPECT_FALSE(Payload::WebhooksUpdate(pressence_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(typing_start));
		EXPECT_FALSE(Payload::WebhooksUpdate(user_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(voice_state_update));
		EXPECT_FALSE(Payload::WebhooksUpdate(voice_server_update));
		EXPECT_TRUE(Payload::WebhooksUpdate(webhooks_update));
		TEST_END
	}
}
