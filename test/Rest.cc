/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Rest.hh"

using namespace testing;
using namespace std;
using namespace RpBot;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	// 2019-11-23 AMR TODO: how do we get a valid token for testing?
	// 2019-11-25 AMR TODO: mock at least a get
	TEST(RestTests, Create) {
		TEST_BEGIN
		Context ctx(
			"An invalid token",
			"localhost",
			"8080"
		);
		ctx.ssl().load_verify_file("ca.crt");
		Rest r(ctx);

		r.createMessage("message", "channel");
		r.createMessage("message", "channel");

		ctx.io().run();
		TEST_END
	}
}
