/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../modules/Base.cc"

using namespace testing;

namespace {
	class StoreMock :
		public Module
	{
		public:
			StoreMock() :
				Module(NAME, {})
			{
				setCommand("help"s, bind(&StoreMock::help, this, _1, _2));
				setCommand("get"s, bind(&StoreMock::get, this, _1, _2));
				setCommand("set"s, bind(&StoreMock::set, this, _1, _2));
				setCommand("save"s, bind(&StoreMock::save, this, _1, _2));
			}
			MOCK_METHOD(string, help, (const string &, const Payload &));
			MOCK_METHOD(string, get, (const string &, const Payload &));
			MOCK_METHOD(string, set, (const string &, const Payload &));
			MOCK_METHOD(string, save, (const string &, const Payload &));
		private:
			static constexpr const char *NAME = "Store";
	};

	class AppMock :
		public Application
	{
		public:
			AppMock(const int c, const char *v[])  :
				Application(c, v)
			{}
			MOCK_METHOD(bool, load, (const string &), (override));
			MOCK_METHOD(bool, unload, (const string &), (override));
			MOCK_METHOD(void, modules, (const function<void(Coeus::Import<ModuleBase> &)> &), (override));
			MOCK_METHOD(void, setPrefix, (const string &), (override));
			MOCK_METHOD(void, addAlias, (const string &, const string &, const string &, const string &), (override));
			MOCK_METHOD(void, removeAlias, (const string &), (override));
	};

	class BaseTests :
		public Test
	{
		public:
			void SetUp() override;
		protected:
			shared_ptr<StrictMock<StoreMock>> store;
			shared_ptr<StrictMock<AppMock>> app;
			shared_ptr<Base> base;
		private:
			Coeus::Import<ModuleBase> _store;
	};

	TEST_F(BaseTests, help) {
		EXPECT_CALL(*app, modules(_))
			.Times(1);

		EXPECT_CALL(*store, help(_, _))
			.Times(1)
			.WillOnce(Return("Some help string"s));

		auto res = base->command("help"s, ""s, {});

		EXPECT_NE(0u, res.size());
	}

	TEST_F(BaseTests, Load) {
		auto what = "foo"s;

		EXPECT_CALL(*app, load(what))
			.Times(1)
			.WillOnce(Return(true));

		EXPECT_EQ(what + " loaded"s, base->command("load", what, {}));
	}

	TEST_F(BaseTests, LoadFail) {
		auto what = "foo"s;

		EXPECT_CALL(*app, load(what))
			.Times(1)
			.WillOnce(Return(false));

		EXPECT_NE(what + " loaded"s, base->command("load", what, {}));
	}

	TEST_F(BaseTests, Unload) {
		auto what = "foo"s;

		EXPECT_CALL(*app, unload(what))
			.Times(1)
			.WillOnce(Return(true));

		EXPECT_EQ(what + " unloaded"s, base->command("unload", what, {}));
	}

	TEST_F(BaseTests, UnloadSelf) {
		auto what = "Base"s;

		EXPECT_NE(what + " loaded"s, base->command("unload", what, {}));
	}

	TEST_F(BaseTests, UnloadFail) {
		auto what = "foo"s;

		EXPECT_CALL(*app, unload(what))
			.Times(1)
			.WillOnce(Return(false));

		EXPECT_NE(what + " unloaded"s, base->command("unload", what, {}));
	}

	TEST_F(BaseTests, onLoadedCallsGet) {
		EXPECT_CALL(*store, get("prefix"s, _))
			.Times(1)
			.WillOnce(Return(""));

		base->onLoaded();
	}

	TEST_F(BaseTests, onLoaded) {
		auto pref = "asdf"s;

		EXPECT_CALL(*store, get("prefix"s, _))
			.Times(1)
			.WillOnce(Return(pref));

		EXPECT_CALL(*app, setPrefix(pref))
			.Times(1);

		base->onLoaded();
	}

	TEST_F(BaseTests, ConfigGet) {
		auto what = "foo"s;
		auto val = "bar"s;

		EXPECT_CALL(*store, get(what, _))
			.Times(1)
			.WillOnce(Return(val));

		EXPECT_EQ(val, base->command("config", what, {}));
	}

	TEST_F(BaseTests, ConfigSetNoPrefix) {
		auto what = "foo"s;
		auto val = "bar"s;
		auto args = what + " "s + val;

		EXPECT_CALL(*store, set(args, _))
			.Times(1)
			.WillOnce(Return(val));
		EXPECT_CALL(*store, get("prefix", _))
			.Times(1)
			.WillOnce(Return(""));

		EXPECT_EQ(val, base->command("config", args, {}));
	}

	TEST_F(BaseTests, ConfigSetPrefix) {
		auto what = "prefix"s;
		auto val = "bar"s;
		auto args = what + " "s + val;

		EXPECT_CALL(*store, set(args, _))
			.Times(1)
			.WillOnce(Return(val));
		EXPECT_CALL(*store, get("prefix", _))
			.Times(1)
			.WillOnce(Return(val));
		EXPECT_CALL(*app, setPrefix(val))
			.Times(1);

		EXPECT_EQ(val, base->command("config", args, {}));
	}

	TEST_F(BaseTests, Alias) {
		auto alias = "!thingy"s;
		auto command = "!mod name arg"s;

		EXPECT_CALL(*app, addAlias("thingy"s, "mod"s, "name"s, "arg"s))
			.Times(1);

		EXPECT_EQ("added alias thingy"s, base->command("alias"s, alias + " "s + command, {}));
	}

	TEST_F(BaseTests, AliasNoArgs) {
		auto alias = "thingy"s;
		auto command = "!mod name"s;

		EXPECT_CALL(*app, addAlias(alias, "mod"s, "name"s, ""s))
			.Times(1);

		EXPECT_EQ("added alias " + alias, base->command("alias"s, "!"s + alias + " "s + command, {}));
	}

	TEST_F(BaseTests, Unalias) {
		auto alias = "!thingy"s;

		EXPECT_CALL(*app, removeAlias("thingy"s))
			.Times(1);

		EXPECT_EQ("removed alias thingy"s, base->command("unalias"s, alias, {}));
	}

	const char *Args[] = {
		"PROGRAM NAME",
	};

	void BaseTests::SetUp() {
		// 2020-01-20 AMR TODO: there hase to be a better way
		store = make_shared<remove_reference<decltype(*store)>::type>();
		app = make_shared<remove_reference<decltype(*app)>::type>(1, Args);
		base = make_shared<remove_reference<decltype(*base)>::type>();
		_store = store;

		ON_CALL(*app, modules(_))
			.WillByDefault(InvokeArgument<0>(_store));

		EXPECT_CALL(*app, load(Eq("Store")))
			.Times(1)
			.WillOnce(Return(true));

		EXPECT_CALL(*app, modules(_))
			.Times(1);

		base->setApplication(app.get());
	}
}
