/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../modules/Campaign.cc"

using namespace testing;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class StoreMock :
		public Module
	{
		public:
			StoreMock() :
				Module(NAME, {})
			{
				setCommand("get"s, bind(&StoreMock::get, this, _1, _2));
				setCommand("set"s, bind(&StoreMock::set, this, _1, _2));
				setCommand("erase"s, bind(&StoreMock::erase, this, _1, _2));
			}
			MOCK_METHOD(string, get, (const string &, const Payload &));
			MOCK_METHOD(string, set, (const string &, const Payload &));
			MOCK_METHOD(string, erase, (const string &, const Payload &));
		private:
			static constexpr const char *NAME = "Store";
	};

	class AppMock :
		public Application
	{
		public:
			AppMock(const int c, const char *v[]) :
				Application(c, v)
			{}
			MOCK_METHOD(bool, load, (const string &), (override));
			MOCK_METHOD(void, modules, (const function<void(Coeus::Import<ModuleBase> &)> &), (override));
	};

	class CampaignTests :
		public Test
	{
		public:
			void SetUp() override;
		protected:
			shared_ptr<StrictMock<StoreMock>> store;
			shared_ptr<Campaign> campaign;
		private:
			shared_ptr<StrictMock<AppMock>> app;
			Coeus::Import<ModuleBase> _store;
	};

	auto const Name = "Campaign"s;

	TEST_F(CampaignTests, CreateCampaign) {
		TEST_BEGIN
		auto name = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		EXPECT_CALL(*store, set(Name + ".names asdf;qwer;test-campaign"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("create"s, name, {});

		EXPECT_EQ(name + " created"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, CreateCampaignFound) {
		TEST_BEGIN
		auto name = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		auto res = campaign->command("create"s, name, {});

		EXPECT_EQ(name + " already exists"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, CreateCampaignEmptyString) {
		TEST_BEGIN
		auto name = ""s;

		auto res = campaign->command("create"s, name, {});

		EXPECT_EQ("disallowed name"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, GetCampaign) {
		TEST_BEGIN
		auto name = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("player1;player2"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("channel1;channel2"s));

		auto res = campaign->command("get"s, name, {});

		EXPECT_EQ(string::npos, res.find("not found"s));
		TEST_END
	}

	TEST_F(CampaignTests, GetCampaignNotFound) {
		TEST_BEGIN
		auto name = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));
		
		auto res = campaign->command("get"s, name, {});

		EXPECT_EQ(name + " not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, GetCampaignFromChannel) {
		TEST_BEGIN
		auto name = "#12341234"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return(name));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		auto res = campaign->command("get"s, "<"s + name + ">"s, {});

		EXPECT_EQ(string::npos, res.find("not found"s));
		TEST_END
	}

	TEST_F(CampaignTests, GetCampaignFromMessageLocation) {
		TEST_BEGIN
		auto msg = Payload::Parse(R"({"op":0,"d":{"channel_id":"12341234"},"t":"MESSAGE_CREATE","s":null})"s);

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("#12341234"s));

		auto res = campaign->command("get"s, ""s, *msg);

		EXPECT_EQ(string::npos, res.find("not found"s));
		TEST_END
	}

	TEST_F(CampaignTests, DeleteCampaign) {
		TEST_BEGIN
		auto name = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;test-campaign"s));

		EXPECT_CALL(*store, set(Name + ".names asdf"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, erase(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, erase(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("delete"s, name, {});

		EXPECT_EQ(name + " deleted"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, DeleteCampaignNotFound) {
		TEST_BEGIN
		auto name = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf"s));

		auto res = campaign->command("delete"s, name, {});

		EXPECT_EQ(name + " not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddPlayer) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf:qwer"s));

		EXPECT_CALL(*store, set(Name + ".test-campaign.players asdf:qwer;"s + character + ":"s + player, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("add"s, "<"s + player + "> "s + character + " "s + which, {});

		EXPECT_EQ(player + " added to "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddPlayerRoom) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto msg = Payload::Parse(R"({"op":0,"t":"MESSAGE_CREATE","d":{"channel_id":"12341234"}})"s);

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".asdf.channels"s, _))
			.Times(1)
			.WillOnce(Return("#7890"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("#12341234"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf:1234"s));

		EXPECT_CALL(*store, set(Name + ".test-campaign.players asdf:1234;"s + character + ":"s + player, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("add"s, "<"s + player + "> "s + character, *msg);

		EXPECT_EQ(player + " added to test-campaign"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddPlayerExisting) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf:qwer;"s + character + ":"s + player));

		auto res = campaign->command("add"s, "<"s + player + "> "s + character + " "s + which, {});

		EXPECT_EQ(player + " is already a player of "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddPlayerNoCampaign) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		auto res = campaign->command("add"s, "<"s + player + "> "s + character + " "s + which, {});

		EXPECT_EQ(which + " not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddPlayerNoCampaignRoom) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto msg = Payload::Parse(R"({"op":0,"t":"MESSAGE_CREATE","d":{"channel_id":"12341234"}})"s);

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		EXPECT_CALL(*store, get(Name + ".asdf.channels"s, _))
			.Times(1)
			.WillOnce(Return("vbnm"s));

		EXPECT_CALL(*store, get(Name + ".qwer.channels"s, _))
			.Times(1)
			.WillOnce(Return("zxcv"s));

		auto res = campaign->command("add"s, "<"s + player + "> "s + character, *msg);

		EXPECT_EQ(" not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddChannel) {
		TEST_BEGIN
		auto channel = "#12341234"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("asdf"s));

		EXPECT_CALL(*store, set(Name + ".test-campaign.channels asdf;"s + channel, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("add"s, "<"s + channel + "> "s + which, {});

		EXPECT_EQ(channel + " added to "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddChannelRoom) {
		TEST_BEGIN
		auto channel = "#12347890"s;
		auto msg = Payload::Parse(R"({"op":0,"t":"MESSAGE_CREATE","d":{"channel_id":"12341234"}})"s);

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".asdf.channels"s, _))
			.Times(1)
			.WillOnce(Return("1234"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("#12341234"s));

		EXPECT_CALL(*store, set(Name + ".test-campaign.channels #12341234;"s + channel, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("add"s, "<"s + channel + ">"s, *msg);

		EXPECT_EQ(channel + " added to test-campaign"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddChannelExisting) {
		TEST_BEGIN
		auto channel = "#12341234"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("asdf;"s + channel));

		auto res = campaign->command("add"s, "<"s + channel + "> "s + which, {});

		EXPECT_EQ(channel + " is already a channel of "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddChannelNoCampaign) {
		TEST_BEGIN
		auto channel = "#12341234"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		auto res = campaign->command("add"s, "<"s + channel + "> "s + which, {});

		EXPECT_EQ(which + " not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddChannelNoCampaignRoom) {
		TEST_BEGIN
		auto channel = "#12347890"s;
		auto msg = Payload::Parse(R"({"op":0,"t":"MESSAGE_CREATE","d":{"channel_id":"12341234"}})"s);

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		EXPECT_CALL(*store, get(Name + ".asdf.channels", _))
			.Times(1)
			.WillOnce(Return("#1234"s));

		EXPECT_CALL(*store, get(Name + ".qwer.channels"s, _))
			.Times(1)
			.WillOnce(Return("#7890"s));

		auto res = campaign->command("add"s, "<"s + channel + ">"s, *msg);

		EXPECT_EQ(" not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, AddJunk) {
		TEST_BEGIN
		auto junk = "asdf"s;
		auto which = "test campaign"s;

		auto res = campaign->command("add"s, junk + " "s + which, {});

		EXPECT_NE(string::npos, res.find("tag"s));
		TEST_END
	}

	TEST_F(CampaignTests, RemoveCharacter) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf:1234;"s + character + ":"s + player));

		EXPECT_CALL(*store, set(Name + ".test-campaign.players asdf:1234"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("remove"s, character + " "s + which, {});

		EXPECT_EQ(character + " removed from "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemoveCharacterRoom) {
		TEST_BEGIN
		auto character = "asdf"s;
		auto msg = Payload::Parse(R"({"op":0,"t":"MESSAGE_CREATE","d":{"channel_id":"12341234"}})"s);

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("#12341234"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("qwer:1234;asdf:@!1234"s));

		EXPECT_CALL(*store, set(Name + ".test-campaign.players qwer:1234"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("remove"s, character, *msg);

		EXPECT_EQ(character + " removed from test-campaign"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemoveCharacterNotFound) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf:1234;qwer:7890"s));

		auto res = campaign->command("remove"s, character + " "s + which, {});

		EXPECT_EQ(character + " not found in "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemovePlayer) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf:1234;"s + character + ":"s + player));

		EXPECT_CALL(*store, set(Name + ".test-campaign.players asdf:1234"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("remove"s, "<"s + player + "> "s + which, {});

		EXPECT_EQ(player + " removed from "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemovePlayerRoom) {
		TEST_BEGIN
		auto player = "@!1234"s;
		auto msg = Payload::Parse(R"({"op":0,"t":"MESSAGE_CREATE","d":{"channel_id":"12341234"}})"s);

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("#12341234"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("qwer:1234;asdf:@!1234"s));

		EXPECT_CALL(*store, set(Name + ".test-campaign.players qwer:1234"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("remove"s, "<"s + player + ">"s, *msg);

		EXPECT_EQ(player + " removed from test-campaign"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemovePlayerNotFound) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto character = "foo"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("asdf:1234;qwer:7890"s));

		auto res = campaign->command("remove"s, "<"s + player + "> "s + which, {});

		EXPECT_EQ(player + " not found in "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemovePlayerNoCampaign) {
		TEST_BEGIN
		auto player = "@!12341234"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		auto res = campaign->command("remove"s, "<" + player + "> "s + which, {});

		EXPECT_EQ(which + " not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemovePlayerShortParams) {
		TEST_BEGIN
		auto player = "@!12341234"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		EXPECT_CALL(*store, get(Name + ".asdf.channels"s, _))
			.Times(1)
			.WillOnce(Return("#1234"s));

		EXPECT_CALL(*store, get(Name + ".qwer.channels"s, _))
			.Times(1)
			.WillOnce(Return("#7890"s));

		auto res = campaign->command("remove"s, "<" + player + ">"s, {});

		EXPECT_EQ(" not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemoveChannel) {
		TEST_BEGIN
		auto channel = "#12341234"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("asdf;"s + channel));

		EXPECT_CALL(*store, set(Name + ".test-campaign.channels asdf"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = campaign->command("remove"s, "<"s + channel + "> "s + which, {});

		EXPECT_EQ(channel + " removed from "s + which, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemoveChannelNotFound) {
		TEST_BEGIN
		auto channel = "#12341234"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.channels"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		auto res = campaign->command("remove"s, "<"s + channel + "> "s + which, {});

		EXPECT_EQ(channel + " not found in "s + which, res);
		TEST_END
	}
	
	TEST_F(CampaignTests, RemoveChannelNoCampaign) {
		TEST_BEGIN
		auto channel = "#12341234"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("asdf;qwer"s));

		auto res = campaign->command("remove"s, "<"s + channel + "> "s + which, {});

		EXPECT_EQ(which + " not found"s, res);
		TEST_END
	}

	TEST_F(CampaignTests, RemoveJunk) {
		auto junk = "asdf"s;
		auto which = "test campaign"s;

		EXPECT_CALL(*store, get(Name + ".names"s, _))
			.Times(1)
			.WillOnce(Return("test-campaign"s));

		EXPECT_CALL(*store, get(Name + ".test-campaign.players"s, _))
			.Times(1)
			.WillOnce(Return("qwer:1234;zxcv:7890"s));

		auto res = campaign->command("remove"s, junk + " "s + which, {});

		EXPECT_EQ(junk + " not found in "s + which, res);
	}

	const char *Args[] = {
		"PROGRAM NAME",
	};

	void CampaignTests::SetUp() {
		store = make_shared<remove_reference<decltype(*store)>::type>();
		app = make_shared<remove_reference<decltype(*app)>::type>(1, Args);
		campaign  = make_shared<remove_reference<decltype(*campaign)>::type>();
		_store = store;

		ON_CALL(*app, modules(_))
			.WillByDefault(InvokeArgument<0>(_store));

		EXPECT_CALL(*app, load(Eq("Store")))
			.Times(1)
			.WillOnce(Return(true));

		EXPECT_CALL(*app, modules(_))
			.Times(1);

		campaign->setApplication(app.get());
	}
}
