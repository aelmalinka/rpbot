/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../../modules/Store.cc"

using namespace testing;

namespace {
	class StoreTests :
		public Test
	{
		public:
			void SetUp() override;
			void TearDown() override;
		protected:
			shared_ptr<Store> store;
	};

	TEST_F(StoreTests, Help) {
		EXPECT_NE(0u, store->command("help"s, ""s, {}).size());
	}

	TEST_F(StoreTests, Set) {
		auto what = "foo"s;
		auto val = "bar"s;
		auto args = what + " "s + val;
		auto res = what + " = "s + val;

		EXPECT_EQ(res, store->command("set", args, {}));
	}

	TEST_F(StoreTests, Get) {
		auto what = "foo"s;
		auto val = "bar"s;
		auto args = what + " "s + val;

		store->command("set", args, {});

		EXPECT_EQ(val, store->command("get", what, {}));
	}

	TEST_F(StoreTests, Erase) {
		auto what = "foo"s;
		auto val = "bar"s;
		auto args = what + " "s + val;

		store->command("set"s, args, {});

		EXPECT_EQ(what + " erased"s, store->command("erase"s, what, {}));
		EXPECT_EQ(""s, store->command("get"s, what, {}));
	}

	TEST_F(StoreTests, Save) {
		EXPECT_EQ("values saved"s, store->command("save"s, ""s, {}));
	}

	void StoreTests::SetUp() {
		store = make_shared<Store>();
	}

	void StoreTests::TearDown() {
		store.reset();

		remove(current_path() / "values");
	}
}
