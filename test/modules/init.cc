/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../modules/Initiative.cc"

using namespace testing;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class CampaignMock :
		public Module
	{
		public:
			CampaignMock() :
				Module(NAME, {})
			{
				setCommand("get"s, bind(&CampaignMock::get, this, _1, _2));
			}
			MOCK_METHOD(string, get, (const string &, const Payload &));
			static constexpr const char *NAME = "Campaign";
	};

	class DiceMock :
		public Module
	{
		public:
			DiceMock() :
				Module(NAME, {})
			{
				setCommand("roll"s, bind(&DiceMock::roll, this, _1, _2));
			}
			MOCK_METHOD(string, roll, (const string &, const Payload &));
			static constexpr const char *NAME = "Dice";
	};

	class StoreMock :
		public Module
	{
		public:
			StoreMock() :
				Module(NAME, {})
			{
				setCommand("get"s, bind(&StoreMock::get, this, _1, _2));
				setCommand("set"s, bind(&StoreMock::set, this, _1, _2));
				setCommand("erase"s, bind(&StoreMock::erase, this, _1, _2));
			}
			MOCK_METHOD(string, get, (const string &, const Payload &));
			MOCK_METHOD(string, set, (const string &, const Payload &));
			MOCK_METHOD(string, erase, (const string &, const Payload &));
			static constexpr const char *NAME = "Store";
	};

	const char *Args[] = {
		"PROGRAM NAME",
	};

	class AppMock :
		public Application
	{
		public:
			AppMock() :
				Application(1, Args)
			{}
			MOCK_METHOD(bool, load, (const string &), (override));
			MOCK_METHOD(void, modules, (const function<void(Coeus::Import<ModuleBase> &)> &), (override));
	};

	class InitiativeTests :
		public Test
	{
		public:
			void SetUp() override;
		protected:
			shared_ptr<StrictMock<StoreMock>> store;
			shared_ptr<StrictMock<CampaignMock>> campaign;
			shared_ptr<StrictMock<DiceMock>> dice;
			shared_ptr<Initiative> initiative;
		private:
			shared_ptr<StrictMock<AppMock>> app;
			Coeus::Import<ModuleBase> _store;
			Coeus::Import<ModuleBase> _camp;
			Coeus::Import<ModuleBase> _dice;
	};

	const auto Name = "Initiative"s;

	TEST_F(InitiativeTests, Help) {
		TEST_BEGIN
		EXPECT_NE(""s, initiative->command("help"s, ""s, {}));
		TEST_END
	}

	TEST_F(InitiativeTests, GetNoneRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto msg = Payload::Parse(R"({"op":0, "d":{"channel_id":"12341234"},"t":"MESSAGE_CREATE","s":null})"s);

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillOnce(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = initiative->command("get", "", {});

		EXPECT_EQ("no initiatives yet"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, GetOneRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:bob"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillOnce(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""));

		auto res = initiative->command("get", "", {});

		EXPECT_EQ("\n1: bob\n", res);
		TEST_END
	}

	TEST_F(InitiativeTests, GetOneCurrentRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:bob"s;
		auto curr = "1"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillOnce(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(curr));

		auto res = initiative->command("get", "", {});

		EXPECT_EQ("\n**1: bob**\n", res);
		TEST_END
	}

	TEST_F(InitiativeTests, GetTwoRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:bob;2:asdf"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillOnce(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = initiative->command("get", "", {});

		EXPECT_EQ("\n2: asdf\n1: bob\n", res);
		TEST_END
	}

	TEST_F(InitiativeTests, GetTwoFirstCurrentRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:bob;2:asdf"s;
		auto curr = "1"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillOnce(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(curr));

		auto res = initiative->command("get", "", {});

		EXPECT_EQ("\n2: asdf\n**1: bob**\n", res);
		TEST_END
	}

	TEST_F(InitiativeTests, GetTwoSecondCurrentRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:bob;2:asdf"s;
		auto curr = "2"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillOnce(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(curr));

		auto res = initiative->command("get", "", {});

		EXPECT_EQ("\n**2: asdf**\n1: bob\n", res);
		TEST_END
	}

	TEST_F(InitiativeTests, SetFirstRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(2)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*dice, roll("1", _))
			.Times(1)
			.WillOnce(Return("1"s));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".inits 1:bob;"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".current -1"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = initiative->command("set", "bob 1", {});

		EXPECT_EQ("1: bob"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, SetSecondRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "2:asdf"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(2)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*dice, roll("1", _))
			.Times(1)
			.WillOnce(Return("1"s));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".inits 2:asdf;1:bob;"s, _))
			.Times(1)
			.WillOnce(Return(""));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".current -1"s, _))
			.Times(1)
			.WillOnce(Return(""));

		auto res = initiative->command("set", "bob 1", {});

		EXPECT_EQ("1: bob"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, StartRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, erase(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(""));

		EXPECT_CALL(*store, erase(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""));

		auto res = initiative->command("start"s, ""s, {});

		EXPECT_EQ("tracking initiative"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, StopRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, erase(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(""));

		EXPECT_CALL(*store, erase(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""));

		auto res = initiative->command("stop"s, ""s, {});

		EXPECT_EQ("done tracking initiative"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, NextEmptyRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(1)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, get(_, _))
			.Times(2);

		auto res = initiative->command("next"s, ""s, {});

		EXPECT_EQ("No initiatives tracked"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, NextBeginRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:foo;2:bar"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(2)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".current 2"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".inits 2:bar;1:foo;"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = initiative->command("next"s, ""s, {});

		EXPECT_EQ("It is now bar's turn"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, NextAfterFirstRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:foo;2:bar"s;
		auto curr = "2"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(2)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(curr));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".current 1"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".inits 2:bar;1:foo;"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = initiative->command("next"s, ""s, {});

		EXPECT_EQ("It is now foo's turn"s, res);
		TEST_END
	}

	TEST_F(InitiativeTests, NextAfterLastRoom) {
		TEST_BEGIN
		auto camp = "test-campaign"s;
		auto inits = "1:foo;2:bar"s;
		auto curr = "1"s;

		EXPECT_CALL(*campaign, get(_, _))
			.Times(2)
			.WillRepeatedly(Return(camp));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".inits"s, _))
			.Times(1)
			.WillOnce(Return(inits));

		EXPECT_CALL(*store, get(Name + "."s + camp + ".current"s, _))
			.Times(1)
			.WillOnce(Return(curr));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".current 2"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		EXPECT_CALL(*store, set(Name + "."s + camp + ".inits 2:bar;1:foo;"s, _))
			.Times(1)
			.WillOnce(Return(""s));

		auto res = initiative->command("next"s, ""s, {});

		EXPECT_EQ("It is now bar's turn"s, res);
		TEST_END
	}

	void InitiativeTests::SetUp() {
		store = make_shared<StrictMock<StoreMock>>();
		campaign = make_shared<StrictMock<CampaignMock>>();
		dice = make_shared<StrictMock<DiceMock>>();
		app = make_shared<StrictMock<AppMock>>();
		initiative = make_shared<Initiative>();

		_store = store;
		_camp = campaign;
		_dice = dice;

		ON_CALL(*app, modules(_))
			.WillByDefault(DoAll(
				InvokeArgument<0>(_store),
				InvokeArgument<0>(_dice),
				InvokeArgument<0>(_camp)
			));

		EXPECT_CALL(*app, load(Eq(StoreMock::NAME)))
			.Times(1)
			.WillOnce(Return(true));

		EXPECT_CALL(*app, load(Eq(CampaignMock::NAME)))
			.Times(1)
			.WillOnce(Return(true));

		EXPECT_CALL(*app, load(Eq(DiceMock::NAME)))
			.Times(1)
			.WillOnce(Return(true));

		EXPECT_CALL(*app, modules(_))
			.Times(1);

		initiative->setApplication(app.get());
	}
}
