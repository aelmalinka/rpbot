/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../../modules/Dice.cc"

using namespace testing;

namespace {
	TEST(DiceTests, Roll1) {
		Mod d;

		EXPECT_EQ("1"s, d.command("roll", "1", {}));
	}

	TEST(DiceTests, Roll2Plus2) {
		Mod d;

		EXPECT_EQ("4"s, d.command("roll", "2+2", {}));
	}

	// 2020-01-23 AMR TODO: random values
}
