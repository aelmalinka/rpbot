'use strict';

// 2020-01-08 AMR TODO: integrate into test squite
// 2020-01-08 AMR NOTE: currently required to be running for tests to succeed
const https = require('https');
const fs = require('fs');

const opts = {
	key: fs.readFileSync('tls/localhost.key'),
	cert: fs.readFileSync('tls/localhost.cert')
};

https.createServer(opts, (req, res) => {
	console.log(req.method);
	console.log(req.url);
	console.log(req.headers);

	let body = [];
	req.on('data', (chunk) => {
		body.push(chunk);
	}).on('end', () => {
		body = Buffer.concat(body).toString();

		console.log();
		console.log(body);
	});

	res.writeHead(200);
	res.end('{"res":"woohoo"}\n');
}).listen(8080);
