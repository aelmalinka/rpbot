/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/Module.hh"
#include "../ext/pcg/include/pcg_random.hpp"
#include <random>
#include <list>
#include <sstream>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace std::placeholders;
using namespace RpBot;

namespace qi = boost::spirit::qi;

using boost::lexical_cast;

namespace {
	// 2020-01-22 AMR TODO: libify?
	namespace detail {
		static pcg_extras::seed_seq_from<random_device> seed;

		class Dice {
			public:
				Dice();
				template<typename Rng>
				long operator () (Rng &r) const {
					if(_sides == 1)
						return _count;

					uniform_int_distribution<long> dist(1, _sides);
					auto res = 0l;

					for(auto i = 0; i < _count; i++)
						res += dist(r);

					return res;
				}
				const int &count() const;
				const int &sides() const;
				void setCount(const int);
				void setSides(const int);
			private:
				int _count;
				int _sides;
		};

		class Math {
			public:
				Math();
				explicit Math(const char);
				operator char () const;
				long operator () (const long a, const long b) const {
					switch(_data) {
						case '+':
							return a + b;
						case '-':
							return a - b;
						case '*':
							return a * b;
						case '/':
							return a / b;
						default:
							COEUS_THROW(Exception("Invalid Math Symbol"));
					}
				}
				// 2020-01-23 AMR TODO: constexpr?
				static Math Plus;
				static Math Minus;
				static Math Multiply;
				static Math Divide;
			private:
				char _data;
		};

		Math Math::Plus('+');
		Math Math::Minus('-');
		Math Math::Multiply('*');
		Math Math::Divide('/');

		class Tree {
			public:
				class Node;
				using iterator = list<Node>::const_iterator;
			public:
				Tree();
				template<typename Rng>
				long operator () (Rng &r) const {
					auto res = _first(r);

					for(auto const &i : _others) {
						res = i(r, res);
					}

					return res;
				}
				void setFirst(const Dice &);
				void add(const Math &, const Dice &);
				const Dice &first() const;
				iterator begin() const;
				iterator end() const;
			private:
				Dice _first;
				std::list<Node> _others;
			public:
				class Node {
					public:
						Node(const Math &, const Dice &);
						template<typename Rng>
						long operator () (Rng &rng, long res) const {
							return _sym(res, _dice(rng));
						}
						const Math &symbol() const;
						const Dice &dice() const;
					private:
						Math _sym;
						Dice _dice;
				};
		};

		template<typename Iterator>
		class dicespec :
			public qi::grammar<Iterator, Dice()>
		{
			public:
				dicespec() :
					dicespec::base_type(start)
				{
					using qi::int_;
					using qi::lit;
					using qi::attr;
					using qi::_val;
					using qi::_1;

					start =
						attr(Dice()) >>
						int_[bind(&Dice::setCount, _val, _1)] >>
						-(
							lit('d') >>
							int_[bind(&Dice::setSides, _val, _1)]
						 )
					;
				}
			private:
				qi::rule<Iterator, Dice()> start;
		};

		template<typename Iterator>
		class mathsymb :
			public qi::grammar<Iterator, Math()>
		{
			public:
				mathsymb() :
					mathsymb::base_type(start)
				{
					using qi::lit;
					using qi::_val;

					start = 
						lit('+')[_val = Math::Plus]		|
						lit('-')[_val = Math::Minus]	|
						lit('*')[_val = Math::Multiply]	|
						lit('/')[_val = Math::Divide]
					;
				}
			private:
				qi::rule<Iterator, Math()> start;
		};

		template<typename Iterator>
		class rollspec :
			public qi::grammar<Iterator, Tree()>
		{
			public:
				rollspec() :
					rollspec::base_type(start)
				{
					using qi::attr;
					using qi::_val;
					using qi::_1;
					using qi::_2;

					start =
						attr(Tree()) >>
						dice[bind(&Tree::setFirst, _val, _1)] >>
						*(
							symb >>
							dice
						)[bind(&Tree::add, _val, _1, _2)]
					;
				}
			private:
				mathsymb<Iterator> symb;
				dicespec<Iterator> dice;
				qi::rule<Iterator, Tree()> start;
		};

		template<typename Iterator>
		Tree parse_dice(Iterator begin, Iterator end) {
			auto p = rollspec<Iterator>();
			auto ret = Tree();

			if(!qi::parse(begin, end, p, ret))
				COEUS_LOG(Log, Severity::Error) << "Failed to parse roll specification";

			return ret;
		}
	}

	class Mod :
		public Module
	{
		public:
			Mod();
		protected:
			string help(const string &, const Payload &);
			string roll(const string &, const Payload &);
		private:
			pcg32 _rng;
			static constexpr const char *NAME = "Dice";
	};

	Mod::Mod() :
		Module(Mod::NAME, {}),
		_rng(detail::seed)
	{
		setCommand("help"s, bind(&Mod::help, this, _1, _2));
		setCommand("roll"s, bind(&Mod::roll, this, _1, _2));
	}

	string Mod::help(const string &, const Payload &) {
		return R"(Dice roller
	roll [expression]: roll expression; expression can be math expressions w/ [x]d[y] expressions
)"s;
	}

	// 2020-01-21 AMR TODO: secret rolls
	// 2020-01-22 AMR TODO: exploding dice
	// 2020-01-22 AMR TODO: compounding exploding dice
	// 2020-01-22 AMR TODO: drop/keep
	// 2020-01-22 AMR TODO: reroll
	// 2020-01-22 AMR TODO: criticals (success/failure)
	// 2020-01-22 AMR TODO: {grouping} [grouping]?
	// 2020-01-22 AMR TODO: [labels]
	// 2020-01-23 AMR TODO: better ouput still work cleaning with command
	string Mod::roll(const string &args, const Payload &) {
		auto tree = detail::parse_dice(args.begin(), args.end());
		auto ret = stringstream();

		ret << "got: " << tree.first().count() << "d" << tree.first().sides();
		for(const auto &i : tree) {
			ret << static_cast<char>(i.symbol()) << i.dice().count() << "d" << i.dice().sides();
		}
		ret << flush;

		COEUS_LOG(Log, Severity::Debug) << "rolling: " << ret.str();

		return lexical_cast<string>(tree(_rng));
	}

	COEUS_MODULE(Mod)

	namespace detail {
		Dice::Dice() :
			_count(0),
			_sides(1)
		{}

		const int &Dice::count() const {
			return _count;
		}

		const int &Dice::sides() const {
			return _sides;
		}

		void Dice::setCount(const int v) {
			_count = v;
		}

		void Dice::setSides(const int v) {
			_sides = v;
		}

		Math::Math() :
			_data(Plus._data)
		{}

		Math::Math(const char c) :
			_data(c)
		{}

		Math::operator char () const {
			return _data;
		}

		Tree::Tree()
		{}

		void Tree::setFirst(const Dice &d) {
			_first = d;
		}

		void Tree::add(const Math &m, const Dice &d) {
			_others.emplace_back(m, d);
		}

		const Dice &Tree::first() const {
			return _first;
		}

		Tree::iterator Tree::begin() const {
			return _others.cbegin();
		}

		Tree::iterator Tree::end() const {
			return _others.cend();
		}

		Tree::Node::Node(const Math &m, const Dice &d) :
			_sym(m),
			_dice(d)
		{}

		const Math &Tree::Node::symbol() const {
			return _sym;
		}

		const Dice &Tree::Node::dice() const {
			return _dice;
		}
	}
}
