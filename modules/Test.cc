/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/


#include "../src/Module.hh"

using namespace std;
using namespace std::placeholders;
using namespace RpBot;

namespace {
	class Test :
		public Module
	{
		public:
			Test();
		protected:
			string help(const string &, const Payload &);
			string load(const string &, const Payload &);
			string unload(const string &, const Payload &);
			string echo(const string &, const Payload &);
			string del(const string &, const Payload &);
			string store(const string &, const Payload &);
		private:
			static constexpr const char *NAME = "Test";
	};

	Test::Test() :
		Module(Test::NAME, {})
	{
		setCommand("help", bind(&Test::help, this, _1, _2));
		setCommand("load", bind(&Test::load, this, _1, _2));
		setCommand("unload", bind(&Test::unload, this, _1, _2));
		setCommand("echo", bind(&Test::echo, this, _1, _2));
		setCommand("del", bind(&Test::del, this, _1, _2));
	}

	string Test::help(const string &, const Payload &) {
		return R"(Test:
	*this module is used heavily for debugging and therefore changes regularly*
	load [name]: same as Base::load
	unload [name]: same as Base::unload
)"s;
	}

	string Test::load(const string &args, const Payload &) {
		if(!app().load(args)) {
			return args + " failed to load"s;
		}

		return args + " loaded"s;
	}

	string Test::unload(const string &args, const Payload &) {
		if(args == NAME)
			return "modules can't unload themselves"s;

		if(!app().unload(args)) {
			return args + " failed to unload"s;
		}

		return args + " unloaded"s;
	}


	string Test::echo(const string &args, const Payload &) {
		return args;
	}

	string Test::del(const string &, const Payload &payload) {
		rest().deleteMessage(payload.Data()["id"].asString(), payload.Data()["channel_id"].asString());
		return ""s;
	}

	COEUS_MODULE(Test)
}
