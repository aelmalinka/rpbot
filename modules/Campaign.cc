/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/Module.hh"
#include <algorithm>
#include <list>
#include <regex>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace std::placeholders;
using namespace RpBot;

using boost::join;
using boost::split;
using boost::replace_all;
using boost::replace_all_copy;
using boost::is_any_of;

namespace {
	class Campaign :
		public Module
	{
		public:
			Campaign();
		private:
			string help(const string &, const Payload &);
			string get(const string &, const Payload &);
			string create(const string &, const Payload &);
			string delete_(const string &, const Payload &);
			string add(const string &, const Payload &);
			string remove(const string &, const Payload &);
		private:
			void onMessageCreate(const Payload &) final;
		private:
			void addCampaign(const string &);
			void addPlayer(const string &, const string &, const string &);
			void addChannel(const string &, const string &);
			void removeCampaign(const string &);
			void removeCharacter(const string &, const string &);
			void removePlayer(const string &, const string &);
			void removeChannel(const string &, const string &);
			bool isCampaign(const string &);
			bool isPlayer(const string &, const string &);
			bool isCharacter(const string &, const string &);
			bool isChannel(const string &, const string &);
		private:
			string getCampaign(const Payload &);
			list<string> &getCampaigns();
			map<string, string> &getPlayers(const string &);
			list<string> &getChannels(const string &);
			void saveCampaigns(list<string> const &);
			void savePlayers(const string &, map<string, string> const &);
			void saveChannels(const string &, list<string> const &);
			list<string>::iterator findCampaign(const string &);
			map<string, string>::iterator findPlayer(const string &, const string &);
			map<string, string>::iterator findCharacter(const string &, const string &);
			list<string>::iterator findChannel(const string &, const string &);
		private:
			bool isPlayerId(const string &);
			bool isChannelId(const string &);
			string getId(const string &);
			string safeName(const string &);
		private:
			list<string> _campaigns;
			map<string, string> _players;
			list<string> _channels;
			string _last_campaign;
			static constexpr const char *NAME = "Campaign";
	};

	Campaign::Campaign() :
		Module(Campaign::NAME, {"Store"s})
	{
		setCommand("help"s, bind(&Campaign::help, this, _1, _2));
		setCommand("get"s, bind(&Campaign::get, this, _1, _2));
		setCommand("create"s, bind(&Campaign::create, this, _1, _2));
		setCommand("delete"s, bind(&Campaign::delete_, this, _1, _2));
		setCommand("add"s, bind(&Campaign::add, this, _1, _2));
		setCommand("remove"s, bind(&Campaign::remove, this, _1, _2));
	}

	string Campaign::help(const string &, const Payload &) {
		return R"(Campaign Management
	get [name]: get info about campaign (or by chat)
	create [name]: create new campaign
	delete [name]: delete campaign
	add [channel] [campaign]: add channel to campaign
	add [player] [name]: add player with character name to campaign in this room
	add [player] [name] [campaign]: add player with character name to campaign
	remove [target] [name]: remove player/channel from campaing
	remove [target]: remove character, player, or channel from campaign in this room
)"s;
	}

	string Campaign::get(const string &a, const Payload &p) {
		COEUS_LOG(Log, Severity::Info) << "Campaign Get";
		auto args = a;
		auto name = ""s;

		if(args == ""s)
			args = "<#"s + p.Data()["channel_id"].asString() + ">"s;

		COEUS_LOG(Log, Severity::Debug) << "looking for " << args;
		// 2020-01-26 AMR TODO: name this better
		if(isChannelId(args)) {
			auto &il = getCampaigns();
			auto id = getId(args);
			auto i = find_if(il.begin(), il.end(), bind(&Campaign::isChannel, this, ref(id), _1));

			if(i != il.end())
				name = *i;
		} else {
			name = args;
		}

		if(!isCampaign(name))
			return name + " not found"s;

		auto &m = getPlayers(name);
		auto f = [](auto const &a, auto const &b) -> string {
			return a + b.first + " (" + b.second + ")\n"s;
		};

		// 2020-01-26 AMR TODO: map to actual player name
		// 2020-01-26 AMR TODO: map to actual channel names
		return name + "\nCharacters:\n"s + 
			accumulate(m.begin(), m.end(), ""s, f) +
			"Channels:\n" +
			join(getChannels(name), "\n");
	}

	string Campaign::create(const string &name, const Payload &) {
		COEUS_LOG(Log, Severity::Info) << "Campaign Create";

		if(name == ""s)
			return "disallowed name"s;

		if(isCampaign(name))
			return name + " already exists"s;

		addCampaign(name);

		return name + " created"s;
	}

	string Campaign::delete_(const string &name, const Payload &) {
		COEUS_LOG(Log, Severity::Info) << "Campaign Delete";

		if(!isCampaign(name))
			return name + " not found"s;

		removeCampaign(name);

		return name + " deleted"s;
	}

	string Campaign::add(const string &args, const Payload &p) {
		COEUS_LOG(Log, Severity::Info) << "Campaign Add";

		auto v = vector<string>();
		split(v, args, is_any_of(" "));

		auto isP = isPlayerId(v[0]);
		auto isC = isChannelId(v[0]);

		auto getName = [this, &args, &p, &v, &isP, &isC]() -> string {
			if(isC && v.size() == 1)
				return getCampaign(p);
			else if(isC)
				return args.substr(v[0].size() + 1);
			else if(isP && v.size() == 2)
				return getCampaign(p);
			else if(isP)
				return args.substr(v[0].size() + v[1].size() + 2);
			else
				return ""s;
		};

		if(!isP && !isC)
			return "Please tag the player or channel you wish to add"s;

		auto name = getId(v[0]);
		auto who = (v.size() > 1 ? v[1] : ""s);
		auto which = getName();

		if(!isCampaign(which))
			return which + " not found"s;

		COEUS_LOG(Log, Severity::Debug) << "adding " << name << " " << who << " to " << which;

		if(isP) {
			if(isPlayer(name, which))
				return name + " is already a player of "s + which;
			if(isCharacter(who, which))
				return who + " is already a character of "s + which;

			addPlayer(name, who, which);
		} else if(isC) {
			if(isChannel(name, which))
				return name + " is already a channel of "s + which;

			addChannel(name, which);
		} else {
			return "Unknown id type"s;
		}

		return name + " added to "s + which;
	}

	string Campaign::remove(const string &args, const Payload &p) {
		COEUS_LOG(Log, Severity::Info) << "Campaign Remove";

		auto v = vector<string>();
		split(v, args, is_any_of(" "));

		auto isP = isPlayerId(v[0]);
		auto isC = isChannelId(v[0]);

		auto getName = [this, &args, &p, &v]() -> string {
			if(v.size() == 1)
				return getCampaign(p);
			else
				return args.substr(v[0].size() + 1);
		};

		auto name = (isP || isC) ? getId(v[0]) : v[0];
		auto which = getName();

		if(!isCampaign(which))
			return which + " not found"s;

		COEUS_LOG(Log, Severity::Debug) << "remove " << name << " from " << which;

		if(isP) {
			if(!isPlayer(name, which))
				return name + " not found in "s + which;

			removePlayer(name, which);
		} else if(isC) {
			if(!isChannel(name, which))
				return name + " not found in "s + which;

			removeChannel(name, which);
		} else {
			if(!isCharacter(name, which))
				return name + " not found in "s + which;

			removeCharacter(name, which);
		}

		return name + " removed from "s + which;
	}

	string Campaign::getCampaign(const Payload &p) {
		auto id = "#"s + p.Data()["channel_id"].asString();
		auto &il = getCampaigns();
		auto i = find_if(il.begin(), il.end(), bind(&Campaign::isChannel, this, ref(id), _1));

		if(i != il.end()) {
			COEUS_LOG(Log, Severity::Debug) << "got campaign " << *i;
			return *i;
		} else
			return ""s;
	}

	list<string> &Campaign::getCampaigns() {
		if(_campaigns.empty()) {
			COEUS_LOG(Log, Severity::Debug) << "loading campaign";
			auto names = dep("Store"s)->command("get"s, NAME + ".names"s, {});

			if(names != ""s)
				split(_campaigns, names, is_any_of(";"));
		}

		return _campaigns;
	}

	map<string, string> &Campaign::getPlayers(const string &name) {
		if(_players.empty() || safeName(name) != _last_campaign) {
			COEUS_LOG(Log, Severity::Debug) << "loading players";
			auto ids = dep("Store"s)->command("get"s, NAME + "."s + safeName(name) + ".players"s, {});

			auto v = vector<string>();
			if(ids != ""s)
				split(v, ids, is_any_of(";"));

			for(auto &i : v) {
				COEUS_LOG(Log, Severity::Debug) << "player: " << i;

				auto t = vector<string>();
				split(t, i, is_any_of(":"));

				if(t.size() != 2)
					COEUS_LOG(Log, Severity::Error) << "failed to parse player: " << i;
				else {
					COEUS_LOG(Log, Severity::Debug) << t[0] << " " << t[1];
					_players[t[0]] = t[1];
				}
			}

			_last_campaign = safeName(name);
		}

		return _players;
	}

	list<string> &Campaign::getChannels(const string &name) {
		if(_channels.empty() || safeName(name) != _last_campaign) {
			COEUS_LOG(Log, Severity::Debug) << "loading channels";
			auto ids = dep("Store"s)->command("get"s, NAME + "."s + safeName(name) + ".channels"s, {});

			if(ids != ""s)
				split(_channels, ids, is_any_of(";"));

			_last_campaign = safeName(name);
		}

		return _channels;
	}

	void Campaign::saveCampaigns(list<string> const &li) {
		auto s = join(li, ";"s);

		if(s != ""s)
			dep("Store"s)->command("set"s, NAME + ".names "s + s, {});
		else
			dep("Store"s)->command("erase", NAME + ".names"s, {});
	}

	void Campaign::savePlayers(const string &where, map<string, string> const &m) {
		auto v = vector<string>();

		v.resize(m.size());
		transform(m.begin(), m.end(), v.begin(), [](auto const &a) {
			return a.first + ":"s + a.second;
		});

		auto s = join(v, ";"s);

		dep("Store"s)->command("set"s, NAME + "."s + safeName(where) + ".players "s + s, {});
	}

	void Campaign::saveChannels(const string &where, list<string> const &li) {
		auto s = join(li, ";"s);

		dep("Store"s)->command("set"s, NAME + "."s + safeName(where) + ".channels "s + s, {});
	}

	bool Campaign::isChannelId(const string &name) {
		return regex_match(name, regex("<#[0-9]+>"s));
	}

	bool Campaign::isPlayerId(const string &name) {
		return regex_match(name, regex("<@![0-9]+>"s));
	}

	string Campaign::getId(const string &name) {
		auto m = smatch();
		regex_search(name, m, regex("<(.+)>"s));

		if(m.size() == 1)
			return m[0];
		else
			return m[1];
	}

	string Campaign::safeName(const string &name) {
		auto ret = replace_all_copy(name, " "s, "-"s);

		replace_all(ret, ";", "-"s);
		replace_all(ret, ".", "-"s);

		return ret;
	}

	void Campaign::addCampaign(const string &name) {
		auto &li = getCampaigns();
		li.push_back(safeName(name));
		saveCampaigns(li);
	}

	void Campaign::addPlayer(const string &name, const string &character, const string &where) {
		auto &li = getPlayers(where);
		li.emplace(character, name);
		savePlayers(where, li);
	}

	void Campaign::addChannel(const string &name, const string &where) {
		auto &li = getChannels(where);
		li.push_back(name);
		saveChannels(where, li);
	}

	void Campaign::removeCampaign(const string &name) {
		auto &li = getCampaigns();
		li.erase(findCampaign(name));

		saveCampaigns(li);

		dep("Store"s)->command("erase"s, NAME + "."s + safeName(name) + ".players"s, {});
		dep("Store"s)->command("erase"s, NAME + "."s + safeName(name) + ".channels"s, {});
	}

	void Campaign::removeCharacter(const string &name, const string &where) {
		auto &li = getPlayers(where);
		auto i = findCharacter(name, where);

		if(i != li.end())
			li.erase(i);
		else
			COEUS_THROW(Exception("removing player that doesn't exist"s));

		savePlayers(where, li);
	}

	void Campaign::removePlayer(const string &name, const string &where) {
		auto &li = getPlayers(where);
		auto i = findPlayer(name, where);

		while(i != li.end()) {
			li.erase(i);
			i = findPlayer(name, where);
		}

		savePlayers(where, li);
	}

	void Campaign::removeChannel(const string &name, const string &where) {
		auto &li = getChannels(where);
		auto i = findChannel(name, where);

		if(i != li.end())
			li.erase(i);
		else
			COEUS_THROW(Exception("removing channel that doesn't exist"s));

		saveChannels(where, li);
	}

	bool Campaign::isCampaign(const string &name) {
		return findCampaign(name) != getCampaigns().end();
	}

	bool Campaign::isPlayer(const string &name, const string &where) {
		return findPlayer(name, where) != getPlayers(where).end();
	}

	bool Campaign::isCharacter(const string &name, const string &where) {
		return findCharacter(name, where) != getPlayers(where).end();
	}

	bool Campaign::isChannel(const string &name, const string &where) {
		return findChannel(name, where) != getChannels(where).end();
	}

	list<string>::iterator Campaign::findCampaign(const string &name) {
		auto &li = getCampaigns();
		return find(li.begin(), li.end(), safeName(name));
	}

	map<string, string>::iterator Campaign::findPlayer(const string &name, const string &campaign) {
		auto &li = getPlayers(campaign);
		return find_if(li.begin(), li.end(), bind(equal_to<string>(), bind(&map<string, string>::value_type::second, _1), cref(name)));
	}

	map<string, string>::iterator Campaign::findCharacter(const string &character, const string &campaign) {
		auto &li = getPlayers(campaign);
		return li.find(character);
	}

	list<string>::iterator Campaign::findChannel(const string &name, const string &campaign) {
		auto &li = getChannels(campaign);
		return find(li.begin(), li.end(), name);
	}

	void Campaign::onMessageCreate(const Payload &p) {
		Module::onMessageCreate(p);
		// 2020-01-25 AMR NOTE: just cache for message
		COEUS_LOG(Log, Severity::Debug) << "clearing caches";
		_campaigns.clear();
		_players.clear();
		_channels.clear();
		_last_campaign = ""s;
	}

	COEUS_MODULE(Campaign)
}
