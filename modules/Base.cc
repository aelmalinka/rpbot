/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3'
*/

#include "../src/Module.hh"
#include <algorithm>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace std::placeholders;
using namespace RpBot;

using boost::split;
using boost::join;
using boost::is_any_of;
using boost::istarts_with;
using boost::ierase_first_copy;

namespace {
	class Base :
		public Module
	{
		public:
			Base();
			void onLoaded();
		protected:
			string load(const string &, const Payload &);
			string unload(const string &, const Payload &);
			string save(const string &, const Payload &);
			string help(const string &, const Payload &);
			string config(const string &, const Payload &);
			string alias(const string &, const Payload &);
			string unalias(const string &, const Payload &);
		private:
			static constexpr const char *NAME = "Base";
	};

	Base::Base() :
		Module(Base::NAME, {"Store"s})
	{
		setCommand("help"s, bind(&Base::help, this, _1, _2));
		setCommand("load"s, bind(&Base::load, this, _1, _2));
		setCommand("unload"s, bind(&Base::unload, this, _1, _2));
		setCommand("config"s, bind(&Base::config, this, _1, _2));
		setCommand("alias"s, bind(&Base::alias, this, _1, _2));
		setCommand("unalias"s, bind(&Base::unalias, this, _1, _2));
		setCommand("save"s, bind(&Base::save, this, _1, _2));
	}

	string Base::load(const string &args, const Payload &) {
		if(!app().load(args)) {
			return args + " failed to load"s;
		}

		return args + " loaded";
	}

	string Base::unload(const string &args, const Payload &) {
		if(args == NAME)
			return "modules can't unload themselves";

		if(!app().unload(args)) {
			return args + " failed to unload"s;
		}

		return args + " unloaded";
	}

	string Base::help(const string &, const Payload &p) {
		auto s = R"(Help:
Base:
	load [name]: load module named name (case sensitive)
	unload [name]: unload module
	config: outputs config options
	config [name]: outputs name's current value
	config [name] [value]: sets name's value to value and then outputs it
	alias [prefix][alias] [prefix][module] [command] [args]
	unalias [prefix][alias]
	save: saves config values (currently just calls Store::save)
	help: this menu
)"s;
		app().modules([this, &s, &p](auto &m) {
			if(m->name() != NAME) {
				auto t = m->command("help"s, ""s, p);
				if(t.find("not found"s) == string::npos)
					s += t;
				else
					s += m->name() + ": no help found\n"s;
			}
		});

		return s;
	}

	string Base::config(const string &args, const Payload &p) {
		auto n = count(args.begin(), args.end(), ' ');

		if(args.size() == 0) {
			return "2020-01-18 AMR TODO: not implemented"s;
		} else if(n == 0) {
			return dep("Store"s)->command("get"s, args, p);
		} else if(n == 1) {
			auto r = dep("Store"s)->command("set"s, args, p);
			onLoaded();
			return r;
		} else {
			return "USAGE: config [name] [value]"s;
		}
	}

	// 2020-02-01 AMR TODO: less copying
	// 2020-02-01 AMR TODO: allow getting current aliases
	string Base::alias(const string &args, const Payload &) {
		auto v = vector<string>();
		split(v, args, is_any_of(" "));

		auto i = find_if(v.begin(), v.end(), [this](auto const &a) -> bool {
			return istarts_with(a, prefix());
		});

		auto alias_begin = i++;

		i = find_if(i, v.end(), [this](auto const &a) -> bool {
			return istarts_with(a, prefix());
		});

		auto command_begin = i;
		auto alias_end = i;

		auto alias_v = vector<string>(alias_begin, alias_end);
		auto alias = ierase_first_copy(join(alias_v, " "s), prefix());

		auto name = ""s;
		auto command = ""s;
		auto rest = ""s;

		if(command_begin != v.end()) {
			name = ierase_first_copy(*command_begin, prefix());

			if(++command_begin != v.end()) {
				command = *command_begin;

				if(++command_begin != v.end()) {
					auto rest_v = vector<string>(command_begin, v.end());
					rest = join(rest_v, " "s);
				}
			}
		}

		if(name.empty() || command.empty()) {
			return "USAGE: alias [prefix][alias] [prefix][module] [command] [args]"s;
		}

		app().addAlias(alias, name, command, rest);

		return "added alias "s + alias;
	}

	string Base::unalias(const string &args, const Payload &) {
		auto alias = ierase_first_copy(args, prefix());

		app().removeAlias(alias);

		return "removed alias "s + alias;
	}

	string Base::save(const string &, const Payload &) {
		return dep("Store"s)->command("save"s, ""s, {});
	}

	void Base::onLoaded() {
		auto p = dep("Store"s)->command("get"s, "prefix"s, {});

		if(p != "")
			app().setPrefix(p);
	}

	COEUS_MODULE(Base)
}
