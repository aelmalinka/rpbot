/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/Module.hh"
#include <map>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace std::placeholders;
using namespace RpBot;

using boost::split;
using boost::is_any_of;
using boost::lexical_cast;
using boost::bad_lexical_cast;

namespace {
	class Initiative :
		public Module
	{
		public:
			Initiative();
		protected:
			string help(const string &, const Payload &);
			string get(const string &, const Payload &);
			string set(const string &, const Payload &);
			string start(const string &, const Payload &);
			string stop(const string &, const Payload &);
			string remove(const string &, const Payload &);
			string next(const string &, const Payload &);
		private:
			void onMessageCreate(const Payload &) override;
		private:
			string getCampaign(const Payload &);
			void fillInits(const Payload &);
			void saveInits(const Payload &);
			void eraseInits(const Payload &);
		private:
			map<int, string, greater<int>> _inits;
			int _current;
		private:
			static constexpr const char *NAME = "Initiative";
	};

	// 2020-01-26 AMR TODO: variable deps (system?)
	Initiative::Initiative() :
		Module(NAME, {"Campaign"s, "Store"s, "Dice"s})
	{
		setCommand("help"s, bind(&Initiative::help, this, _1, _2));
		setCommand("get"s, bind(&Initiative::get, this, _1, _2));
		setCommand("set"s, bind(&Initiative::set, this, _1, _2));
		setCommand("start"s, bind(&Initiative::start, this, _1, _2));
		setCommand("stop"s, bind(&Initiative::stop, this, _1, _2));
		setCommand("remove"s, bind(&Initiative::remove, this, _1, _2));
		setCommand("next"s, bind(&Initiative::next, this, _1, _2));
	}

	string Initiative::help(const string &, const Payload &) {
		// 2020-01-26 AMR TODO: set [expr]: set your character to rolled exp
		// 2020-01-26 AMR TODO: remove: removes yourself?
		return R"(Initiative
	start: start tracking initiative
	stop: stop tracking initiative
	get: print out current initative table
	set [name] [expr]: set name to rolled exp
	remove [name]: removes name from table
	next: move to the next turn
)"s;
	}

	// 2020-01-29 AMR TODO: get init list from other channel?
	string Initiative::get(const string &, const Payload &p) {
		fillInits(p);

		// 2020-01-30 AMR NOTE: discord api trims newline off front
		auto res = "\n"s;
		for(auto const &[init, name] : _inits) {
			if(init == _current)
				res += "**"s;

			res += lexical_cast<string>(init) + ": "s + name;

			if(init == _current)
				res += "**"s;

			res += "\n"s;
		}

		if(res == "\n"s)
			res = "no initiatives yet"s;

		return res;
	}

	string Initiative::set(const string &args, const Payload &p) {
		fillInits(p);

		auto v = vector<string>();

		split(v, args, is_any_of(" "));
		if(v.size() != 2)
			return "set [name] [expr]"s;

		auto name = v[0];
		auto expr = v[1];
		auto val = lexical_cast<int>(dep("Dice"s)->command("roll", expr, {}));
		auto i = find_if(_inits.begin(), _inits.end(), bind(equal_to<string>(), cref(name), bind(&map<int, string, greater<int>>::value_type::second, _1)));

		if(i == _inits.end()) {
			_inits[val] = name;
		} else {
			auto n = _inits.extract(i);
			n.key() = val;
			_inits.insert(move(n));
		}

		saveInits(p);

		return lexical_cast<string>(val) + ": "s + name;
	}

	string Initiative::remove(const string &args, const Payload &p) {
		fillInits(p);

		auto i = find_if(_inits.begin(), _inits.end(), bind(equal_to<string>(), cref(args), bind(&map<int, string, greater<int>>::value_type::second, _1)));
		if(i != _inits.end()) {
			_inits.erase(i);
			saveInits(p);
			return args + " erased"s;
		}

		return args + " not found"s;
	}

	string Initiative::start(const string &, const Payload &p) {
		eraseInits(p);

		return "tracking initiative"s;
	}

	string Initiative::stop(const string &, const Payload &p) {
		eraseInits(p);

		return "done tracking initiative"s;
	}

	string Initiative::next(const string &, const Payload &p) {
		fillInits(p);

		auto i = (_current > 0 ? _inits.find(_current) : _inits.begin());

		if(_inits.empty())
			return "No initiatives tracked"s;

		if(_current != -1 && ++i == _inits.end())
			i = _inits.begin();

		_current = i->first;

		COEUS_LOG(Log, Severity::Debug) << "next is " << _current << ": " << i->second;
		saveInits(p);

		return "It is now "s + i->second + "'s turn"s;
	}

	void Initiative::fillInits(const Payload &p) {
		auto camp = getCampaign(p);
		auto vec = vector<string>();
		auto str = dep("Store"s)->command("get", NAME + "."s + camp + ".inits"s, {});
		auto cur = dep("Store"s)->command("get", NAME + "."s + camp + ".current"s, {});

		split(vec, str, is_any_of(";"));

		COEUS_LOG(Log, Severity::Debug) << "inits: " << str;
		for(auto &i : vec) {
			auto t = vector<string>();

			split(t, i, is_any_of(":"));

			if(t.size() != 2)
				COEUS_LOG(Log, Severity::Error) << "Init parse failed: " << i;
			else {
				try {
					_inits[lexical_cast<int>(t[0])] = t[1];
				} catch(bad_lexical_cast &) {
					COEUS_LOG(Log, Severity::Error) << t[0] << " not understood";
				}
			}
		}

		COEUS_LOG(Log, Severity::Debug) << "current: " << cur;

		if(cur != "")
			_current = lexical_cast<int>(cur);
		else
			_current = -1;
	}

	void Initiative::saveInits(const Payload &p) {
		auto camp = getCampaign(p);
		auto inits = ""s;

		for(auto const &[k, v] : _inits) {
			inits += lexical_cast<string>(k) + ":"s + v + ";"s;
		}

		dep("Store"s)->command("set"s, NAME + "."s + camp + ".current "s + lexical_cast<string>(_current), {});
		dep("Store"s)->command("set"s, NAME + "."s + camp + ".inits "s + inits, {});
	}

	void Initiative::eraseInits(const Payload &p) {
		auto camp = getCampaign(p);

		dep("Store"s)->command("erase"s, NAME + "."s + camp + ".current"s, {});
		dep("Store"s)->command("erase"s, NAME + "."s + camp + ".inits"s, {});
	}

	string Initiative::getCampaign(const Payload &p) {
		auto v = vector<string>();
		auto s = dep("Campaign"s)->command("get"s, ""s, p);

		split(v, s, is_any_of("\n"));

		return v[0];
	}

	void Initiative::onMessageCreate(const Payload &p) {
		Module::onMessageCreate(p);
		// 2020-01-28 AMR NOTE: clear caches
		_inits.clear();
		_current = -1;
	}

	COEUS_MODULE(Initiative)
}
