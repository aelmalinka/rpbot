/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/Module.hh"
#include <map>
#include <vector>
#include <fstream>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace std::placeholders;
using namespace std::filesystem;
using namespace RpBot;

using boost::split;
using boost::is_any_of;

namespace {
	class Store :
		public Module
	{
		public:
			Store();
			~Store();
		protected:
			string help(const string &, const Payload &);
			string get(const string &, const Payload &);
			string set(const string &, const Payload &);
			string erase(const string &, const Payload &);
			string save(const string &, const Payload &);
		private:
			void _load();
			void _save();
		private:
			map<string, string> _values;
			static constexpr const char *NAME = "Store";
	};

	Store::Store() :
		Module(Store::NAME, {})
	{
		_load();

		setCommand("help", bind(&Store::help, this, _1, _2));
		setCommand("get", bind(&Store::get, this, _1, _2));
		setCommand("set", bind(&Store::set, this, _1, _2));
		setCommand("erase", bind(&Store::erase, this, _1, _2));
		setCommand("save", bind(&Store::save, this, _1, _2));
	}

	Store::~Store() {
		// 2020-01-14 AMR TODO: this is bad
		_save();
	}

	string Store::help(const string &, const Payload &) {
		return R"(Store:
	get [name]: returns value of name
	set [name]: sets value of name
	erase [name]: removes name from storage
	save: saves values to disk
)"s;
	}

	string Store::get(const string &what, const Payload &) {
		return _values[what];
	}

	string Store::set(const string &args, const Payload &) {
		vector<string> parts;
		split(parts, args, is_any_of(" \t\n"));

		if(parts.size() != 2)
			return "USAGE: set name value";

		auto what = parts[0];
		auto val = parts[1];

		_values[what] = val;

		return what + " = "s + val;
	}

	string Store::erase(const string &what, const Payload &) {
		if(_values.find(what) != _values.end()) {
			_values.erase(what);
			return what + " erased"s;
		} else
			return what + " not found"s;
	}

	string Store::save(const string &, const Payload &) {
		_save();
		return "values saved"s;
	}

	void Store::_load() {
		if(exists(current_path() / "values")) {
			COEUS_LOG(Log, Severity::Info) << "Loading values";
			fstream file("values", ios_base::in);
			while(!file.eof()) {
				string line;
				vector<string> parts;

				getline(file, line);
				split(parts, line, is_any_of(":"));

				if(parts[0] != "" && parts[1] != "")
					_values.emplace(move(parts[0]), move(parts[1]));
			}
		}
	}

	void Store::_save() {
		COEUS_LOG(Log, Severity::Info) << "Saving values";
		fstream file("values", ios_base::out);
		for(auto &&[k, v] : _values) {
			// 2020-01-15 AMR TODO: why are these values showing up?
			if(k != "" && v != "") {
				COEUS_LOG(Log, Severity::Debug) << "writing " << k << ":" << v;
				file << k << ":" << v << endl;
			}
		}
	}

	COEUS_MODULE(Store)
}
